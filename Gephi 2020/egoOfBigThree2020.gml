graph [
  node [
    id 0
    label "105575"
    name "Ricardas"
    surname "Berankis"
    country "LTU"
    hand "R"
    dateOfBirth 19900621.0
    tourneyNum 7
    avgRank 68.11111111111111
    avgPoints 839.7407407407408
  ]
  node [
    id 1
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 9
    avgRank 9.962962962962964
    avgPoints 2820.0
  ]
  node [
    id 2
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 9
    avgRank 1.1111111111111112
    avgPoints 10940.185185185184
  ]
  node [
    id 3
    label "105147"
    name "Tatsuma"
    surname "Ito"
    country "JPN"
    hand "R"
    dateOfBirth 19880518.0
    tourneyNum 1
    avgRank 166.96296296296296
    avgPoints 346.962962962963
  ]
  node [
    id 4
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 10
    avgRank 107.44444444444444
    avgPoints 592.5925925925926
  ]
  node [
    id 5
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 16
    avgRank 34.333333333333336
    avgPoints 1380.7407407407406
  ]
  node [
    id 6
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 11
    avgRank 21.074074074074073
    avgPoints 1987.2222222222222
  ]
  node [
    id 7
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 11
    avgRank 27.333333333333332
    avgPoints 1608.3333333333333
  ]
  node [
    id 8
    label "123755"
    name "Daniel Elahi"
    surname "Galan Riveros"
    country "COL"
    hand "U"
    dateOfBirth 19960618.0
    tourneyNum 4
    avgRank 141.55555555555554
    avgPoints 432.85185185185185
  ]
  node [
    id 9
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 13
    avgRank 87.44444444444444
    avgPoints 713.2592592592592
  ]
  node [
    id 10
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 12
    avgRank 22.925925925925927
    avgPoints 1912.6296296296296
  ]
  node [
    id 11
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 14
    avgRank 17.555555555555557
    avgPoints 2147.962962962963
  ]
  node [
    id 12
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 12
    avgRank 32.55555555555556
    avgPoints 1467.4444444444443
  ]
  node [
    id 13
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 12
    avgRank 55.888888888888886
    avgPoints 985.7777777777778
  ]
  node [
    id 14
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 8
    avgRank 86.25925925925925
    avgPoints 720.4814814814815
  ]
  node [
    id 15
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 8
    avgRank 3.3333333333333335
    avgPoints 8073.148148148148
  ]
  node [
    id 16
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 1
    avgRank 4.037037037037037
    avgPoints 6699.62962962963
  ]
  node [
    id 17
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 10
    avgRank 7.0
    avgPoints 4496.666666666667
  ]
  node [
    id 18
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 14
    avgRank 13.444444444444445
    avgPoints 2470.5555555555557
  ]
  node [
    id 19
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 10
    avgRank 22.925925925925927
    avgPoints 1925.5555555555557
  ]
  node [
    id 20
    label "104291"
    name "Malek"
    surname "Jaziri"
    country "TUN"
    hand "R"
    dateOfBirth 19840120.0
    tourneyNum 3
    avgRank 249.62962962962962
    avgPoints 201.92592592592592
  ]
  node [
    id 21
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 15
    avgRank 33.370370370370374
    avgPoints 1472.6666666666667
  ]
  node [
    id 22
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 14
    avgRank 42.44444444444444
    avgPoints 1225.7777777777778
  ]
  node [
    id 23
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 7
    avgRank 1.8888888888888888
    avgPoints 9816.111111111111
  ]
  node [
    id 24
    label "144707"
    name "Mikael"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19980909.0
    tourneyNum 8
    avgRank 81.70370370370371
    avgPoints 752.1111111111111
  ]
  node [
    id 25
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 14
    avgRank 5.888888888888889
    avgPoints 5405.185185185185
  ]
  node [
    id 26
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 12
    avgRank 11.444444444444445
    avgPoints 2764.074074074074
  ]
  node [
    id 27
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 9
    avgRank 110.14814814814815
    avgPoints 553.5555555555555
  ]
  node [
    id 28
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 13
    avgRank 54.96296296296296
    avgPoints 978.3333333333334
  ]
  node [
    id 29
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 10
    avgRank 49.77777777777778
    avgPoints 1030.7407407407406
  ]
  node [
    id 30
    label "136440"
    name "Dominik"
    surname "Koepfer"
    country "GER"
    hand "L"
    dateOfBirth 19940429.0
    tourneyNum 7
    avgRank 78.0
    avgPoints 777.2592592592592
  ]
  node [
    id 31
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 9
    avgRank 11.925925925925926
    avgPoints 2570.5555555555557
  ]
  node [
    id 32
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 12
    avgRank 4.7407407407407405
    avgPoints 6547.592592592592
  ]
  node [
    id 33
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 15
    avgRank 25.185185185185187
    avgPoints 1700.037037037037
  ]
  node [
    id 34
    label "206173"
    name "Jannik"
    surname "Sinner"
    country "ITA"
    hand "U"
    dateOfBirth 20010816.0
    tourneyNum 12
    avgRank 59.81481481481482
    avgPoints 998.7037037037037
  ]
  node [
    id 35
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 13
    avgRank 33.074074074074076
    avgPoints 1410.3703703703704
  ]
  node [
    id 36
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 13
    avgRank 46.22222222222222
    avgPoints 1152.888888888889
  ]
  node [
    id 37
    label "200624"
    name "Sebastian"
    surname "Korda"
    country "USA"
    hand "U"
    dateOfBirth 20000705.0
    tourneyNum 3
    avgRank 180.74074074074073
    avgPoints 355.74074074074076
  ]
  node [
    id 38
    label "111456"
    name "Mackenzie"
    surname "Mcdonald"
    country "USA"
    hand "R"
    dateOfBirth 19950416.0
    tourneyNum 8
    avgRank 202.37037037037038
    avgPoints 290.0740740740741
  ]
  node [
    id 39
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 9
    avgRank 24.74074074074074
    avgPoints 1689.6296296296296
  ]
  node [
    id 40
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 12
    avgRank 60.111111111111114
    avgPoints 930.3333333333334
  ]
  node [
    id 41
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 14
    avgRank 11.88888888888889
    avgPoints 2926.222222222222
  ]
  node [
    id 42
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 7
    avgRank 85.22222222222223
    avgPoints 727.0
  ]
  node [
    id 43
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 10
    avgRank 19.77777777777778
    avgPoints 2000.3333333333333
  ]
  node [
    id 44
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 10
    avgRank 79.44444444444444
    avgPoints 756.3333333333334
  ]
  node [
    id 45
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 10
    avgRank 59.333333333333336
    avgPoints 951.7037037037037
  ]
  node [
    id 46
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 3
    avgRank 37.925925925925924
    avgPoints 1279.8148148148148
  ]
  node [
    id 47
    label "106198"
    name "Hugo"
    surname "Dellien"
    country "BOL"
    hand "R"
    dateOfBirth 19930616.0
    tourneyNum 8
    avgRank 97.74074074074075
    avgPoints 660.7777777777778
  ]
  node [
    id 48
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 12
    avgRank 79.55555555555556
    avgPoints 761.5185185185185
  ]
  node [
    id 49
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 10
    avgRank 12.407407407407407
    avgPoints 2550.185185185185
  ]
  node [
    id 50
    label "105882"
    name "Stefano"
    surname "Travaglia"
    country "ITA"
    hand "U"
    dateOfBirth 19911218.0
    tourneyNum 10
    avgRank 78.18518518518519
    avgPoints 771.8518518518518
  ]
  node [
    id 51
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 12
    avgRank 57.44444444444444
    avgPoints 968.8888888888889
  ]
  node [
    id 52
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 13
    avgRank 58.44444444444444
    avgPoints 943.6666666666666
  ]
  node [
    id 53
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 15
    avgRank 29.48148148148148
    avgPoints 1537.2222222222222
  ]
  node [
    id 54
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 16
    avgRank 41.592592592592595
    avgPoints 1216.1851851851852
  ]
  node [
    id 55
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 8
    avgRank 70.85185185185185
    avgPoints 816.4814814814815
  ]
  node [
    id 56
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 12
    avgRank 62.851851851851855
    avgPoints 911.925925925926
  ]
  edge [
    source 0
    target 2
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 2
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 12
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 23
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 2
    target 32
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 2
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 4
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 5
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 2
    target 3
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 13
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 26
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 2
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 15
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 2
    target 25
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 2
    target 11
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 6
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 2
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 12
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 2
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 17
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 23
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 6
    target 31
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 17
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 10
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 24
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 26
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 15
    target 23
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 15
    target 17
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 15
    target 32
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 15
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 15
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 26
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 17
    target 32
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 17
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 26
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 23
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 41
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 25
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 32
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 26
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 49
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 52
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 36
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 53
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
]
