graph [
  node [
    id 0
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 12
    avgRank 32.55555555555556
    avgPoints 1467.4444444444443
  ]
  node [
    id 1
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 9
    avgRank 1.1111111111111112
    avgPoints 10940.185185185184
  ]
  node [
    id 2
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 13
    avgRank 54.96296296296296
    avgPoints 978.3333333333334
  ]
  node [
    id 3
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 16
    avgRank 41.592592592592595
    avgPoints 1216.1851851851852
  ]
  node [
    id 4
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 1
    avgRank 4.037037037037037
    avgPoints 6699.62962962963
  ]
  node [
    id 5
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 8
    avgRank 70.85185185185185
    avgPoints 816.4814814814815
  ]
  node [
    id 6
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 12
    avgRank 62.851851851851855
    avgPoints 911.925925925926
  ]
  edge [
    source 0
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 1
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 4
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 2
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
]
