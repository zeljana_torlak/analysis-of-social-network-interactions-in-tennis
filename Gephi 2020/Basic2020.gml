graph [
  node [
    id 0
    label "110602"
    name "Dennis"
    surname "Novak"
    country "AUT"
    hand "R"
    dateOfBirth 19930828.0
    tourneyNum 12
    avgRank 93.37037037037037
    avgPoints 683.6296296296297
  ]
  node [
    id 1
    label "104467"
    name "Lamine"
    surname "Ouahab"
    country "ALG"
    hand "R"
    dateOfBirth 19841222.0
    tourneyNum 1
    avgRank 671.4074074074074
    avgPoints 30.185185185185187
  ]
  node [
    id 2
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 13
    avgRank 58.03703703703704
    avgPoints 964.6296296296297
  ]
  node [
    id 3
    label "206909"
    name "Brandon"
    surname "Nakashima"
    country "USA"
    hand "U"
    dateOfBirth 20010803.0
    tourneyNum 2
    avgRank 228.88888888888889
    avgPoints 255.96296296296296
  ]
  node [
    id 4
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 8
    avgRank 16.814814814814813
    avgPoints 2200.925925925926
  ]
  node [
    id 5
    label "110686"
    name "Finn"
    surname "Tearney"
    country "NZL"
    hand "R"
    dateOfBirth 19900927.0
    tourneyNum 1
    avgRank 964.8518518518518
    avgPoints 11.555555555555555
  ]
  node [
    id 6
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 2
    avgRank 51.666666666666664
    avgPoints 1055.9259259259259
  ]
  node [
    id 7
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 8
    avgRank 22.185185185185187
    avgPoints 1827.7777777777778
  ]
  node [
    id 8
    label "104586"
    name "Lukas"
    surname "Rosol"
    country "CZE"
    hand "R"
    dateOfBirth 19850724.0
    tourneyNum 2
    avgRank 183.11111111111111
    avgPoints 310.77777777777777
  ]
  node [
    id 9
    label "110748"
    name "Nikola"
    surname "Milojevic"
    country "SRB"
    hand "U"
    dateOfBirth 19950619.0
    tourneyNum 2
    avgRank 142.03703703703704
    avgPoints 415.5925925925926
  ]
  node [
    id 10
    label "127143"
    name "Jirat"
    surname "Navasirisomboon"
    country "THA"
    hand "R"
    dateOfBirth 19961117.0
    tourneyNum 1
    avgRank 1561.5925925925926
    avgPoints 2.0
  ]
  node [
    id 11
    label "127157"
    name "Daniel"
    surname "Altmaier"
    country "GER"
    hand "U"
    dateOfBirth 19980912.0
    tourneyNum 3
    avgRank 180.77777777777777
    avgPoints 348.2962962962963
  ]
  node [
    id 12
    label "209080"
    name "Blaise"
    surname "Bicknell"
    country "USA"
    hand "U"
    dateOfBirth 20011126.0
    tourneyNum 1
    avgRank 1331.2962962962963
    avgPoints 3.0
  ]
  node [
    id 13
    label "108739"
    name "Wilfredo"
    surname "Gonzalez"
    country "GUA"
    hand "R"
    dateOfBirth 19930106.0
    tourneyNum 1
    avgRank 1100.962962962963
    avgPoints 7.0
  ]
  node [
    id 14
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 12
    avgRank 60.111111111111114
    avgPoints 930.3333333333334
  ]
  node [
    id 15
    label "104656"
    name "Haydn"
    surname "Lewis"
    country "BAR"
    hand "L"
    dateOfBirth 19860102.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 16
    label "104660"
    name "Sergiy"
    surname "Stakhovsky"
    country "UKR"
    hand "R"
    dateOfBirth 19860106.0
    tourneyNum 2
    avgRank 179.03703703703704
    avgPoints 321.7037037037037
  ]
  node [
    id 17
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 12
    avgRank 57.44444444444444
    avgPoints 968.8888888888889
  ]
  node [
    id 18
    label "108763"
    name "Alberto Emmanuel"
    surname "Alvarado Larin"
    country "ESA"
    hand "U"
    dateOfBirth 19941201.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 19
    label "209119"
    name "Aissa"
    surname "Benchakroun"
    country "MAR"
    hand "U"
    dateOfBirth 20020325.0
    tourneyNum 1
    avgRank 1696.7037037037037
    avgPoints 1.0
  ]
  node [
    id 20
    label "108772"
    name "Julius"
    surname "Tverijonas"
    country "LTU"
    hand "R"
    dateOfBirth 19940614.0
    tourneyNum 1
    avgRank 808.1851851851852
    avgPoints 19.0
  ]
  node [
    id 21
    label "104678"
    name "Viktor"
    surname "Troicki"
    country "SRB"
    hand "R"
    dateOfBirth 19860210.0
    tourneyNum 3
    avgRank 187.07407407407408
    avgPoints 303.0740740740741
  ]
  node [
    id 22
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 10
    avgRank 107.44444444444444
    avgPoints 592.5925925925926
  ]
  node [
    id 23
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 10
    avgRank 7.0
    avgPoints 4496.666666666667
  ]
  node [
    id 24
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 7
    avgRank 1.8888888888888888
    avgPoints 9816.111111111111
  ]
  node [
    id 25
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 10
    avgRank 51.81481481481482
    avgPoints 1019.2592592592592
  ]
  node [
    id 26
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 14
    avgRank 13.444444444444445
    avgPoints 2470.5555555555557
  ]
  node [
    id 27
    label "209225"
    name "Martin Antonio"
    surname "Vergara Del Puerto"
    country "PAR"
    hand "U"
    dateOfBirth 20040421.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 28
    label "209226"
    name "Adolfo Daniel"
    surname "Vallejo"
    country "PAR"
    hand "U"
    dateOfBirth 20040428.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 29
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 9
    avgRank 9.962962962962964
    avgPoints 2820.0
  ]
  node [
    id 30
    label "104797"
    name "Denis"
    surname "Istomin"
    country "UZB"
    hand "R"
    dateOfBirth 19860907.0
    tourneyNum 2
    avgRank 171.59259259259258
    avgPoints 333.22222222222223
  ]
  node [
    id 31
    label "127326"
    name "Ben"
    surname "Fatael"
    country "ISR"
    hand "U"
    dateOfBirth 19970602.0
    tourneyNum 1
    avgRank 559.5185185185185
    avgPoints 47.888888888888886
  ]
  node [
    id 32
    label "127339"
    name "Borna"
    surname "Gojo"
    country "CRO"
    hand "U"
    dateOfBirth 19980227.0
    tourneyNum 1
    avgRank 252.8148148148148
    avgPoints 199.96296296296296
  ]
  node [
    id 33
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 5
    avgRank 66.07407407407408
    avgPoints 867.5925925925926
  ]
  node [
    id 34
    label "104882"
    name "Blaz"
    surname "Kavcic"
    country "SLO"
    hand "R"
    dateOfBirth 19870305.0
    tourneyNum 1
    avgRank 281.44444444444446
    avgPoints 168.5185185185185
  ]
  node [
    id 35
    label "104898"
    name "Robin"
    surname "Haase"
    country "NED"
    hand "R"
    dateOfBirth 19870406.0
    tourneyNum 2
    avgRank 177.55555555555554
    avgPoints 321.55555555555554
  ]
  node [
    id 36
    label "104901"
    name "Vladimir"
    surname "Ivanov"
    country "EST"
    hand "R"
    dateOfBirth 19870410.0
    tourneyNum 1
    avgRank 731.3703703703703
    avgPoints 25.0
  ]
  node [
    id 37
    label "104907"
    name "Jose Rubin"
    surname "Statham"
    country "NZL"
    hand "R"
    dateOfBirth 19870425.0
    tourneyNum 1
    avgRank 769.0
    avgPoints 20.0
  ]
  node [
    id 38
    label "104918"
    name "Andy"
    surname "Murray"
    country "GBR"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 4
    avgRank 122.33333333333333
    avgPoints 491.44444444444446
  ]
  node [
    id 39
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 8
    avgRank 120.96296296296296
    avgPoints 494.18518518518516
  ]
  node [
    id 40
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 9
    avgRank 1.1111111111111112
    avgPoints 10940.185185185184
  ]
  node [
    id 41
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 9
    avgRank 14.037037037037036
    avgPoints 2389.259259259259
  ]
  node [
    id 42
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 2
    avgRank 263.6296296296296
    avgPoints 187.74074074074073
  ]
  node [
    id 43
    label "207401"
    name "Sheil"
    surname "Kotecha"
    country "KEN"
    hand "R"
    dateOfBirth 20000328.0
    tourneyNum 1
    avgRank 1402.7777777777778
    avgPoints 3.0
  ]
  node [
    id 44
    label "111153"
    name "Christopher"
    surname "Eubanks"
    country "USA"
    hand "R"
    dateOfBirth 19960505.0
    tourneyNum 1
    avgRank 233.40740740740742
    avgPoints 222.66666666666666
  ]
  node [
    id 45
    label "105011"
    name "Illya"
    surname "Marchenko"
    country "UKR"
    hand "R"
    dateOfBirth 19870908.0
    tourneyNum 2
    avgRank 221.37037037037038
    avgPoints 242.44444444444446
  ]
  node [
    id 46
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 6
    avgRank 47.592592592592595
    avgPoints 1073.3333333333333
  ]
  node [
    id 47
    label "105030"
    name "Michael"
    surname "Venus"
    country "NZL"
    hand "R"
    dateOfBirth 19871016.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 48
    label "105053"
    name "Santiago"
    surname "Giraldo"
    country "COL"
    hand "R"
    dateOfBirth 19871127.0
    tourneyNum 1
    avgRank 300.1111111111111
    avgPoints 142.44444444444446
  ]
  node [
    id 49
    label "111200"
    name "Elias"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 1
    avgRank 194.40740740740742
    avgPoints 289.5925925925926
  ]
  node [
    id 50
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 10
    avgRank 84.25925925925925
    avgPoints 748.5925925925926
  ]
  node [
    id 51
    label "117356"
    name "Cem"
    surname "Ilkel"
    country "TUR"
    hand "R"
    dateOfBirth 19950821.0
    tourneyNum 2
    avgRank 215.92592592592592
    avgPoints 245.4814814814815
  ]
  node [
    id 52
    label "117360"
    name "Marc"
    surname "Polmans"
    country "AUS"
    hand "U"
    dateOfBirth 19970502.0
    tourneyNum 4
    avgRank 122.18518518518519
    avgPoints 490.6296296296296
  ]
  node [
    id 53
    label "105074"
    name "Ruben"
    surname "Bemelmans"
    country "BEL"
    hand "L"
    dateOfBirth 19880114.0
    tourneyNum 1
    avgRank 229.40740740740742
    avgPoints 230.03703703703704
  ]
  node [
    id 54
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 14
    avgRank 43.74074074074074
    avgPoints 1143.888888888889
  ]
  node [
    id 55
    label "207518"
    name "Lorenzo"
    surname "Musetti"
    country "ITA"
    hand "R"
    dateOfBirth 20020303.0
    tourneyNum 3
    avgRank 215.62962962962962
    avgPoints 317.6296296296296
  ]
  node [
    id 56
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 9
    avgRank 11.925925925925926
    avgPoints 2570.5555555555557
  ]
  node [
    id 57
    label "105147"
    name "Tatsuma"
    surname "Ito"
    country "JPN"
    hand "R"
    dateOfBirth 19880518.0
    tourneyNum 1
    avgRank 166.96296296296296
    avgPoints 346.962962962963
  ]
  node [
    id 58
    label "105155"
    name "Pedro"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19880527.0
    tourneyNum 4
    avgRank 115.5925925925926
    avgPoints 545.5925925925926
  ]
  node [
    id 59
    label "105156"
    name "Federico"
    surname "Zeballos"
    country "BOL"
    hand "U"
    dateOfBirth 19880530.0
    tourneyNum 1
    avgRank 554.7407407407408
    avgPoints 49.55555555555556
  ]
  node [
    id 60
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 17
    avgRank 38.77777777777778
    avgPoints 1288.4074074074074
  ]
  node [
    id 61
    label "105208"
    name "Ernests"
    surname "Gulbis"
    country "LAT"
    hand "R"
    dateOfBirth 19880830.0
    tourneyNum 3
    avgRank 185.8148148148148
    avgPoints 311.3703703703704
  ]
  node [
    id 62
    label "207608"
    name "Timofey"
    surname "Skatov"
    country "RUS"
    hand "R"
    dateOfBirth 20010121.0
    tourneyNum 1
    avgRank 489.2962962962963
    avgPoints 66.51851851851852
  ]
  node [
    id 63
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 6
    avgRank 94.03703703703704
    avgPoints 683.5925925925926
  ]
  node [
    id 64
    label "105226"
    name "Attila"
    surname "Balazs"
    country "HUN"
    hand "R"
    dateOfBirth 19880927.0
    tourneyNum 6
    avgRank 92.33333333333333
    avgPoints 688.4444444444445
  ]
  node [
    id 65
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 13
    avgRank 39.592592592592595
    avgPoints 1259.8148148148148
  ]
  node [
    id 66
    label "207669"
    name "Robert"
    surname "Strombachs"
    country "GER"
    hand "U"
    dateOfBirth 19990915.0
    tourneyNum 1
    avgRank 667.8518518518518
    avgPoints 31.0
  ]
  node [
    id 67
    label "207680"
    name "Facundo"
    surname "Diaz Acosta"
    country "ARG"
    hand "U"
    dateOfBirth 20001215.0
    tourneyNum 1
    avgRank 431.9259259259259
    avgPoints 82.70370370370371
  ]
  node [
    id 68
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 13
    avgRank 58.44444444444444
    avgPoints 943.6666666666666
  ]
  node [
    id 69
    label "133975"
    name "Benjamin"
    surname "Hassan"
    country "GER"
    hand "U"
    dateOfBirth 19950204.0
    tourneyNum 1
    avgRank 351.51851851851853
    avgPoints 112.37037037037037
  ]
  node [
    id 70
    label "133979"
    name "Hasan"
    surname "Ibrahim"
    country "GER"
    hand "U"
    dateOfBirth 19950817.0
    tourneyNum 1
    avgRank 1523.0740740740741
    avgPoints 2.0
  ]
  node [
    id 71
    label "111453"
    name "Mikael"
    surname "Torpegaard"
    country "DEN"
    hand "R"
    dateOfBirth 19940508.0
    tourneyNum 1
    avgRank 188.11111111111111
    avgPoints 301.962962962963
  ]
  node [
    id 72
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 10
    avgRank 76.29629629629629
    avgPoints 792.6666666666666
  ]
  node [
    id 73
    label "111456"
    name "Mackenzie"
    surname "Mcdonald"
    country "USA"
    hand "R"
    dateOfBirth 19950416.0
    tourneyNum 8
    avgRank 202.37037037037038
    avgPoints 290.0740740740741
  ]
  node [
    id 74
    label "111459"
    name "Arjun"
    surname "Kadhe"
    country "IND"
    hand "R"
    dateOfBirth 19940107.0
    tourneyNum 1
    avgRank 653.2592592592592
    avgPoints 32.7037037037037
  ]
  node [
    id 75
    label "111460"
    name "Quentin"
    surname "Halys"
    country "FRA"
    hand "R"
    dateOfBirth 19961026.0
    tourneyNum 2
    avgRank 203.07407407407408
    avgPoints 272.3333333333333
  ]
  node [
    id 76
    label "125802"
    name "Ilya"
    surname "Ivashka"
    country "BLR"
    hand "R"
    dateOfBirth 19940224.0
    tourneyNum 5
    avgRank 128.62962962962962
    avgPoints 487.74074074074076
  ]
  node [
    id 77
    label "123755"
    name "Daniel Elahi"
    surname "Galan Riveros"
    country "COL"
    hand "U"
    dateOfBirth 19960618.0
    tourneyNum 4
    avgRank 141.55555555555554
    avgPoints 432.85185185185185
  ]
  node [
    id 78
    label "131951"
    name "Daniel"
    surname "Cukierman"
    country "ISR"
    hand "U"
    dateOfBirth 19950709.0
    tourneyNum 1
    avgRank 805.7777777777778
    avgPoints 18.814814814814813
  ]
  node [
    id 79
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 13
    avgRank 24.703703703703702
    avgPoints 1724.111111111111
  ]
  node [
    id 80
    label "105334"
    name "Luis David"
    surname "Martinez"
    country "VEN"
    hand "U"
    dateOfBirth 19890512.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 81
    label "207738"
    name "Brandon"
    surname "Perez"
    country "VEN"
    hand "U"
    dateOfBirth 20000114.0
    tourneyNum 1
    avgRank 1696.7037037037037
    avgPoints 1.0
  ]
  node [
    id 82
    label "105341"
    name "Thomas"
    surname "Fabbiano"
    country "ITA"
    hand "R"
    dateOfBirth 19890526.0
    tourneyNum 1
    avgRank 151.11111111111111
    avgPoints 393.18518518518516
  ]
  node [
    id 83
    label "205695"
    name "Ching"
    surname "Lam"
    country "HKG"
    hand "R"
    dateOfBirth 19990903.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 84
    label "105349"
    name "Harri"
    surname "Heliovaara"
    country "FIN"
    hand "R"
    dateOfBirth 19890604.0
    tourneyNum 1
    avgRank 617.4814814814815
    avgPoints 37.0
  ]
  node [
    id 85
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 16
    avgRank 41.592592592592595
    avgPoints 1216.1851851851852
  ]
  node [
    id 86
    label "105359"
    name "Jason"
    surname "Jung"
    country "TPE"
    hand "R"
    dateOfBirth 19890615.0
    tourneyNum 6
    avgRank 126.33333333333333
    avgPoints 465.51851851851853
  ]
  node [
    id 87
    label "111505"
    name "Enzo"
    surname "Couacaud"
    country "FRA"
    hand "R"
    dateOfBirth 19950301.0
    tourneyNum 1
    avgRank 198.88888888888889
    avgPoints 276.18518518518516
  ]
  node [
    id 88
    label "125842"
    name "Adam"
    surname "Moundir"
    country "SUI"
    hand "U"
    dateOfBirth 19950426.0
    tourneyNum 1
    avgRank 472.037037037037
    avgPoints 69.0
  ]
  node [
    id 89
    label "123795"
    name "Altug"
    surname "Celikbilek"
    country "TUR"
    hand "U"
    dateOfBirth 19960907.0
    tourneyNum 1
    avgRank 324.14814814814815
    avgPoints 127.33333333333333
  ]
  node [
    id 90
    label "111511"
    name "Noah"
    surname "Rubin"
    country "USA"
    hand "R"
    dateOfBirth 19960221.0
    tourneyNum 1
    avgRank 244.4814814814815
    avgPoints 206.14814814814815
  ]
  node [
    id 91
    label "111513"
    name "Laslo"
    surname "Djere"
    country "SRB"
    hand "R"
    dateOfBirth 19950602.0
    tourneyNum 10
    avgRank 59.407407407407405
    avgPoints 974.7777777777778
  ]
  node [
    id 92
    label "111515"
    name "Thai Son"
    surname "Kwiatkowski"
    country "USA"
    hand "U"
    dateOfBirth 19950213.0
    tourneyNum 1
    avgRank 205.44444444444446
    avgPoints 267.74074074074076
  ]
  node [
    id 93
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 2
    avgRank 154.66666666666666
    avgPoints 374.1111111111111
  ]
  node [
    id 94
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 3
    avgRank 129.92592592592592
    avgPoints 461.962962962963
  ]
  node [
    id 95
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 12
    avgRank 56.074074074074076
    avgPoints 977.4074074074074
  ]
  node [
    id 96
    label "103333"
    name "Ivo"
    surname "Karlovic"
    country "CRO"
    hand "R"
    dateOfBirth 19790228.0
    tourneyNum 4
    avgRank 133.5185185185185
    avgPoints 441.9259259259259
  ]
  node [
    id 97
    label "205734"
    name "Thiago"
    surname "Seyboth Wild"
    country "BRA"
    hand "R"
    dateOfBirth 20000310.0
    tourneyNum 4
    avgRank 135.96296296296296
    avgPoints 481.9259259259259
  ]
  node [
    id 98
    label "207797"
    name "Jonas"
    surname "Forejtek"
    country "CZE"
    hand "R"
    dateOfBirth 20010310.0
    tourneyNum 1
    avgRank 420.74074074074076
    avgPoints 87.33333333333333
  ]
  node [
    id 99
    label "105406"
    name "Roberto"
    surname "Marcora"
    country "ITA"
    hand "R"
    dateOfBirth 19890830.0
    tourneyNum 1
    avgRank 171.85185185185185
    avgPoints 332.7037037037037
  ]
  node [
    id 100
    label "105413"
    name "Andrej"
    surname "Martin"
    country "SVK"
    hand "R"
    dateOfBirth 19890920.0
    tourneyNum 9
    avgRank 100.11111111111111
    avgPoints 650.925925925926
  ]
  node [
    id 101
    label "209866"
    name "Karlis"
    surname "Ozolins"
    country "LVA"
    hand "U"
    dateOfBirth 20020615.0
    tourneyNum 1
    avgRank 1088.2105263157894
    avgPoints 7.157894736842105
  ]
  node [
    id 102
    label "209867"
    name "Daniil"
    surname "Ostapenkov"
    country "BLR"
    hand "U"
    dateOfBirth 20030508.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 103
    label "209868"
    name "Kaipo"
    surname "Marshall"
    country "BAR"
    hand "U"
    dateOfBirth 20020321.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 104
    label "209869"
    name "Rodrigo"
    surname "Crespo Piedra"
    country "CRC"
    hand "U"
    dateOfBirth 20010531.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 105
    label "209870"
    name "Gunawan"
    surname "Trismuwantara"
    country "INA"
    hand "U"
    dateOfBirth 20030109.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 106
    label "209871"
    name "Amr Elsayed Abdou Ahmed"
    surname "Mohamed"
    country "EGY"
    hand "U"
    dateOfBirth 19990304.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 107
    label "209872"
    name "Thehan Sanjaya"
    surname "Wijemanne"
    country "SRI"
    hand "U"
    dateOfBirth 20031221.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 108
    label "209873"
    name "Eric Jr."
    surname "Olivarez"
    country "PHI"
    hand "U"
    dateOfBirth 19980418.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 109
    label "209874"
    name "Maks"
    surname "Kasnikowski"
    country "POL"
    hand "U"
    dateOfBirth 20030706.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 110
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 11
    avgRank 74.62962962962963
    avgPoints 830.8888888888889
  ]
  node [
    id 111
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 14
    avgRank 17.555555555555557
    avgPoints 2147.962962962963
  ]
  node [
    id 112
    label "105432"
    name "Prajnesh"
    surname "Gunneswaran"
    country "IND"
    hand "L"
    dateOfBirth 19891112.0
    tourneyNum 4
    avgRank 133.77777777777777
    avgPoints 438.8888888888889
  ]
  node [
    id 113
    label "111576"
    name "Sumit"
    surname "Nagal"
    country "IND"
    hand "R"
    dateOfBirth 19970816.0
    tourneyNum 5
    avgRank 129.96296296296296
    avgPoints 450.2962962962963
  ]
  node [
    id 114
    label "207830"
    name "Tomas"
    surname "Machac"
    country "CZE"
    hand "R"
    dateOfBirth 20001013.0
    tourneyNum 1
    avgRank 252.44444444444446
    avgPoints 223.88888888888889
  ]
  node [
    id 115
    label "105434"
    name "Ruan"
    surname "Roelofse"
    country "RSA"
    hand "R"
    dateOfBirth 19891118.0
    tourneyNum 1
    avgRank 633.2962962962963
    avgPoints 34.7037037037037
  ]
  node [
    id 116
    label "111578"
    name "Stefan"
    surname "Kozlov"
    country "USA"
    hand "R"
    dateOfBirth 19980201.0
    tourneyNum 1
    avgRank 377.8888888888889
    avgPoints 100.70370370370371
  ]
  node [
    id 117
    label "111581"
    name "Michael"
    surname "Mmoh"
    country "USA"
    hand "R"
    dateOfBirth 19980110.0
    tourneyNum 4
    avgRank 182.96296296296296
    avgPoints 313.0740740740741
  ]
  node [
    id 118
    label "105441"
    name "John Patrick"
    surname "Smith"
    country "AUS"
    hand "L"
    dateOfBirth 19891204.0
    tourneyNum 1
    avgRank 304.6666666666667
    avgPoints 138.77777777777777
  ]
  node [
    id 119
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 8
    avgRank 70.85185185185185
    avgPoints 816.4814814814815
  ]
  node [
    id 120
    label "207852"
    name "Wai Yu"
    surname "Kai"
    country "HKG"
    hand "U"
    dateOfBirth 20010320.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 121
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 5
    avgRank 33.370370370370374
    avgPoints 1430.0
  ]
  node [
    id 122
    label "105464"
    name "Christopher"
    surname "Rungkat"
    country "INA"
    hand "R"
    dateOfBirth 19900114.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 123
    label "105467"
    name "Jordi"
    surname "Munoz Abreu"
    country "VEN"
    hand "U"
    dateOfBirth 19900117.0
    tourneyNum 1
    avgRank 1246.2592592592594
    avgPoints 4.0
  ]
  node [
    id 124
    label "105477"
    name "Marco"
    surname "Trungelliti"
    country "ARG"
    hand "R"
    dateOfBirth 19900131.0
    tourneyNum 1
    avgRank 230.14814814814815
    avgPoints 223.96296296296296
  ]
  node [
    id 125
    label "105487"
    name "Facundo"
    surname "Bagnis"
    country "ARG"
    hand "L"
    dateOfBirth 19900227.0
    tourneyNum 4
    avgRank 128.62962962962962
    avgPoints 464.51851851851853
  ]
  node [
    id 126
    label "105497"
    name "Jose"
    surname "Hernandez"
    country "DOM"
    hand "U"
    dateOfBirth 19900313.0
    tourneyNum 1
    avgRank 299.25925925925924
    avgPoints 142.33333333333334
  ]
  node [
    id 127
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 14
    avgRank 31.77777777777778
    avgPoints 1439.2962962962963
  ]
  node [
    id 128
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 16
    avgRank 34.333333333333336
    avgPoints 1380.7407407407406
  ]
  node [
    id 129
    label "105539"
    name "Evgeny"
    surname "Donskoy"
    country "RUS"
    hand "R"
    dateOfBirth 19900509.0
    tourneyNum 4
    avgRank 116.77777777777777
    avgPoints 518.2592592592592
  ]
  node [
    id 130
    label "103499"
    name "Aqeel"
    surname "Khan"
    country "PAK"
    hand "R"
    dateOfBirth 19800130.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 131
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 9
    avgRank 35.51851851851852
    avgPoints 1376.111111111111
  ]
  node [
    id 132
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 14
    avgRank 32.407407407407405
    avgPoints 1424.962962962963
  ]
  node [
    id 133
    label "105575"
    name "Ricardas"
    surname "Berankis"
    country "LTU"
    hand "R"
    dateOfBirth 19900621.0
    tourneyNum 7
    avgRank 68.11111111111111
    avgPoints 839.7407407407408
  ]
  node [
    id 134
    label "105577"
    name "Vasek"
    surname "Pospisil"
    country "CAN"
    hand "R"
    dateOfBirth 19900623.0
    tourneyNum 10
    avgRank 87.48148148148148
    avgPoints 739.4074074074074
  ]
  node [
    id 135
    label "103529"
    name "Aisam Ul Haq"
    surname "Qureshi"
    country "PAK"
    hand "R"
    dateOfBirth 19800317.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 136
    label "124014"
    name "Ernesto"
    surname "Escobedo"
    country "USA"
    hand "R"
    dateOfBirth 19960704.0
    tourneyNum 1
    avgRank 194.33333333333334
    avgPoints 293.48148148148147
  ]
  node [
    id 137
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 15
    avgRank 25.185185185185187
    avgPoints 1700.037037037037
  ]
  node [
    id 138
    label "207989"
    name "Carlos"
    surname "Alcaraz Garfia"
    country "ESP"
    hand "U"
    dateOfBirth 20030505.0
    tourneyNum 1
    avgRank 248.37037037037038
    avgPoints 284.6666666666667
  ]
  node [
    id 139
    label "208004"
    name "Harold"
    surname "Mayot"
    country "FRA"
    hand "U"
    dateOfBirth 20020204.0
    tourneyNum 2
    avgRank 461.0740740740741
    avgPoints 74.51851851851852
  ]
  node [
    id 140
    label "105613"
    name "Norbert"
    surname "Gombos"
    country "SVK"
    hand "R"
    dateOfBirth 19900813.0
    tourneyNum 8
    avgRank 100.81481481481481
    avgPoints 634.2592592592592
  ]
  node [
    id 141
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 14
    avgRank 11.88888888888889
    avgPoints 2926.222222222222
  ]
  node [
    id 142
    label "103565"
    name "Stephane"
    surname "Robert"
    country "FRA"
    hand "R"
    dateOfBirth 19800517.0
    tourneyNum 1
    avgRank 843.4814814814815
    avgPoints 17.296296296296298
  ]
  node [
    id 143
    label "105614"
    name "Bradley"
    surname "Klahn"
    country "USA"
    hand "L"
    dateOfBirth 19900820.0
    tourneyNum 1
    avgRank 136.25925925925927
    avgPoints 433.22222222222223
  ]
  node [
    id 144
    label "111761"
    name "Benjamin"
    surname "Lock"
    country "ZIM"
    hand "R"
    dateOfBirth 19930304.0
    tourneyNum 1
    avgRank 437.81481481481484
    avgPoints 80.07407407407408
  ]
  node [
    id 145
    label "208029"
    name "Holger Vitus Nodskov"
    surname "Rune"
    country "DEN"
    hand "U"
    dateOfBirth 20030429.0
    tourneyNum 1
    avgRank 690.7407407407408
    avgPoints 37.2962962962963
  ]
  node [
    id 146
    label "105633"
    name "Mohamed"
    surname "Safwat"
    country "EGY"
    hand "R"
    dateOfBirth 19900919.0
    tourneyNum 4
    avgRank 147.92592592592592
    avgPoints 393.8888888888889
  ]
  node [
    id 147
    label "126120"
    name "Nino"
    surname "Serdarusic"
    country "CRO"
    hand "U"
    dateOfBirth 19961213.0
    tourneyNum 1
    avgRank 295.037037037037
    avgPoints 150.55555555555554
  ]
  node [
    id 148
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 12
    avgRank 79.55555555555556
    avgPoints 761.5185185185185
  ]
  node [
    id 149
    label "109739"
    name "Maximilian"
    surname "Marterer"
    country "GER"
    hand "L"
    dateOfBirth 19950615.0
    tourneyNum 1
    avgRank 280.77777777777777
    avgPoints 188.92592592592592
  ]
  node [
    id 150
    label "126125"
    name "Maxime"
    surname "Janvier"
    country "FRA"
    hand "U"
    dateOfBirth 19961018.0
    tourneyNum 1
    avgRank 199.22222222222223
    avgPoints 276.14814814814815
  ]
  node [
    id 151
    label "111790"
    name "Brayden"
    surname "Schnur"
    country "CAN"
    hand "R"
    dateOfBirth 19950704.0
    tourneyNum 1
    avgRank 179.03703703703704
    avgPoints 331.25925925925924
  ]
  node [
    id 152
    label "124079"
    name "Pedro"
    surname "Martinez Portero"
    country "ESP"
    hand "R"
    dateOfBirth 19970426.0
    tourneyNum 8
    avgRank 108.66666666666667
    avgPoints 605.4074074074074
  ]
  node [
    id 153
    label "126127"
    name "Benjamin"
    surname "Bonzi"
    country "FRA"
    hand "U"
    dateOfBirth 19960609.0
    tourneyNum 2
    avgRank 229.1851851851852
    avgPoints 263.8888888888889
  ]
  node [
    id 154
    label "105649"
    name "Cedrik Marcel"
    surname "Stebe"
    country "GER"
    hand "L"
    dateOfBirth 19901009.0
    tourneyNum 3
    avgRank 136.0
    avgPoints 439.25925925925924
  ]
  node [
    id 155
    label "126128"
    name "Roman"
    surname "Safiullin"
    country "RUS"
    hand "U"
    dateOfBirth 19970807.0
    tourneyNum 1
    avgRank 192.59259259259258
    avgPoints 294.9259259259259
  ]
  node [
    id 156
    label "111794"
    name "Kamil"
    surname "Majchrzak"
    country "POL"
    hand "R"
    dateOfBirth 19960113.0
    tourneyNum 4
    avgRank 104.62962962962963
    avgPoints 610.0370370370371
  ]
  node [
    id 157
    label "208043"
    name "Ismael Changawa Ruwa"
    surname "Mzai"
    country "KEN"
    hand "L"
    dateOfBirth 19960626.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 158
    label "111797"
    name "Nicolas"
    surname "Jarry"
    country "CHI"
    hand "R"
    dateOfBirth 19951011.0
    tourneyNum 1
    avgRank 420.75
    avgPoints 424.25
  ]
  node [
    id 159
    label "208055"
    name "Ajeet"
    surname "Rai"
    country "NZL"
    hand "R"
    dateOfBirth 19990118.0
    tourneyNum 1
    avgRank 940.2222222222222
    avgPoints 12.518518518518519
  ]
  node [
    id 160
    label "105656"
    name "Marcelo"
    surname "Arevalo"
    country "ESA"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 1
    avgRank 415.74074074074076
    avgPoints 86.44444444444444
  ]
  node [
    id 161
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 14
    avgRank 42.44444444444444
    avgPoints 1225.7777777777778
  ]
  node [
    id 162
    label "105668"
    name "Jerzy"
    surname "Janowicz"
    country "POL"
    hand "R"
    dateOfBirth 19901113.0
    tourneyNum 1
    avgRank 576.6666666666666
    avgPoints 58.0
  ]
  node [
    id 163
    label "126149"
    name "Gianluca"
    surname "Mager"
    country "ITA"
    hand "R"
    dateOfBirth 19941201.0
    tourneyNum 6
    avgRank 98.0
    avgPoints 656.5185185185185
  ]
  node [
    id 164
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 12
    avgRank 68.92592592592592
    avgPoints 841.5555555555555
  ]
  node [
    id 165
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 10
    avgRank 12.407407407407407
    avgPoints 2550.185185185185
  ]
  node [
    id 166
    label "126156"
    name "Antonie"
    surname "Hoang"
    country "FRA"
    hand "R"
    dateOfBirth 19951104.0
    tourneyNum 3
    avgRank 126.70370370370371
    avgPoints 469.81481481481484
  ]
  node [
    id 167
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 10
    avgRank 22.925925925925927
    avgPoints 1925.5555555555557
  ]
  node [
    id 168
    label "124116"
    name "Sebastian"
    surname "Ofner"
    country "AUT"
    hand "R"
    dateOfBirth 19960512.0
    tourneyNum 1
    avgRank 161.44444444444446
    avgPoints 357.1111111111111
  ]
  node [
    id 169
    label "105688"
    name "Manuel"
    surname "Sanchez"
    country "MEX"
    hand "R"
    dateOfBirth 19910105.0
    tourneyNum 1
    avgRank 835.1851851851852
    avgPoints 16.77777777777778
  ]
  node [
    id 170
    label "122078"
    name "Dmitry"
    surname "Popko"
    country "KAZ"
    hand "R"
    dateOfBirth 19961024.0
    tourneyNum 1
    avgRank 173.11111111111111
    avgPoints 330.18518518518516
  ]
  node [
    id 171
    label "208108"
    name "Brian"
    surname "Shi"
    country "USA"
    hand "U"
    dateOfBirth 20000224.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 172
    label "126190"
    name "Nicolas"
    surname "Alvarez"
    country "PER"
    hand "R"
    dateOfBirth 19960608.0
    tourneyNum 1
    avgRank 335.74074074074076
    avgPoints 120.5925925925926
  ]
  node [
    id 173
    label "136440"
    name "Dominik"
    surname "Koepfer"
    country "GER"
    hand "L"
    dateOfBirth 19940429.0
    tourneyNum 7
    avgRank 78.0
    avgPoints 777.2592592592592
  ]
  node [
    id 174
    label "124154"
    name "Sasi Kumar"
    surname "Mukund"
    country "IND"
    hand "R"
    dateOfBirth 19970114.0
    tourneyNum 1
    avgRank 286.77777777777777
    avgPoints 156.25925925925927
  ]
  node [
    id 175
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 15
    avgRank 29.48148148148148
    avgPoints 1537.2222222222222
  ]
  node [
    id 176
    label "105723"
    name "Andrey"
    surname "Kuznetsov"
    country "RUS"
    hand "R"
    dateOfBirth 19910222.0
    tourneyNum 1
    avgRank 525.3333333333334
    avgPoints 58.53333333333333
  ]
  node [
    id 177
    label "126205"
    name "Tommy"
    surname "Paul"
    country "USA"
    hand "R"
    dateOfBirth 19970517.0
    tourneyNum 14
    avgRank 61.51851851851852
    avgPoints 917.8518518518518
  ]
  node [
    id 178
    label "126204"
    name "Gerardo"
    surname "Lopez Villasenor"
    country "MEX"
    hand "U"
    dateOfBirth 19950512.0
    tourneyNum 2
    avgRank 531.1851851851852
    avgPoints 54.22222222222222
  ]
  node [
    id 179
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 11
    avgRank 66.66666666666667
    avgPoints 889.2592592592592
  ]
  node [
    id 180
    label "122109"
    name "Sanjar"
    surname "Fayziev"
    country "UZB"
    hand "U"
    dateOfBirth 19940729.0
    tourneyNum 1
    avgRank 490.25925925925924
    avgPoints 64.55555555555556
  ]
  node [
    id 181
    label "144642"
    name "Marcelo Tomas"
    surname "Barrios Vera"
    country "CHI"
    hand "U"
    dateOfBirth 19971210.0
    tourneyNum 2
    avgRank 288.55555555555554
    avgPoints 164.33333333333334
  ]
  node [
    id 182
    label "105731"
    name "Steven"
    surname "Diez"
    country "CAN"
    hand "R"
    dateOfBirth 19910317.0
    tourneyNum 1
    avgRank 166.96296296296296
    avgPoints 347.7037037037037
  ]
  node [
    id 183
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 10
    avgRank 76.62962962962963
    avgPoints 786.6666666666666
  ]
  node [
    id 184
    label "126214"
    name "Alejandro"
    surname "Tabilo"
    country "CHI"
    hand "L"
    dateOfBirth 19970602.0
    tourneyNum 3
    avgRank 173.59259259259258
    avgPoints 328.0
  ]
  node [
    id 185
    label "208134"
    name "Luca"
    surname "Nardi"
    country "ITA"
    hand "R"
    dateOfBirth 20030806.0
    tourneyNum 1
    avgRank 1059.5925925925926
    avgPoints 11.481481481481481
  ]
  node [
    id 186
    label "105747"
    name "Karim Mohamed"
    surname "Maamoun"
    country "EGY"
    hand "R"
    dateOfBirth 19910409.0
    tourneyNum 1
    avgRank 379.25925925925924
    avgPoints 101.0
  ]
  node [
    id 187
    label "132374"
    name "Jesse"
    surname "Flores"
    country "CAN"
    hand "U"
    dateOfBirth 19950331.0
    tourneyNum 1
    avgRank 1165.7037037037037
    avgPoints 5.888888888888889
  ]
  node [
    id 188
    label "124187"
    name "Reilly"
    surname "Opelka"
    country "USA"
    hand "R"
    dateOfBirth 19970828.0
    tourneyNum 10
    avgRank 38.22222222222222
    avgPoints 1290.9259259259259
  ]
  node [
    id 189
    label "105757"
    name "Sandro"
    surname "Ehrat"
    country "SUI"
    hand "R"
    dateOfBirth 19910418.0
    tourneyNum 1
    avgRank 369.3703703703704
    avgPoints 104.81481481481481
  ]
  node [
    id 190
    label "126239"
    name "Arthur"
    surname "Rinderknech"
    country "FRA"
    hand "U"
    dateOfBirth 19950723.0
    tourneyNum 1
    avgRank 196.22222222222223
    avgPoints 302.4074074074074
  ]
  node [
    id 191
    label "144687"
    name "Quinton"
    surname "Vega"
    country "USA"
    hand "R"
    dateOfBirth 19921122.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 192
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 10
    avgRank 19.77777777777778
    avgPoints 2000.3333333333333
  ]
  node [
    id 193
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 17
    avgRank 20.814814814814813
    avgPoints 1964.888888888889
  ]
  node [
    id 194
    label "144707"
    name "Mikael"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19980909.0
    tourneyNum 8
    avgRank 81.70370370370371
    avgPoints 752.1111111111111
  ]
  node [
    id 195
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 14
    avgRank 38.592592592592595
    avgPoints 1305.5925925925926
  ]
  node [
    id 196
    label "105806"
    name "Mirza"
    surname "Basic"
    country "BIH"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 1
    avgRank 313.14814814814815
    avgPoints 133.5185185185185
  ]
  node [
    id 197
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 11
    avgRank 21.074074074074073
    avgPoints 1987.2222222222222
  ]
  node [
    id 198
    label "144719"
    name "Jaume"
    surname "Munar"
    country "ESP"
    hand "R"
    dateOfBirth 19970505.0
    tourneyNum 8
    avgRank 104.18518518518519
    avgPoints 602.1481481481482
  ]
  node [
    id 199
    label "202065"
    name "Petros"
    surname "Tsitsipas"
    country "GRE"
    hand "U"
    dateOfBirth 20000727.0
    tourneyNum 1
    avgRank 1147.0
    avgPoints 7.296296296296297
  ]
  node [
    id 200
    label "105812"
    name "David"
    surname "Agung Susanto"
    country "INA"
    hand "U"
    dateOfBirth 19910721.0
    tourneyNum 1
    avgRank 1364.4814814814815
    avgPoints 3.0
  ]
  node [
    id 201
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 13
    avgRank 54.96296296296296
    avgPoints 978.3333333333334
  ]
  node [
    id 202
    label "206173"
    name "Jannik"
    surname "Sinner"
    country "ITA"
    hand "U"
    dateOfBirth 20010816.0
    tourneyNum 12
    avgRank 59.81481481481482
    avgPoints 998.7037037037037
  ]
  node [
    id 203
    label "105827"
    name "Laurynas"
    surname "Grigelis"
    country "LTU"
    hand "R"
    dateOfBirth 19910814.0
    tourneyNum 1
    avgRank 453.6296296296296
    avgPoints 75.07407407407408
  ]
  node [
    id 204
    label "202090"
    name "Adrian"
    surname "Andreev"
    country "BUL"
    hand "U"
    dateOfBirth 20010512.0
    tourneyNum 1
    avgRank 654.1111111111111
    avgPoints 33.44444444444444
  ]
  node [
    id 205
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 11
    avgRank 89.96296296296296
    avgPoints 705.1851851851852
  ]
  node [
    id 206
    label "105841"
    name "Lorenzo"
    surname "Giustino"
    country "ITA"
    hand "R"
    dateOfBirth 19910910.0
    tourneyNum 2
    avgRank 151.14814814814815
    avgPoints 383.6666666666667
  ]
  node [
    id 207
    label "202103"
    name "Francisco"
    surname "Cerundolo"
    country "ARG"
    hand "U"
    dateOfBirth 19980813.0
    tourneyNum 1
    avgRank 208.7037037037037
    avgPoints 282.3333333333333
  ]
  node [
    id 208
    label "206199"
    name "Kevin"
    surname "Cheruiyot"
    country "KEN"
    hand "R"
    dateOfBirth 19980128.0
    tourneyNum 1
    avgRank 1776.5925925925926
    avgPoints 1.0
  ]
  node [
    id 209
    label "126340"
    name "Viktor"
    surname "Durasovic"
    country "NOR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 2
    avgRank 338.51851851851853
    avgPoints 120.18518518518519
  ]
  node [
    id 210
    label "144775"
    name "Linh Giang"
    surname "Trinh"
    country "VIE"
    hand "U"
    dateOfBirth 19970803.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 211
    label "202120"
    name "Rowland"
    surname "Phillips"
    country "JAM"
    hand "U"
    dateOfBirth 19940123.0
    tourneyNum 1
    avgRank 1566.3703703703704
    avgPoints 2.111111111111111
  ]
  node [
    id 212
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 1
    avgRank 4.037037037037037
    avgPoints 6699.62962962963
  ]
  node [
    id 213
    label "105870"
    name "Yannick"
    surname "Hanfmann"
    country "GER"
    hand "U"
    dateOfBirth 19911113.0
    tourneyNum 3
    avgRank 119.92592592592592
    avgPoints 559.074074074074
  ]
  node [
    id 214
    label "105877"
    name "Emilio"
    surname "Gomez"
    country "ECU"
    hand "R"
    dateOfBirth 19911128.0
    tourneyNum 4
    avgRank 154.03703703703704
    avgPoints 376.6666666666667
  ]
  node [
    id 215
    label "105882"
    name "Stefano"
    surname "Travaglia"
    country "ITA"
    hand "U"
    dateOfBirth 19911218.0
    tourneyNum 10
    avgRank 78.18518518518519
    avgPoints 771.8518518518518
  ]
  node [
    id 216
    label "105899"
    name "Martin"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19920114.0
    tourneyNum 2
    avgRank 491.6666666666667
    avgPoints 64.25925925925925
  ]
  node [
    id 217
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 10
    avgRank 59.333333333333336
    avgPoints 951.7037037037037
  ]
  node [
    id 218
    label "105902"
    name "James"
    surname "Duckworth"
    country "AUS"
    hand "R"
    dateOfBirth 19920121.0
    tourneyNum 8
    avgRank 93.03703703703704
    avgPoints 684.1111111111111
  ]
  node [
    id 219
    label "144817"
    name "Marc Andrea"
    surname "Huesler"
    country "SUI"
    hand "U"
    dateOfBirth 19960624.0
    tourneyNum 2
    avgRank 221.92592592592592
    avgPoints 277.14814814814815
  ]
  node [
    id 220
    label "105905"
    name "Danilo"
    surname "Petrovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920124.0
    tourneyNum 2
    avgRank 151.96296296296296
    avgPoints 385.7037037037037
  ]
  node [
    id 221
    label "202165"
    name "Alberto"
    surname "Lim"
    country "PHI"
    hand "R"
    dateOfBirth 19990518.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 222
    label "144826"
    name "Aziz"
    surname "Dougaz"
    country "TUN"
    hand "U"
    dateOfBirth 19970326.0
    tourneyNum 1
    avgRank 387.7037037037037
    avgPoints 98.33333333333333
  ]
  node [
    id 223
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 12
    avgRank 62.851851851851855
    avgPoints 911.925925925926
  ]
  node [
    id 224
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 13
    avgRank 33.074074074074076
    avgPoints 1410.3703703703704
  ]
  node [
    id 225
    label "105933"
    name "Roberto"
    surname "Quiroz"
    country "ECU"
    hand "L"
    dateOfBirth 19920223.0
    tourneyNum 1
    avgRank 281.7037037037037
    avgPoints 165.40740740740742
  ]
  node [
    id 226
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 12
    avgRank 32.55555555555556
    avgPoints 1467.4444444444443
  ]
  node [
    id 227
    label "202195"
    name "Maxime"
    surname "Cressy"
    country "FRA"
    hand "U"
    dateOfBirth 19970508.0
    tourneyNum 1
    avgRank 178.77777777777777
    avgPoints 327.3703703703704
  ]
  node [
    id 228
    label "103893"
    name "Paolo"
    surname "Lorenzi"
    country "ITA"
    hand "R"
    dateOfBirth 19811215.0
    tourneyNum 4
    avgRank 130.7037037037037
    avgPoints 454.22222222222223
  ]
  node [
    id 229
    label "105943"
    name "Federico"
    surname "Gaio"
    country "ITA"
    hand "R"
    dateOfBirth 19920305.0
    tourneyNum 3
    avgRank 134.88888888888889
    avgPoints 435.7037037037037
  ]
  node [
    id 230
    label "122330"
    name "Alexander"
    surname "Bublik"
    country "KAZ"
    hand "R"
    dateOfBirth 19970617.0
    tourneyNum 17
    avgRank 51.7037037037037
    avgPoints 1007.4074074074074
  ]
  node [
    id 231
    label "105948"
    name "Federico"
    surname "Coria"
    country "ARG"
    hand "R"
    dateOfBirth 19920309.0
    tourneyNum 8
    avgRank 100.29629629629629
    avgPoints 643.6296296296297
  ]
  node [
    id 232
    label "105952"
    name "Renzo"
    surname "Olivo"
    country "ARG"
    hand "R"
    dateOfBirth 19920315.0
    tourneyNum 1
    avgRank 228.7037037037037
    avgPoints 234.25925925925927
  ]
  node [
    id 233
    label "208364"
    name "Franco"
    surname "Roncadelli"
    country "URU"
    hand "U"
    dateOfBirth 20000206.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 234
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 13
    avgRank 46.22222222222222
    avgPoints 1152.888888888889
  ]
  node [
    id 235
    label "105967"
    name "Henri"
    surname "Laaksonen"
    country "SUI"
    hand "R"
    dateOfBirth 19920331.0
    tourneyNum 6
    avgRank 129.03703703703704
    avgPoints 461.1111111111111
  ]
  node [
    id 236
    label "200181"
    name "Amer"
    surname "Naow"
    country "SYR"
    hand "R"
    dateOfBirth 19950101.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 237
    label "144895"
    name "Corentin"
    surname "Moutet"
    country "FRA"
    hand "L"
    dateOfBirth 19990419.0
    tourneyNum 11
    avgRank 74.66666666666667
    avgPoints 789.8148148148148
  ]
  node [
    id 238
    label "105985"
    name "Darian"
    surname "King"
    country "BAR"
    hand "R"
    dateOfBirth 19920426.0
    tourneyNum 1
    avgRank 263.6296296296296
    avgPoints 186.7037037037037
  ]
  node [
    id 239
    label "202246"
    name "Nikoloz"
    surname "Davlianidze"
    country "GEO"
    hand "U"
    dateOfBirth 20000101.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 240
    label "105992"
    name "Ryan"
    surname "Harrison"
    country "USA"
    hand "R"
    dateOfBirth 19920507.0
    tourneyNum 1
    avgRank 434.7037037037037
    avgPoints 81.14814814814815
  ]
  node [
    id 241
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 9
    avgRank 110.14814814814815
    avgPoints 553.5555555555555
  ]
  node [
    id 242
    label "202261"
    name "Otto"
    surname "Virtanen"
    country "FIN"
    hand "U"
    dateOfBirth 20010621.0
    tourneyNum 1
    avgRank 760.4444444444445
    avgPoints 25.185185185185187
  ]
  node [
    id 243
    label "200221"
    name "Alejandro"
    surname "Davidovich Fokina"
    country "ESP"
    hand "R"
    dateOfBirth 19990605.0
    tourneyNum 10
    avgRank 73.66666666666667
    avgPoints 825.5925925925926
  ]
  node [
    id 244
    label "144932"
    name "Kristjan"
    surname "Tamm"
    country "EST"
    hand "U"
    dateOfBirth 19980616.0
    tourneyNum 1
    avgRank 719.4444444444445
    avgPoints 26.14814814814815
  ]
  node [
    id 245
    label "126504"
    name "Andrea"
    surname "Pellegrino"
    country "ITA"
    hand "R"
    dateOfBirth 19970323.0
    tourneyNum 1
    avgRank 309.962962962963
    avgPoints 147.37037037037038
  ]
  node [
    id 246
    label "106034"
    name "Yasutaka"
    surname "Uchiyama"
    country "JPN"
    hand "R"
    dateOfBirth 19920805.0
    tourneyNum 7
    avgRank 96.70370370370371
    avgPoints 665.3333333333334
  ]
  node [
    id 247
    label "200247"
    name "Alexandar"
    surname "Lazarov"
    country "BUL"
    hand "R"
    dateOfBirth 19971106.0
    tourneyNum 1
    avgRank 538.5925925925926
    avgPoints 52.888888888888886
  ]
  node [
    id 248
    label "200250"
    name "Mehluli Don Ayanda"
    surname "Sibanda"
    country "ZIM"
    hand "R"
    dateOfBirth 19991106.0
    tourneyNum 1
    avgRank 779.2962962962963
    avgPoints 21.14814814814815
  ]
  node [
    id 249
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 12
    avgRank 11.444444444444445
    avgPoints 2764.074074074074
  ]
  node [
    id 250
    label "202300"
    name "Phuong Van"
    surname "Nguyen"
    country "VIE"
    hand "U"
    dateOfBirth 20010227.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 251
    label "106045"
    name "Denis"
    surname "Kudla"
    country "USA"
    hand "R"
    dateOfBirth 19920817.0
    tourneyNum 1
    avgRank 113.51851851851852
    avgPoints 538.7037037037037
  ]
  node [
    id 252
    label "126535"
    name "Carlos"
    surname "Taberner"
    country "ESP"
    hand "R"
    dateOfBirth 19970808.0
    tourneyNum 2
    avgRank 164.92592592592592
    avgPoints 351.2962962962963
  ]
  node [
    id 253
    label "200265"
    name "Ulises"
    surname "Blanch"
    country "USA"
    hand "R"
    dateOfBirth 19980325.0
    tourneyNum 1
    avgRank 254.8148148148148
    avgPoints 199.22222222222223
  ]
  node [
    id 254
    label "106058"
    name "Jack"
    surname "Sock"
    country "USA"
    hand "R"
    dateOfBirth 19920924.0
    tourneyNum 4
    avgRank 334.3809523809524
    avgPoints 162.33333333333334
  ]
  node [
    id 255
    label "200267"
    name "Zizou"
    surname "Bergs"
    country "BEL"
    hand "R"
    dateOfBirth 19990603.0
    tourneyNum 1
    avgRank 498.3703703703704
    avgPoints 64.22222222222223
  ]
  node [
    id 256
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 13
    avgRank 90.92592592592592
    avgPoints 687.1851851851852
  ]
  node [
    id 257
    label "106071"
    name "Bernard"
    surname "Tomic"
    country "AUS"
    hand "R"
    dateOfBirth 19921021.0
    tourneyNum 1
    avgRank 209.55555555555554
    avgPoints 255.59259259259258
  ]
  node [
    id 258
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 9
    avgRank 24.74074074074074
    avgPoints 1689.6296296296296
  ]
  node [
    id 259
    label "106075"
    name "Jozef"
    surname "Kovalik"
    country "SVK"
    hand "R"
    dateOfBirth 19921104.0
    tourneyNum 6
    avgRank 127.51851851851852
    avgPoints 459.962962962963
  ]
  node [
    id 260
    label "126555"
    name "Tung Lin"
    surname "Wu"
    country "TPE"
    hand "R"
    dateOfBirth 19980512.0
    tourneyNum 1
    avgRank 240.4814814814815
    avgPoints 212.77777777777777
  ]
  node [
    id 261
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 10
    avgRank 79.44444444444444
    avgPoints 756.3333333333334
  ]
  node [
    id 262
    label "120424"
    name "Yannick"
    surname "Maden"
    country "GER"
    hand "U"
    dateOfBirth 19891028.0
    tourneyNum 1
    avgRank 154.14814814814815
    avgPoints 384.1111111111111
  ]
  node [
    id 263
    label "200303"
    name "Pavel"
    surname "Kotov"
    country "RUS"
    hand "U"
    dateOfBirth 19981118.0
    tourneyNum 1
    avgRank 272.8888888888889
    avgPoints 176.55555555555554
  ]
  node [
    id 264
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 15
    avgRank 33.370370370370374
    avgPoints 1472.6666666666667
  ]
  node [
    id 265
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 13
    avgRank 87.44444444444444
    avgPoints 713.2592592592592
  ]
  node [
    id 266
    label "202358"
    name "Chun Hsin"
    surname "Tseng"
    country "TPE"
    hand "R"
    dateOfBirth 20010808.0
    tourneyNum 1
    avgRank 285.2962962962963
    avgPoints 162.33333333333334
  ]
  node [
    id 267
    label "145016"
    name "Julian"
    surname "Saborio"
    country "CRC"
    hand "U"
    dateOfBirth 19961102.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 268
    label "106109"
    name "Alex"
    surname "Bolt"
    country "AUS"
    hand "L"
    dateOfBirth 19930105.0
    tourneyNum 3
    avgRank 155.33333333333334
    avgPoints 376.6296296296296
  ]
  node [
    id 269
    label "106110"
    name "Filip"
    surname "Horansky"
    country "SVK"
    hand "R"
    dateOfBirth 19930107.0
    tourneyNum 3
    avgRank 176.77777777777777
    avgPoints 321.9259259259259
  ]
  node [
    id 270
    label "200325"
    name "Emil"
    surname "Ruusuvuori"
    country "FIN"
    hand "R"
    dateOfBirth 19990402.0
    tourneyNum 9
    avgRank 95.37037037037037
    avgPoints 669.1481481481482
  ]
  node [
    id 271
    label "208518"
    name "Hazem"
    surname "Naw"
    country "UNK"
    hand "R"
    dateOfBirth 20000101.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 272
    label "106121"
    name "Taro"
    surname "Daniel"
    country "JPN"
    hand "R"
    dateOfBirth 19930127.0
    tourneyNum 4
    avgRank 113.48148148148148
    avgPoints 536.925925925926
  ]
  node [
    id 273
    label "200335"
    name "Felipe"
    surname "Meligeni Rodrigues Alves"
    country "BRA"
    hand "R"
    dateOfBirth 19980219.0
    tourneyNum 1
    avgRank 313.74074074074076
    avgPoints 145.62962962962962
  ]
  node [
    id 274
    label "126609"
    name "Patrik"
    surname "Niklas Salminen"
    country "FIN"
    hand "U"
    dateOfBirth 19970305.0
    tourneyNum 1
    avgRank 442.8888888888889
    avgPoints 78.11111111111111
  ]
  node [
    id 275
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 6
    avgRank 8.851851851851851
    avgPoints 2976.8518518518517
  ]
  node [
    id 276
    label "126612"
    name "Alexander"
    surname "Cozbinov"
    country "MDA"
    hand "R"
    dateOfBirth 19950428.0
    tourneyNum 1
    avgRank 772.2222222222222
    avgPoints 21.85185185185185
  ]
  node [
    id 277
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 8
    avgRank 96.11111111111111
    avgPoints 669.7777777777778
  ]
  node [
    id 278
    label "122533"
    name "Nam Hoang"
    surname "Ly"
    country "VIE"
    hand "R"
    dateOfBirth 19970225.0
    tourneyNum 1
    avgRank 691.6296296296297
    avgPoints 28.37037037037037
  ]
  node [
    id 279
    label "106150"
    name "Jeson"
    surname "Patrombon"
    country "PHI"
    hand "R"
    dateOfBirth 19930327.0
    tourneyNum 1
    avgRank 1440.851851851852
    avgPoints 2.0
  ]
  node [
    id 280
    label "122548"
    name "Edan"
    surname "Leshem"
    country "ISR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 1
    avgRank 361.48148148148147
    avgPoints 108.81481481481481
  ]
  node [
    id 281
    label "200384"
    name "Hugo"
    surname "Gaston"
    country "FRA"
    hand "U"
    dateOfBirth 20000926.0
    tourneyNum 3
    avgRank 203.55555555555554
    avgPoints 286.18518518518516
  ]
  node [
    id 282
    label "200390"
    name "Kacper"
    surname "Zuk"
    country "POL"
    hand "R"
    dateOfBirth 19990121.0
    tourneyNum 2
    avgRank 311.0
    avgPoints 153.4814814814815
  ]
  node [
    id 283
    label "126663"
    name "Juan Pablo"
    surname "Ficovich"
    country "ARG"
    hand "R"
    dateOfBirth 19970124.0
    tourneyNum 1
    avgRank 209.74074074074073
    avgPoints 265.9259259259259
  ]
  node [
    id 284
    label "134868"
    name "Tallon"
    surname "Griekspoor"
    country "NED"
    hand "R"
    dateOfBirth 19960702.0
    tourneyNum 3
    avgRank 162.4814814814815
    avgPoints 360.14814814814815
  ]
  node [
    id 285
    label "106198"
    name "Hugo"
    surname "Dellien"
    country "BOL"
    hand "R"
    dateOfBirth 19930616.0
    tourneyNum 8
    avgRank 97.74074074074075
    avgPoints 660.7777777777778
  ]
  node [
    id 286
    label "202462"
    name "Yasitha"
    surname "De Silva"
    country "SRI"
    hand "U"
    dateOfBirth 19950703.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 287
    label "106210"
    name "Jiri"
    surname "Vesely"
    country "CZE"
    hand "L"
    dateOfBirth 19930710.0
    tourneyNum 8
    avgRank 73.77777777777777
    avgPoints 800.2592592592592
  ]
  node [
    id 288
    label "106214"
    name "Oscar"
    surname "Otte"
    country "GER"
    hand "R"
    dateOfBirth 19930716.0
    tourneyNum 2
    avgRank 161.5185185185185
    avgPoints 372.3333333333333
  ]
  node [
    id 289
    label "134886"
    name "Sharmal"
    surname "Dissanayake"
    country "SRI"
    hand "R"
    dateOfBirth 19960514.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 290
    label "106218"
    name "Marcos"
    surname "Giron"
    country "USA"
    hand "R"
    dateOfBirth 19930724.0
    tourneyNum 9
    avgRank 93.0
    avgPoints 681.7037037037037
  ]
  node [
    id 291
    label "106220"
    name "Dimitar"
    surname "Kuzmanov"
    country "BUL"
    hand "R"
    dateOfBirth 19930728.0
    tourneyNum 3
    avgRank 307.51851851851853
    avgPoints 143.5185185185185
  ]
  node [
    id 292
    label "106223"
    name "Aleksandre"
    surname "Metreveli"
    country "GEO"
    hand "R"
    dateOfBirth 19930810.0
    tourneyNum 2
    avgRank 580.2222222222222
    avgPoints 43.44444444444444
  ]
  node [
    id 293
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 10
    avgRank 68.07407407407408
    avgPoints 860.1111111111111
  ]
  node [
    id 294
    label "106232"
    name "Roberto"
    surname "Cid"
    country "DOM"
    hand "R"
    dateOfBirth 19930830.0
    tourneyNum 1
    avgRank 223.11111111111111
    avgPoints 237.59259259259258
  ]
  node [
    id 295
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 8
    avgRank 3.3333333333333335
    avgPoints 8073.148148148148
  ]
  node [
    id 296
    label "106234"
    name "Aslan"
    surname "Karatsev"
    country "RUS"
    hand "R"
    dateOfBirth 19930904.0
    tourneyNum 3
    avgRank 173.4814814814815
    avgPoints 416.037037037037
  ]
  node [
    id 297
    label "106247"
    name "Nik"
    surname "Razborsek"
    country "SLO"
    hand "L"
    dateOfBirth 19931004.0
    tourneyNum 1
    avgRank 683.2222222222222
    avgPoints 29.88888888888889
  ]
  node [
    id 298
    label "206600"
    name "Zura"
    surname "Tkemaladze"
    country "GEO"
    hand "U"
    dateOfBirth 20000604.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 299
    label "106249"
    name "Joao"
    surname "Domingues"
    country "POR"
    hand "R"
    dateOfBirth 19931005.0
    tourneyNum 2
    avgRank 166.88888888888889
    avgPoints 344.6666666666667
  ]
  node [
    id 300
    label "104229"
    name "Yen Hsun"
    surname "Lu"
    country "TPE"
    hand "R"
    dateOfBirth 19830814.0
    tourneyNum 2
    avgRank 983.7916666666666
    avgPoints 10.0
  ]
  node [
    id 301
    label "104233"
    name "Frederik"
    surname "Nielsen"
    country "DEN"
    hand "R"
    dateOfBirth 19830827.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 302
    label "106281"
    name "Liam"
    surname "Broady"
    country "GBR"
    hand "L"
    dateOfBirth 19940104.0
    tourneyNum 1
    avgRank 207.2962962962963
    avgPoints 270.85185185185185
  ]
  node [
    id 303
    label "106283"
    name "Mitchell"
    surname "Krueger"
    country "USA"
    hand "R"
    dateOfBirth 19940112.0
    tourneyNum 1
    avgRank 192.33333333333334
    avgPoints 294.74074074074076
  ]
  node [
    id 304
    label "122669"
    name "Juan Pablo"
    surname "Varillas Patino Samudio"
    country "PER"
    hand "R"
    dateOfBirth 19951006.0
    tourneyNum 2
    avgRank 147.2962962962963
    avgPoints 397.6666666666667
  ]
  node [
    id 305
    label "106293"
    name "Kimmer"
    surname "Coppejans"
    country "BEL"
    hand "R"
    dateOfBirth 19940207.0
    tourneyNum 3
    avgRank 165.2962962962963
    avgPoints 348.44444444444446
  ]
  node [
    id 306
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 14
    avgRank 5.888888888888889
    avgPoints 5405.185185185185
  ]
  node [
    id 307
    label "106296"
    name "Gregoire"
    surname "Barrere"
    country "FRA"
    hand "R"
    dateOfBirth 19940216.0
    tourneyNum 8
    avgRank 99.22222222222223
    avgPoints 643.7777777777778
  ]
  node [
    id 308
    label "106302"
    name "Andrew"
    surname "Harris"
    country "AUS"
    hand "R"
    dateOfBirth 19940307.0
    tourneyNum 1
    avgRank 207.77777777777777
    avgPoints 262.1111111111111
  ]
  node [
    id 309
    label "200514"
    name "Jurij"
    surname "Rodionov"
    country "AUT"
    hand "L"
    dateOfBirth 19990516.0
    tourneyNum 3
    avgRank 188.03703703703704
    avgPoints 333.6666666666667
  ]
  node [
    id 310
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 8
    avgRank 86.25925925925925
    avgPoints 720.4814814814815
  ]
  node [
    id 311
    label "132932"
    name "Franz"
    surname "Luna Lavidalie"
    country "GUA"
    hand "R"
    dateOfBirth 19971005.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 312
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 7
    avgRank 57.333333333333336
    avgPoints 977.7777777777778
  ]
  node [
    id 313
    label "106329"
    name "Thiago"
    surname "Monteiro"
    country "BRA"
    hand "L"
    dateOfBirth 19940531.0
    tourneyNum 9
    avgRank 84.14814814814815
    avgPoints 732.2592592592592
  ]
  node [
    id 314
    label "106331"
    name "Christopher"
    surname "Oconnell"
    country "AUS"
    hand "U"
    dateOfBirth 19940603.0
    tourneyNum 2
    avgRank 116.29629629629629
    avgPoints 523.7037037037037
  ]
  node [
    id 315
    label "104291"
    name "Malek"
    surname "Jaziri"
    country "TUN"
    hand "R"
    dateOfBirth 19840120.0
    tourneyNum 3
    avgRank 249.62962962962962
    avgPoints 201.92592592592592
  ]
  node [
    id 316
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 7
    avgRank 95.70370370370371
    avgPoints 675.9629629629629
  ]
  node [
    id 317
    label "206716"
    name "Lukas"
    surname "Hellum Lilleengen"
    country "NOR"
    hand "U"
    dateOfBirth 20001115.0
    tourneyNum 1
    avgRank 1466.851851851852
    avgPoints 2.7037037037037037
  ]
  node [
    id 318
    label "126845"
    name "Max"
    surname "Purcell"
    country "AUS"
    hand "U"
    dateOfBirth 19980403.0
    tourneyNum 1
    avgRank 224.7037037037037
    avgPoints 234.33333333333334
  ]
  node [
    id 319
    label "200574"
    name "Alexander"
    surname "Donski"
    country "CAN"
    hand "U"
    dateOfBirth 19980801.0
    tourneyNum 1
    avgRank 579.925925925926
    avgPoints 43.851851851851855
  ]
  node [
    id 320
    label "126846"
    name "Aleksandar"
    surname "Vukic"
    country "AUS"
    hand "R"
    dateOfBirth 19960406.0
    tourneyNum 1
    avgRank 212.0
    avgPoints 268.9259259259259
  ]
  node [
    id 321
    label "106368"
    name "Ramkumar"
    surname "Ramanathan"
    country "IND"
    hand "R"
    dateOfBirth 19941108.0
    tourneyNum 2
    avgRank 190.14814814814815
    avgPoints 296.48148148148147
  ]
  node [
    id 322
    label "104327"
    name "Steve"
    surname "Darcis"
    country "BEL"
    hand "R"
    dateOfBirth 19840313.0
    tourneyNum 1
    avgRank 244.85185185185185
    avgPoints 212.88888888888889
  ]
  node [
    id 323
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 10
    avgRank 49.77777777777778
    avgPoints 1030.7407407407406
  ]
  node [
    id 324
    label "106388"
    name "Pak Long"
    surname "Yeung"
    country "HKG"
    hand "U"
    dateOfBirth 19950214.0
    tourneyNum 1
    avgRank 993.5555555555555
    avgPoints 10.666666666666666
  ]
  node [
    id 325
    label "106393"
    name "Frederico"
    surname "Ferreira Silva"
    country "POR"
    hand "L"
    dateOfBirth 19950318.0
    tourneyNum 1
    avgRank 193.5185185185185
    avgPoints 291.22222222222223
  ]
  node [
    id 326
    label "133018"
    name "Michail"
    surname "Pervolarakis"
    country "CYP"
    hand "R"
    dateOfBirth 19960606.0
    tourneyNum 1
    avgRank 443.51851851851853
    avgPoints 78.03703703703704
  ]
  node [
    id 327
    label "206748"
    name "Giulio"
    surname "Zeppieri"
    country "ITA"
    hand "L"
    dateOfBirth 20011207.0
    tourneyNum 1
    avgRank 342.6296296296296
    avgPoints 118.51851851851852
  ]
  node [
    id 328
    label "106397"
    name "Wishaya"
    surname "Trongcharoenchaikul"
    country "THA"
    hand "R"
    dateOfBirth 19950408.0
    tourneyNum 1
    avgRank 636.0370370370371
    avgPoints 35.03703703703704
  ]
  node [
    id 329
    label "106398"
    name "Pedro"
    surname "Cachin"
    country "ARG"
    hand "R"
    dateOfBirth 19950412.0
    tourneyNum 1
    avgRank 383.6666666666667
    avgPoints 99.74074074074075
  ]
  node [
    id 330
    label "206750"
    name "Daniil"
    surname "Glinka"
    country "EST"
    hand "U"
    dateOfBirth 20000512.0
    tourneyNum 1
    avgRank 934.2592592592592
    avgPoints 13.11111111111111
  ]
  node [
    id 331
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 3
    avgRank 37.925925925925924
    avgPoints 1279.8148148148148
  ]
  node [
    id 332
    label "200611"
    name "Elliot"
    surname "Benchetrit"
    country "FRA"
    hand "U"
    dateOfBirth 19981002.0
    tourneyNum 2
    avgRank 216.14814814814815
    avgPoints 245.1851851851852
  ]
  node [
    id 333
    label "200615"
    name "Alexei"
    surname "Popyrin"
    country "AUS"
    hand "R"
    dateOfBirth 19990805.0
    tourneyNum 5
    avgRank 105.66666666666667
    avgPoints 597.8148148148148
  ]
  node [
    id 334
    label "106408"
    name "Mario"
    surname "Vilella Martinez"
    country "ESP"
    hand "U"
    dateOfBirth 19950703.0
    tourneyNum 1
    avgRank 186.25925925925927
    avgPoints 307.1111111111111
  ]
  node [
    id 335
    label "200620"
    name "Ignacio"
    surname "Garcia"
    country "PUR"
    hand "U"
    dateOfBirth 19990518.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 336
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 12
    avgRank 55.888888888888886
    avgPoints 985.7777777777778
  ]
  node [
    id 337
    label "200624"
    name "Sebastian"
    surname "Korda"
    country "USA"
    hand "U"
    dateOfBirth 20000705.0
    tourneyNum 3
    avgRank 180.74074074074073
    avgPoints 355.74074074074076
  ]
  node [
    id 338
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 12
    avgRank 4.7407407407407405
    avgPoints 6547.592592592592
  ]
  node [
    id 339
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 12
    avgRank 22.925925925925927
    avgPoints 1912.6296296296296
  ]
  node [
    id 340
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 11
    avgRank 27.333333333333332
    avgPoints 1608.3333333333333
  ]
  node [
    id 341
    label "200670"
    name "Jeffrey John"
    surname "Wolf"
    country "USA"
    hand "U"
    dateOfBirth 19981221.0
    tourneyNum 3
    avgRank 140.8148148148148
    avgPoints 429.6666666666667
  ]
  node [
    id 342
    label "104424"
    name "Go"
    surname "Soeda"
    country "JPN"
    hand "R"
    dateOfBirth 19840905.0
    tourneyNum 4
    avgRank 122.5925925925926
    avgPoints 488.77777777777777
  ]
  node [
    id 343
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 7
    avgRank 85.22222222222223
    avgPoints 727.0
  ]
  node [
    id 344
    label "126964"
    name "Vitaliy"
    surname "Sachko"
    country "UKR"
    hand "U"
    dateOfBirth 19970714.0
    tourneyNum 1
    avgRank 508.5925925925926
    avgPoints 62.111111111111114
  ]
  edge [
    source 0
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 111
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 112
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 243
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 149
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 79
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 278
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 331
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 228
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 281
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 129
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 132
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 177
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 184
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 177
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 127
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 119
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 332
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 99
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 166
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 197
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 315
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 163
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 254
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 183
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 266
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 260
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 312
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 91
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 22
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 141
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 22
    target 338
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 197
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 256
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 312
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 295
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 60
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 23
    target 243
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 197
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 202
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 249
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 338
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 197
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 24
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 295
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 24
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 249
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 215
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 134
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 194
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 56
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 258
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 98
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 195
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 134
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 192
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 65
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 197
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 286
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 289
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 300
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 226
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 140
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 213
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 89
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 112
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 161
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 338
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 128
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 40
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 249
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 40
    target 167
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 295
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 306
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 111
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 315
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 133
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 197
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 226
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 194
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 161
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 264
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 41
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 131
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 111
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 219
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 122
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 94
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 260
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 266
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 141
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 184
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 315
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 183
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 111
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 223
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 138
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 91
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 223
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 255
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 91
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 213
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 245
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 342
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 197
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 56
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 111
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 127
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 303
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 294
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 333
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 60
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 164
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 85
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 254
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 137
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 186
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 332
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 110
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 163
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 237
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 65
    target 79
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 65
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 251
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 140
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 295
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 65
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 119
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 98
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 234
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 68
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 340
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 68
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 164
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 69
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 70
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 72
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 246
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 182
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 230
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 73
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 129
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 184
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 158
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 127
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 202
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 79
    target 154
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 99
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 95
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 159
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 83
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 111
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 127
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 179
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 85
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 85
    target 177
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 164
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 231
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 86
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 250
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 91
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 249
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 91
    target 293
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 91
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 314
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 213
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 91
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 91
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 91
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 91
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 92
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 127
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 194
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 218
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 95
    target 111
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 190
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 167
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 95
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 154
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 339
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 100
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 231
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 102
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 104
    target 319
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 105
    target 208
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 199
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 109
    target 120
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 254
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 140
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 168
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 127
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 137
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 111
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 334
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 194
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 296
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 114
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 254
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 285
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 249
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 127
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 194
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 161
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 127
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 166
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 149
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 128
    target 331
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 128
    target 224
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 128
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 129
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 129
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 129
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 341
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 192
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 132
    target 141
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 132
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 293
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 132
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 161
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 154
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 177
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 229
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 338
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 134
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 135
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 136
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 136
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 158
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 161
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 137
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 213
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 163
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 138
    target 231
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 197
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 140
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 223
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 183
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 314
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 226
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 141
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 306
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 141
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 177
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 223
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 295
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 141
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 142
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 145
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 145
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 310
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 147
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 293
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 148
    target 220
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 149
    target 213
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 150
    target 281
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 152
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 152
    target 229
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 285
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 152
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 153
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 153
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 153
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 153
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 155
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 155
    target 214
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 200
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 158
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 161
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 161
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 161
    target 214
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 161
    target 230
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 161
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 161
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 162
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 163
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 163
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 163
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 163
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 163
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 163
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 163
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 163
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 322
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 175
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 164
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 183
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 165
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 165
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 166
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 306
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 167
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 168
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 169
    target 274
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 169
    target 242
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 173
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 173
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 173
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 173
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 174
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 215
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 175
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 192
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 177
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 177
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 177
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 177
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 242
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 223
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 179
    target 194
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 338
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 179
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 214
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 237
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 179
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 194
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 183
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 183
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 183
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 183
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 183
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 183
    target 201
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 184
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 184
    target 228
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 184
    target 194
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 185
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 336
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 188
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 188
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 188
    target 254
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 189
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 293
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 307
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 192
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 223
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 192
    target 195
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 193
    target 326
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 268
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 193
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 197
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 261
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 193
    target 215
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 336
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 193
    target 235
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 194
    target 246
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 194
    target 226
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 194
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 194
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 195
    target 264
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 195
    target 217
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 234
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 195
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 195
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 331
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 195
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 195
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 195
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 195
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 196
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 197
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 197
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 197
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 197
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 197
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 197
    target 303
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 197
    target 281
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 281
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 295
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 198
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 199
    target 279
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 296
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 318
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 223
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 202
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 218
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 219
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 203
    target 325
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 215
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 213
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 213
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 213
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 213
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 213
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 214
    target 342
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 215
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 215
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 215
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 215
    target 245
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 215
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 216
    target 342
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 217
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 219
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 218
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 218
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 218
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 218
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 219
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 219
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 219
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 220
    target 228
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 220
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 220
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 221
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 223
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 223
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 223
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 223
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 223
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 223
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 223
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 223
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 224
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 225
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 256
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 259
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 228
    target 261
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 228
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 229
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 235
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 230
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 231
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 231
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 231
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 231
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 231
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 232
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 234
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 234
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 234
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 234
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 234
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 235
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 235
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 239
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 243
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 249
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 243
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 244
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 244
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 246
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 246
    target 300
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 248
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 249
    target 338
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 249
    target 295
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 249
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 249
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 249
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 249
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 252
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 253
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 254
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 254
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 256
    target 293
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 256
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 256
    target 285
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 256
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 256
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 296
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 259
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 259
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 261
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 261
    target 287
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 261
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 261
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 264
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 264
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 264
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 264
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 264
    target 275
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 264
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 265
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 265
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 265
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 265
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 268
    target 295
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 270
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 270
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 272
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 272
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 272
    target 296
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 275
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 342
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 322
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 277
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 277
    target 341
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 279
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 281
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 281
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 285
    target 329
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 285
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 287
    target 302
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 290
    target 310
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 290
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 290
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 291
    target 322
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 342
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 293
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 293
    target 340
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 295
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 295
    target 338
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 295
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 295
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 295
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 296
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 301
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 306
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 306
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 306
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 306
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 310
    target 339
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 311
    target 315
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 313
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 314
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 316
    target 323
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 316
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 323
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 323
    target 336
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 338
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 339
    target 340
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 342
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
]
