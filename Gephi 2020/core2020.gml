graph [
  node [
    id 0
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 15
    avgRank 25.185185185185187
    avgPoints 1700.037037037037
  ]
  node [
    id 1
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 9
    avgRank 9.962962962962964
    avgPoints 2820.0
  ]
  node [
    id 2
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 13
    avgRank 24.703703703703702
    avgPoints 1724.111111111111
  ]
  node [
    id 3
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 9
    avgRank 1.1111111111111112
    avgPoints 10940.185185185184
  ]
  node [
    id 4
    label "111513"
    name "Laslo"
    surname "Djere"
    country "SRB"
    hand "R"
    dateOfBirth 19950602.0
    tourneyNum 10
    avgRank 59.407407407407405
    avgPoints 974.7777777777778
  ]
  node [
    id 5
    label "105613"
    name "Norbert"
    surname "Gombos"
    country "SVK"
    hand "R"
    dateOfBirth 19900813.0
    tourneyNum 8
    avgRank 100.81481481481481
    avgPoints 634.2592592592592
  ]
  node [
    id 6
    label "206173"
    name "Jannik"
    surname "Sinner"
    country "ITA"
    hand "U"
    dateOfBirth 20010816.0
    tourneyNum 12
    avgRank 59.81481481481482
    avgPoints 998.7037037037037
  ]
  node [
    id 7
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 10
    avgRank 107.44444444444444
    avgPoints 592.5925925925926
  ]
  node [
    id 8
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 13
    avgRank 58.03703703703704
    avgPoints 964.6296296296297
  ]
  node [
    id 9
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 16
    avgRank 34.333333333333336
    avgPoints 1380.7407407407406
  ]
  node [
    id 10
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 10
    avgRank 76.62962962962963
    avgPoints 786.6666666666666
  ]
  node [
    id 11
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 11
    avgRank 21.074074074074073
    avgPoints 1987.2222222222222
  ]
  node [
    id 12
    label "122330"
    name "Alexander"
    surname "Bublik"
    country "KAZ"
    hand "R"
    dateOfBirth 19970617.0
    tourneyNum 17
    avgRank 51.7037037037037
    avgPoints 1007.4074074074074
  ]
  node [
    id 13
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 17
    avgRank 38.77777777777778
    avgPoints 1288.4074074074074
  ]
  node [
    id 14
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 12
    avgRank 68.92592592592592
    avgPoints 841.5555555555555
  ]
  node [
    id 15
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 13
    avgRank 33.074074074074076
    avgPoints 1410.3703703703704
  ]
  node [
    id 16
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 11
    avgRank 27.333333333333332
    avgPoints 1608.3333333333333
  ]
  node [
    id 17
    label "200221"
    name "Alejandro"
    surname "Davidovich Fokina"
    country "ESP"
    hand "R"
    dateOfBirth 19990605.0
    tourneyNum 10
    avgRank 73.66666666666667
    avgPoints 825.5925925925926
  ]
  node [
    id 18
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 13
    avgRank 46.22222222222222
    avgPoints 1152.888888888889
  ]
  node [
    id 19
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 13
    avgRank 90.92592592592592
    avgPoints 687.1851851851852
  ]
  node [
    id 20
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 13
    avgRank 87.44444444444444
    avgPoints 713.2592592592592
  ]
  node [
    id 21
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 12
    avgRank 22.925925925925927
    avgPoints 1912.6296296296296
  ]
  node [
    id 22
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 14
    avgRank 17.555555555555557
    avgPoints 2147.962962962963
  ]
  node [
    id 23
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 12
    avgRank 32.55555555555556
    avgPoints 1467.4444444444443
  ]
  node [
    id 24
    label "106329"
    name "Thiago"
    surname "Monteiro"
    country "BRA"
    hand "L"
    dateOfBirth 19940531.0
    tourneyNum 9
    avgRank 84.14814814814815
    avgPoints 732.2592592592592
  ]
  node [
    id 25
    label "126205"
    name "Tommy"
    surname "Paul"
    country "USA"
    hand "R"
    dateOfBirth 19970517.0
    tourneyNum 14
    avgRank 61.51851851851852
    avgPoints 917.8518518518518
  ]
  node [
    id 26
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 11
    avgRank 89.96296296296296
    avgPoints 705.1851851851852
  ]
  node [
    id 27
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 12
    avgRank 60.111111111111114
    avgPoints 930.3333333333334
  ]
  node [
    id 28
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 14
    avgRank 11.88888888888889
    avgPoints 2926.222222222222
  ]
  node [
    id 29
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 9
    avgRank 24.74074074074074
    avgPoints 1689.6296296296296
  ]
  node [
    id 30
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 12
    avgRank 55.888888888888886
    avgPoints 985.7777777777778
  ]
  node [
    id 31
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 14
    avgRank 43.74074074074074
    avgPoints 1143.888888888889
  ]
  node [
    id 32
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 8
    avgRank 3.3333333333333335
    avgPoints 8073.148148148148
  ]
  node [
    id 33
    label "144895"
    name "Corentin"
    surname "Moutet"
    country "FRA"
    hand "L"
    dateOfBirth 19990419.0
    tourneyNum 11
    avgRank 74.66666666666667
    avgPoints 789.8148148148148
  ]
  node [
    id 34
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 7
    avgRank 85.22222222222223
    avgPoints 727.0
  ]
  node [
    id 35
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 14
    avgRank 32.407407407407405
    avgPoints 1424.962962962963
  ]
  node [
    id 36
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 10
    avgRank 19.77777777777778
    avgPoints 2000.3333333333333
  ]
  node [
    id 37
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 10
    avgRank 79.44444444444444
    avgPoints 756.3333333333334
  ]
  node [
    id 38
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 10
    avgRank 7.0
    avgPoints 4496.666666666667
  ]
  node [
    id 39
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 14
    avgRank 13.444444444444445
    avgPoints 2470.5555555555557
  ]
  node [
    id 40
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 10
    avgRank 22.925925925925927
    avgPoints 1925.5555555555557
  ]
  node [
    id 41
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 15
    avgRank 33.370370370370374
    avgPoints 1472.6666666666667
  ]
  node [
    id 42
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 8
    avgRank 22.185185185185187
    avgPoints 1827.7777777777778
  ]
  node [
    id 43
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 14
    avgRank 42.44444444444444
    avgPoints 1225.7777777777778
  ]
  node [
    id 44
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 10
    avgRank 59.333333333333336
    avgPoints 951.7037037037037
  ]
  node [
    id 45
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 7
    avgRank 1.8888888888888888
    avgPoints 9816.111111111111
  ]
  node [
    id 46
    label "105577"
    name "Vasek"
    surname "Pospisil"
    country "CAN"
    hand "R"
    dateOfBirth 19900623.0
    tourneyNum 10
    avgRank 87.48148148148148
    avgPoints 739.4074074074074
  ]
  node [
    id 47
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 16
    avgRank 41.592592592592595
    avgPoints 1216.1851851851852
  ]
  node [
    id 48
    label "110602"
    name "Dennis"
    surname "Novak"
    country "AUT"
    hand "R"
    dateOfBirth 19930828.0
    tourneyNum 12
    avgRank 93.37037037037037
    avgPoints 683.6296296296297
  ]
  node [
    id 49
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 14
    avgRank 31.77777777777778
    avgPoints 1439.2962962962963
  ]
  node [
    id 50
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 12
    avgRank 79.55555555555556
    avgPoints 761.5185185185185
  ]
  node [
    id 51
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 10
    avgRank 12.407407407407407
    avgPoints 2550.185185185185
  ]
  node [
    id 52
    label "124187"
    name "Reilly"
    surname "Opelka"
    country "USA"
    hand "R"
    dateOfBirth 19970828.0
    tourneyNum 10
    avgRank 38.22222222222222
    avgPoints 1290.9259259259259
  ]
  node [
    id 53
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 9
    avgRank 35.51851851851852
    avgPoints 1376.111111111111
  ]
  node [
    id 54
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 12
    avgRank 11.444444444444445
    avgPoints 2764.074074074074
  ]
  node [
    id 55
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 14
    avgRank 5.888888888888889
    avgPoints 5405.185185185185
  ]
  node [
    id 56
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 17
    avgRank 20.814814814814813
    avgPoints 1964.888888888889
  ]
  node [
    id 57
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 12
    avgRank 62.851851851851855
    avgPoints 911.925925925926
  ]
  node [
    id 58
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 13
    avgRank 39.592592592592595
    avgPoints 1259.8148148148148
  ]
  node [
    id 59
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 13
    avgRank 58.44444444444444
    avgPoints 943.6666666666666
  ]
  node [
    id 60
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 11
    avgRank 66.66666666666667
    avgPoints 889.2592592592592
  ]
  node [
    id 61
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 13
    avgRank 54.96296296296296
    avgPoints 978.3333333333334
  ]
  node [
    id 62
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 10
    avgRank 84.25925925925925
    avgPoints 748.5925925925926
  ]
  node [
    id 63
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 8
    avgRank 16.814814814814813
    avgPoints 2200.925925925926
  ]
  node [
    id 64
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 12
    avgRank 56.074074074074076
    avgPoints 977.4074074074074
  ]
  node [
    id 65
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 10
    avgRank 49.77777777777778
    avgPoints 1030.7407407407406
  ]
  node [
    id 66
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 11
    avgRank 74.62962962962963
    avgPoints 830.8888888888889
  ]
  node [
    id 67
    label "200325"
    name "Emil"
    surname "Ruusuvuori"
    country "FIN"
    hand "R"
    dateOfBirth 19990402.0
    tourneyNum 9
    avgRank 95.37037037037037
    avgPoints 669.1481481481482
  ]
  node [
    id 68
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 15
    avgRank 29.48148148148148
    avgPoints 1537.2222222222222
  ]
  node [
    id 69
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 14
    avgRank 38.592592592592595
    avgPoints 1305.5925925925926
  ]
  node [
    id 70
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 9
    avgRank 11.925925925925926
    avgPoints 2570.5555555555557
  ]
  node [
    id 71
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 10
    avgRank 51.81481481481482
    avgPoints 1019.2592592592592
  ]
  node [
    id 72
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 9
    avgRank 14.037037037037036
    avgPoints 2389.259259259259
  ]
  node [
    id 73
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 12
    avgRank 4.7407407407407405
    avgPoints 6547.592592592592
  ]
  edge [
    source 0
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 22
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 2
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 13
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 4
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 3
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 6
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 58
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 45
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 9
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 3
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 54
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 3
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 55
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 22
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 11
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 7
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 11
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 57
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 51
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 55
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 51
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 45
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 70
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 55
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 47
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 38
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 13
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 21
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 59
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 54
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 48
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 59
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 18
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 55
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 27
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 35
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 28
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 55
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 28
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 28
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 52
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 45
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 38
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 58
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 55
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 57
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 36
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 37
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 56
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 37
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 38
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 38
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 73
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 38
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 46
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 55
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 64
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 41
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 69
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 41
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 45
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 47
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 49
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 73
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 54
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 64
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 57
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 73
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 60
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 69
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 69
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 70
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 71
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
]
