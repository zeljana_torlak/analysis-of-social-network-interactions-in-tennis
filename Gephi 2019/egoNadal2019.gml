graph [
  node [
    id 0
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 28
    avgRank 18.595744680851062
    avgPoints 1879.3617021276596
  ]
  node [
    id 1
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 22
    avgRank 74.87234042553192
    avgPoints 832.9787234042553
  ]
  node [
    id 2
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 25
    avgRank 5.042553191489362
    avgPoints 4747.234042553191
  ]
  node [
    id 3
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 15
    avgRank 8.27659574468085
    avgPoints 3509.68085106383
  ]
  node [
    id 4
    label "127339"
    name "Borna"
    surname "Gojo"
    country "CRO"
    hand "U"
    dateOfBirth 19980227.0
    tourneyNum 2
    avgRank 309.02127659574467
    avgPoints 113.82978723404256
  ]
  node [
    id 5
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 24
    avgRank 9.936170212765957
    avgPoints 3646.1063829787236
  ]
  node [
    id 6
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 22
    avgRank 16.914893617021278
    avgPoints 2027.340425531915
  ]
  node [
    id 7
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 23
    avgRank 36.40425531914894
    avgPoints 1390.595744680851
  ]
  node [
    id 8
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 27
    avgRank 7.851063829787234
    avgPoints 3751.063829787234
  ]
  node [
    id 9
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 18
    avgRank 37.638297872340424
    avgPoints 1219.6808510638298
  ]
  node [
    id 10
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 18
    avgRank 1.8085106382978724
    avgPoints 8636.170212765957
  ]
  node [
    id 11
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 19.893617021276597
    avgPoints 1776.9148936170213
  ]
  node [
    id 12
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 18
    avgRank 73.80851063829788
    avgPoints 745.3617021276596
  ]
  node [
    id 13
    label "106423"
    name "Thanasi"
    surname "Kokkinakis"
    country "AUS"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 4
    avgRank 178.4255319148936
    avgPoints 299.0425531914894
  ]
  node [
    id 14
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 27
    avgRank 49.91489361702128
    avgPoints 1003.8510638297872
  ]
  node [
    id 15
    label "111577"
    name "Jared"
    surname "Donaldson"
    country "USA"
    hand "R"
    dateOfBirth 19961009.0
    tourneyNum 2
    avgRank 443.0425531914894
    avgPoints 150.53191489361703
  ]
  node [
    id 16
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 31
    avgRank 11.595744680851064
    avgPoints 2647.021276595745
  ]
  node [
    id 17
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 20
    avgRank 28.127659574468087
    avgPoints 1520.9574468085107
  ]
  node [
    id 18
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 18
    avgRank 1.1914893617021276
    avgPoints 10754.04255319149
  ]
  node [
    id 19
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 27
    avgRank 54.340425531914896
    avgPoints 961.5957446808511
  ]
  node [
    id 20
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 22
    avgRank 65.2127659574468
    avgPoints 856.5531914893617
  ]
  node [
    id 21
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 14
    avgRank 3.5531914893617023
    avgPoints 6221.063829787234
  ]
  node [
    id 22
    label "120424"
    name "Yannick"
    surname "Maden"
    country "GER"
    hand "U"
    dateOfBirth 19891028.0
    tourneyNum 6
    avgRank 117.70212765957447
    avgPoints 482.27659574468083
  ]
  node [
    id 23
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 16
    avgRank 118.70212765957447
    avgPoints 612.0851063829788
  ]
  node [
    id 24
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 30
    avgRank 41.808510638297875
    avgPoints 1121.3829787234042
  ]
  node [
    id 25
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 4
    avgRank 157.40425531914894
    avgPoints 371.1063829787234
  ]
  node [
    id 26
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 30
    avgRank 54.787234042553195
    avgPoints 938.1702127659574
  ]
  node [
    id 27
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 17
    avgRank 52.51063829787234
    avgPoints 960.7446808510638
  ]
  node [
    id 28
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 29
    avgRank 52.787234042553195
    avgPoints 966.2553191489362
  ]
  node [
    id 29
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 26
    avgRank 26.04255319148936
    avgPoints 1475.8936170212767
  ]
  node [
    id 30
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 20
    avgRank 20.382978723404257
    avgPoints 2191.595744680851
  ]
  node [
    id 31
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 12
    avgRank 184.27659574468086
    avgPoints 394.8510638297872
  ]
  node [
    id 32
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 26
    avgRank 15.446808510638299
    avgPoints 2115.2127659574467
  ]
  node [
    id 33
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 25
    avgRank 12.595744680851064
    avgPoints 2402.2340425531916
  ]
  node [
    id 34
    label "105870"
    name "Yannick"
    surname "Hanfmann"
    country "GER"
    hand "U"
    dateOfBirth 19911113.0
    tourneyNum 2
    avgRank 180.80851063829786
    avgPoints 288.8085106382979
  ]
  node [
    id 35
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 27
    avgRank 28.404255319148938
    avgPoints 1726.8936170212767
  ]
  node [
    id 36
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 29
    avgRank 29.638297872340427
    avgPoints 1412.5531914893618
  ]
  node [
    id 37
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 21
    avgRank 35.1063829787234
    avgPoints 1320.127659574468
  ]
  node [
    id 38
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 40.02127659574468
    avgPoints 1147.872340425532
  ]
  node [
    id 39
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 22
    avgRank 5.1063829787234045
    avgPoints 4782.978723404255
  ]
  node [
    id 40
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 22
    avgRank 79.65957446808511
    avgPoints 856.0425531914893
  ]
  node [
    id 41
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 8
    avgRank 121.65957446808511
    avgPoints 526.9574468085107
  ]
  node [
    id 42
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 32
    avgRank 24.72340425531915
    avgPoints 1574.5744680851064
  ]
  node [
    id 43
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 25
    avgRank 20.02127659574468
    avgPoints 1787.872340425532
  ]
  node [
    id 44
    label "103970"
    name "David"
    surname "Ferrer"
    country "ESP"
    hand "R"
    dateOfBirth 19820402.0
    tourneyNum 6
    avgRank 162.47727272727272
    avgPoints 348.52272727272725
  ]
  node [
    id 45
    label "104607"
    name "Tomas"
    surname "Berdych"
    country "CZE"
    hand "R"
    dateOfBirth 19850917.0
    tourneyNum 9
    avgRank 96.4090909090909
    avgPoints 594.2045454545455
  ]
  node [
    id 46
    label "105902"
    name "James"
    surname "Duckworth"
    country "AUS"
    hand "R"
    dateOfBirth 19920121.0
    tourneyNum 3
    avgRank 149.89361702127658
    avgPoints 389.0425531914894
  ]
  node [
    id 47
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 21
    avgRank 63.48936170212766
    avgPoints 887.4255319148937
  ]
  node [
    id 48
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 27
    avgRank 36.87234042553192
    avgPoints 1217.9787234042553
  ]
  edge [
    source 0
    target 5
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 47
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 8
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 14
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 21
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 0
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 2
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 36
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 18
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 21
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 17
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 10
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 44
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 2
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 7
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 8
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 2
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 5
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 5
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 17
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 18
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 5
    target 6
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 48
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 19
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 8
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 5
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 9
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 10
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 8
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 8
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 7
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 21
    weight 5
    lowerId 2
    higherId 3
  ]
  edge [
    source 8
    target 10
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 8
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 24
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 8
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 18
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 8
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 14
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 47
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 10
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 18
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 21
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 10
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 11
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 10
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 17
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 11
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 17
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 32
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 16
    target 35
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 16
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 48
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 21
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 37
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 32
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 42
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 18
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 21
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 21
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 21
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 42
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 24
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 24
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 36
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 26
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 35
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 32
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 35
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 38
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 47
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 48
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 47
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 47
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
]
