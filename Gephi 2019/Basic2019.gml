graph [
  node [
    id 0
    label "110602"
    name "Dennis"
    surname "Novak"
    country "AUT"
    hand "R"
    dateOfBirth 19930828.0
    tourneyNum 8
    avgRank 125.61702127659575
    avgPoints 453.468085106383
  ]
  node [
    id 1
    label "104460"
    name "Dustin"
    surname "Brown"
    country "GER"
    hand "R"
    dateOfBirth 19841208.0
    tourneyNum 1
    avgRank 190.80851063829786
    avgPoints 278.36170212765956
  ]
  node [
    id 2
    label "104467"
    name "Lamine"
    surname "Ouahab"
    country "ALG"
    hand "R"
    dateOfBirth 19841222.0
    tourneyNum 1
    avgRank 567.8484848484849
    avgPoints 28.272727272727273
  ]
  node [
    id 3
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 28
    avgRank 38.212765957446805
    avgPoints 1202.9787234042553
  ]
  node [
    id 4
    label "208937"
    name "Kasidit"
    surname "Samrej"
    country "THA"
    hand "U"
    dateOfBirth 20010126.0
    tourneyNum 1
    avgRank 1179.0
    avgPoints 5.666666666666667
  ]
  node [
    id 5
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 20
    avgRank 28.127659574468087
    avgPoints 1520.9574468085107
  ]
  node [
    id 6
    label "104534"
    name "Dudi"
    surname "Sela"
    country "ISR"
    hand "R"
    dateOfBirth 19850404.0
    tourneyNum 2
    avgRank 178.61702127659575
    avgPoints 305.7659574468085
  ]
  node [
    id 7
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 22
    avgRank 79.65957446808511
    avgPoints 856.0425531914893
  ]
  node [
    id 8
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 18
    avgRank 13.74468085106383
    avgPoints 2471.063829787234
  ]
  node [
    id 9
    label "104563"
    name "Luca"
    surname "Vanni"
    country "ITA"
    hand "R"
    dateOfBirth 19850604.0
    tourneyNum 1
    avgRank 247.61702127659575
    avgPoints 224.2340425531915
  ]
  node [
    id 10
    label "104571"
    name "Marcos"
    surname "Baghdatis"
    country "CYP"
    hand "R"
    dateOfBirth 19850617.0
    tourneyNum 3
    avgRank 149.91489361702128
    avgPoints 372.8936170212766
  ]
  node [
    id 11
    label "104586"
    name "Lukas"
    surname "Rosol"
    country "CZE"
    hand "R"
    dateOfBirth 19850724.0
    tourneyNum 4
    avgRank 148.48936170212767
    avgPoints 376.2340425531915
  ]
  node [
    id 12
    label "110748"
    name "Nikola"
    surname "Milojevic"
    country "SRB"
    hand "U"
    dateOfBirth 19950619.0
    tourneyNum 1
    avgRank 162.51063829787233
    avgPoints 330.8723404255319
  ]
  node [
    id 13
    label "104607"
    name "Tomas"
    surname "Berdych"
    country "CZE"
    hand "R"
    dateOfBirth 19850917.0
    tourneyNum 9
    avgRank 96.4090909090909
    avgPoints 594.2045454545455
  ]
  node [
    id 14
    label "104620"
    name "Simone"
    surname "Bolelli"
    country "ITA"
    hand "R"
    dateOfBirth 19851008.0
    tourneyNum 3
    avgRank 232.89361702127658
    avgPoints 241.87234042553192
  ]
  node [
    id 15
    label "133297"
    name "Yosuke"
    surname "Watanuki"
    country "JPN"
    hand "U"
    dateOfBirth 19980412.0
    tourneyNum 1
    avgRank 223.93617021276594
    avgPoints 222.80851063829786
  ]
  node [
    id 16
    label "104629"
    name "Adrian"
    surname "Menendez Maceiras"
    country "ESP"
    hand "R"
    dateOfBirth 19851028.0
    tourneyNum 3
    avgRank 175.91489361702128
    avgPoints 324.2340425531915
  ]
  node [
    id 17
    label "127157"
    name "Daniel"
    surname "Altmaier"
    country "GER"
    hand "U"
    dateOfBirth 19980912.0
    tourneyNum 1
    avgRank 458.54347826086956
    avgPoints 54.391304347826086
  ]
  node [
    id 18
    label "110778"
    name "Joao Lucas"
    surname "Magalhaes Hueb De Menezes"
    country "BRA"
    hand "R"
    dateOfBirth 19961217.0
    tourneyNum 1
    avgRank 275.468085106383
    avgPoints 161.0
  ]
  node [
    id 19
    label "108739"
    name "Wilfredo"
    surname "Gonzalez"
    country "GUA"
    hand "R"
    dateOfBirth 19930106.0
    tourneyNum 1
    avgRank 1065.857142857143
    avgPoints 7.380952380952381
  ]
  node [
    id 20
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 24
    avgRank 56.87234042553192
    avgPoints 935.1914893617021
  ]
  node [
    id 21
    label "104656"
    name "Haydn"
    surname "Lewis"
    country "BAR"
    hand "L"
    dateOfBirth 19860102.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 22
    label "104660"
    name "Sergiy"
    surname "Stakhovsky"
    country "UKR"
    hand "R"
    dateOfBirth 19860106.0
    tourneyNum 7
    avgRank 136.51063829787233
    avgPoints 411.0851063829787
  ]
  node [
    id 23
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 18
    avgRank 74.27659574468085
    avgPoints 779.9787234042553
  ]
  node [
    id 24
    label "108763"
    name "Alberto Emmanuel"
    surname "Alvarado Larin"
    country "ESA"
    hand "U"
    dateOfBirth 19941201.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 25
    label "104678"
    name "Viktor"
    surname "Troicki"
    country "SRB"
    hand "R"
    dateOfBirth 19860210.0
    tourneyNum 4
    avgRank 189.91489361702128
    avgPoints 282.1276595744681
  ]
  node [
    id 26
    label "104719"
    name "Marcel"
    surname "Granollers"
    country "ESP"
    hand "R"
    dateOfBirth 19860412.0
    tourneyNum 12
    avgRank 107.31914893617021
    avgPoints 527.9787234042553
  ]
  node [
    id 27
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 5
    avgRank 26.361702127659573
    avgPoints 2800.531914893617
  ]
  node [
    id 28
    label "207134"
    name "Fitriadi"
    surname "M Rifqi"
    country "INA"
    hand "U"
    dateOfBirth 19990123.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 29
    label "104735"
    name "Tobias"
    surname "Kamke"
    country "GER"
    hand "R"
    dateOfBirth 19860521.0
    tourneyNum 1
    avgRank 234.40425531914894
    avgPoints 204.82978723404256
  ]
  node [
    id 30
    label "207139"
    name "Palaphoom"
    surname "Kovapitukted"
    country "THA"
    hand "U"
    dateOfBirth 19991205.0
    tourneyNum 1
    avgRank 1116.857142857143
    avgPoints 6.761904761904762
  ]
  node [
    id 31
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 25
    avgRank 5.042553191489362
    avgPoints 4747.234042553191
  ]
  node [
    id 32
    label "209188"
    name "Ari"
    surname "Fahresi"
    country "INA"
    hand "U"
    dateOfBirth 20020802.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 33
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 18
    avgRank 1.8085106382978724
    avgPoints 8636.170212765957
  ]
  node [
    id 34
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 18
    avgRank 44.808510638297875
    avgPoints 1111.276595744681
  ]
  node [
    id 35
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 32
    avgRank 24.72340425531915
    avgPoints 1574.5744680851064
  ]
  node [
    id 36
    label "127300"
    name "Djurabek"
    surname "Karimov"
    country "UZB"
    hand "U"
    dateOfBirth 19980604.0
    tourneyNum 1
    avgRank 720.2380952380952
    avgPoints 26.80952380952381
  ]
  node [
    id 37
    label "209226"
    name "Adolfo Daniel"
    surname "Vallejo"
    country "PAR"
    hand "U"
    dateOfBirth 20040428.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 38
    label "207182"
    name "Emilio"
    surname "Nava"
    country "USA"
    hand "U"
    dateOfBirth 20011202.0
    tourneyNum 1
    avgRank 793.1944444444445
    avgPoints 8.527777777777779
  ]
  node [
    id 39
    label "209238"
    name "Sebastian"
    surname "Dominguez"
    country "GUA"
    hand "U"
    dateOfBirth 20020622.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 40
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 22
    avgRank 16.914893617021278
    avgPoints 2027.340425531915
  ]
  node [
    id 41
    label "104797"
    name "Denis"
    surname "Istomin"
    country "UZB"
    hand "R"
    dateOfBirth 19860907.0
    tourneyNum 12
    avgRank 134.06382978723406
    avgPoints 445.78723404255317
  ]
  node [
    id 42
    label "104804"
    name "Amir"
    surname "Weintraub"
    country "ISR"
    hand "R"
    dateOfBirth 19860916.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 43
    label "104810"
    name "Zhe"
    surname "Li"
    country "CHN"
    hand "R"
    dateOfBirth 19860920.0
    tourneyNum 6
    avgRank 232.06382978723406
    avgPoints 207.93617021276594
  ]
  node [
    id 44
    label "127339"
    name "Borna"
    surname "Gojo"
    country "CRO"
    hand "U"
    dateOfBirth 19980227.0
    tourneyNum 2
    avgRank 309.02127659574467
    avgPoints 113.82978723404256
  ]
  node [
    id 45
    label "104868"
    name "James"
    surname "Ward"
    country "GBR"
    hand "R"
    dateOfBirth 19870209.0
    tourneyNum 3
    avgRank 236.27659574468086
    avgPoints 216.5531914893617
  ]
  node [
    id 46
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 27
    avgRank 54.340425531914896
    avgPoints 961.5957446808511
  ]
  node [
    id 47
    label "104897"
    name "Matthias"
    surname "Bachinger"
    country "GER"
    hand "R"
    dateOfBirth 19870402.0
    tourneyNum 4
    avgRank 151.4468085106383
    avgPoints 383.02127659574467
  ]
  node [
    id 48
    label "104898"
    name "Robin"
    surname "Haase"
    country "NED"
    hand "R"
    dateOfBirth 19870406.0
    tourneyNum 21
    avgRank 104.04255319148936
    avgPoints 601.9148936170212
  ]
  node [
    id 49
    label "104907"
    name "Jose Rubin"
    surname "Statham"
    country "NZL"
    hand "R"
    dateOfBirth 19870425.0
    tourneyNum 1
    avgRank 469.6595744680851
    avgPoints 52.680851063829785
  ]
  node [
    id 50
    label "104918"
    name "Andy"
    surname "Murray"
    country "GBR"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 9
    avgRank 227.2127659574468
    avgPoints 252.4255319148936
  ]
  node [
    id 51
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 18
    avgRank 73.80851063829788
    avgPoints 745.3617021276596
  ]
  node [
    id 52
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 18
    avgRank 1.1914893617021276
    avgPoints 10754.04255319149
  ]
  node [
    id 53
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 25
    avgRank 12.595744680851064
    avgPoints 2402.2340425531916
  ]
  node [
    id 54
    label "104970"
    name "Matteo"
    surname "Viola"
    country "ITA"
    hand "R"
    dateOfBirth 19870707.0
    tourneyNum 1
    avgRank 241.19148936170214
    avgPoints 194.70212765957447
  ]
  node [
    id 55
    label "104978"
    name "Daniel"
    surname "Brands"
    country "GER"
    hand "R"
    dateOfBirth 19870717.0
    tourneyNum 1
    avgRank 292.5744680851064
    avgPoints 192.4255319148936
  ]
  node [
    id 56
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 12
    avgRank 184.27659574468086
    avgPoints 394.8510638297872
  ]
  node [
    id 57
    label "111153"
    name "Christopher"
    surname "Eubanks"
    country "USA"
    hand "R"
    dateOfBirth 19960505.0
    tourneyNum 6
    avgRank 189.31914893617022
    avgPoints 281.531914893617
  ]
  node [
    id 58
    label "105011"
    name "Illya"
    surname "Marchenko"
    country "UKR"
    hand "R"
    dateOfBirth 19870908.0
    tourneyNum 1
    avgRank 355.1063829787234
    avgPoints 86.25531914893617
  ]
  node [
    id 59
    label "105015"
    name "Franco"
    surname "Skugor"
    country "CRO"
    hand "R"
    dateOfBirth 19870920.0
    tourneyNum 1
    avgRank 513.3617021276596
    avgPoints 40.57446808510638
  ]
  node [
    id 60
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 17
    avgRank 52.51063829787234
    avgPoints 960.7446808510638
  ]
  node [
    id 61
    label "105041"
    name "Lukas"
    surname "Lacko"
    country "SVK"
    hand "R"
    dateOfBirth 19871103.0
    tourneyNum 3
    avgRank 171.5531914893617
    avgPoints 332.0851063829787
  ]
  node [
    id 62
    label "111190"
    name "Zhizhen"
    surname "Zhang"
    country "CHN"
    hand "U"
    dateOfBirth 19961016.0
    tourneyNum 4
    avgRank 279.3191489361702
    avgPoints 167.2127659574468
  ]
  node [
    id 63
    label "111192"
    name "Lucas"
    surname "Miedler"
    country "AUT"
    hand "R"
    dateOfBirth 19960621.0
    tourneyNum 1
    avgRank 260.1063829787234
    avgPoints 173.31914893617022
  ]
  node [
    id 64
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 16
    avgRank 118.70212765957447
    avgPoints 612.0851063829788
  ]
  node [
    id 65
    label "105053"
    name "Santiago"
    surname "Giraldo"
    country "COL"
    hand "R"
    dateOfBirth 19871127.0
    tourneyNum 4
    avgRank 239.87234042553192
    avgPoints 197.31914893617022
  ]
  node [
    id 66
    label "111200"
    name "Elias"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 6
    avgRank 126.55319148936171
    avgPoints 452.6808510638298
  ]
  node [
    id 67
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 8
    avgRank 121.65957446808511
    avgPoints 526.9574468085107
  ]
  node [
    id 68
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 28
    avgRank 52.723404255319146
    avgPoints 954.6595744680851
  ]
  node [
    id 69
    label "105063"
    name "Andrea"
    surname "Arnaboldi"
    country "ITA"
    hand "L"
    dateOfBirth 19871227.0
    tourneyNum 1
    avgRank 244.4468085106383
    avgPoints 206.91489361702128
  ]
  node [
    id 70
    label "105065"
    name "Tim"
    surname "Smyczek"
    country "USA"
    hand "R"
    dateOfBirth 19871230.0
    tourneyNum 3
    avgRank 264.8723404255319
    avgPoints 184.48936170212767
  ]
  node [
    id 71
    label "117353"
    name "Duck Hee"
    surname "Lee"
    country "KOR"
    hand "R"
    dateOfBirth 19980529.0
    tourneyNum 1
    avgRank 235.72340425531914
    avgPoints 203.89361702127658
  ]
  node [
    id 72
    label "117356"
    name "Cem"
    surname "Ilkel"
    country "TUR"
    hand "R"
    dateOfBirth 19950821.0
    tourneyNum 3
    avgRank 268.5531914893617
    avgPoints 162.95744680851064
  ]
  node [
    id 73
    label "117360"
    name "Marc"
    surname "Polmans"
    country "AUS"
    hand "U"
    dateOfBirth 19970502.0
    tourneyNum 2
    avgRank 167.48936170212767
    avgPoints 328.8936170212766
  ]
  node [
    id 74
    label "105074"
    name "Ruben"
    surname "Bemelmans"
    country "BEL"
    hand "L"
    dateOfBirth 19880114.0
    tourneyNum 2
    avgRank 188.5531914893617
    avgPoints 291.9148936170213
  ]
  node [
    id 75
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 25
    avgRank 67.25531914893617
    avgPoints 847.8936170212766
  ]
  node [
    id 76
    label "105091"
    name "Peter"
    surname "Torebko"
    country "GER"
    hand "R"
    dateOfBirth 19880210.0
    tourneyNum 1
    avgRank 413.4255319148936
    avgPoints 64.91489361702128
  ]
  node [
    id 77
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 26
    avgRank 15.446808510638299
    avgPoints 2115.2127659574467
  ]
  node [
    id 78
    label "105147"
    name "Tatsuma"
    surname "Ito"
    country "JPN"
    hand "R"
    dateOfBirth 19880518.0
    tourneyNum 3
    avgRank 145.89361702127658
    avgPoints 379.4255319148936
  ]
  node [
    id 79
    label "121531"
    name "Mats"
    surname "Moraing"
    country "GER"
    hand "L"
    dateOfBirth 19920620.0
    tourneyNum 1
    avgRank 188.7872340425532
    avgPoints 274.93617021276594
  ]
  node [
    id 80
    label "105155"
    name "Pedro"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19880527.0
    tourneyNum 9
    avgRank 118.0
    avgPoints 483.21276595744683
  ]
  node [
    id 81
    label "105156"
    name "Federico"
    surname "Zeballos"
    country "BOL"
    hand "U"
    dateOfBirth 19880530.0
    tourneyNum 1
    avgRank 585.7619047619048
    avgPoints 40.95238095238095
  ]
  node [
    id 82
    label "105163"
    name "Gleb"
    surname "Sakharov"
    country "FRA"
    hand "R"
    dateOfBirth 19880610.0
    tourneyNum 1
    avgRank 286.2340425531915
    avgPoints 159.48936170212767
  ]
  node [
    id 83
    label "105166"
    name "Peter"
    surname "Polansky"
    country "CAN"
    hand "R"
    dateOfBirth 19880615.0
    tourneyNum 3
    avgRank 152.40425531914894
    avgPoints 368.72340425531917
  ]
  node [
    id 84
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 27
    avgRank 49.91489361702128
    avgPoints 1003.8510638297872
  ]
  node [
    id 85
    label "105208"
    name "Ernests"
    surname "Gulbis"
    country "LAT"
    hand "R"
    dateOfBirth 19880830.0
    tourneyNum 17
    avgRank 127.53191489361703
    avgPoints 502.9574468085106
  ]
  node [
    id 86
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 4
    avgRank 157.40425531914894
    avgPoints 371.1063829787234
  ]
  node [
    id 87
    label "105217"
    name "Thiemo"
    surname "De Bakker"
    country "NED"
    hand "R"
    dateOfBirth 19880919.0
    tourneyNum 1
    avgRank 370.1914893617021
    avgPoints 132.12765957446808
  ]
  node [
    id 88
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 5
    avgRank 45.1063829787234
    avgPoints 2415.6382978723404
  ]
  node [
    id 89
    label "105226"
    name "Attila"
    surname "Balazs"
    country "HUN"
    hand "R"
    dateOfBirth 19880927.0
    tourneyNum 3
    avgRank 187.87234042553192
    avgPoints 302.9574468085106
  ]
  node [
    id 90
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 20
    avgRank 20.382978723404257
    avgPoints 2191.595744680851
  ]
  node [
    id 91
    label "105254"
    name "Nikola"
    surname "Mektic"
    country "CRO"
    hand "R"
    dateOfBirth 19881224.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 92
    label "105292"
    name "Alejandro"
    surname "Gonzalez"
    country "COL"
    hand "R"
    dateOfBirth 19890207.0
    tourneyNum 2
    avgRank 416.78723404255317
    avgPoints 59.12765957446808
  ]
  node [
    id 93
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 22
    avgRank 60.744680851063826
    avgPoints 874.5531914893617
  ]
  node [
    id 94
    label "133975"
    name "Benjamin"
    surname "Hassan"
    country "GER"
    hand "U"
    dateOfBirth 19950204.0
    tourneyNum 1
    avgRank 331.2340425531915
    avgPoints 96.25531914893617
  ]
  node [
    id 95
    label "111453"
    name "Mikael"
    surname "Torpegaard"
    country "DEN"
    hand "R"
    dateOfBirth 19940508.0
    tourneyNum 1
    avgRank 204.0212765957447
    avgPoints 252.87234042553192
  ]
  node [
    id 96
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 30
    avgRank 54.787234042553195
    avgPoints 938.1702127659574
  ]
  node [
    id 97
    label "111456"
    name "Mackenzie"
    surname "Mcdonald"
    country "USA"
    hand "R"
    dateOfBirth 19950416.0
    tourneyNum 12
    avgRank 92.23404255319149
    avgPoints 633.5744680851063
  ]
  node [
    id 98
    label "111459"
    name "Arjun"
    surname "Kadhe"
    country "IND"
    hand "R"
    dateOfBirth 19940107.0
    tourneyNum 1
    avgRank 477.5531914893617
    avgPoints 39.02127659574468
  ]
  node [
    id 99
    label "111460"
    name "Quentin"
    surname "Halys"
    country "FRA"
    hand "R"
    dateOfBirth 19961026.0
    tourneyNum 1
    avgRank 172.3404255319149
    avgPoints 317.59574468085106
  ]
  node [
    id 100
    label "125802"
    name "Ilya"
    surname "Ivashka"
    country "BLR"
    hand "R"
    dateOfBirth 19940224.0
    tourneyNum 12
    avgRank 121.70212765957447
    avgPoints 474.17021276595744
  ]
  node [
    id 101
    label "123755"
    name "Daniel Elahi"
    surname "Galan Riveros"
    country "COL"
    hand "U"
    dateOfBirth 19960618.0
    tourneyNum 5
    avgRank 206.9787234042553
    avgPoints 244.53191489361703
  ]
  node [
    id 102
    label "131951"
    name "Daniel"
    surname "Cukierman"
    country "ISR"
    hand "U"
    dateOfBirth 19950709.0
    tourneyNum 1
    avgRank 856.9047619047619
    avgPoints 15.0
  ]
  node [
    id 103
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 31
    avgRank 38.08510638297872
    avgPoints 1230.0212765957447
  ]
  node [
    id 104
    label "105334"
    name "Luis David"
    surname "Martinez"
    country "VEN"
    hand "U"
    dateOfBirth 19890512.0
    tourneyNum 1
    avgRank 573.1333333333333
    avgPoints 5.0
  ]
  node [
    id 105
    label "105339"
    name "Yan"
    surname "Bai"
    country "CHN"
    hand "R"
    dateOfBirth 19890521.0
    tourneyNum 2
    avgRank 300.9574468085106
    avgPoints 127.87234042553192
  ]
  node [
    id 106
    label "105341"
    name "Thomas"
    surname "Fabbiano"
    country "ITA"
    hand "R"
    dateOfBirth 19890526.0
    tourneyNum 18
    avgRank 96.68085106382979
    avgPoints 587.2127659574468
  ]
  node [
    id 107
    label "105349"
    name "Harri"
    surname "Heliovaara"
    country "FIN"
    hand "R"
    dateOfBirth 19890604.0
    tourneyNum 1
    avgRank 538.0212765957447
    avgPoints 31.595744680851062
  ]
  node [
    id 108
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 29
    avgRank 52.787234042553195
    avgPoints 966.2553191489362
  ]
  node [
    id 109
    label "105359"
    name "Jason"
    surname "Jung"
    country "TPE"
    hand "R"
    dateOfBirth 19890615.0
    tourneyNum 7
    avgRank 128.3404255319149
    avgPoints 435.9574468085106
  ]
  node [
    id 110
    label "125842"
    name "Adam"
    surname "Moundir"
    country "SUI"
    hand "U"
    dateOfBirth 19950426.0
    tourneyNum 1
    avgRank 481.76190476190476
    avgPoints 62.666666666666664
  ]
  node [
    id 111
    label "123795"
    name "Altug"
    surname "Celikbilek"
    country "TUR"
    hand "U"
    dateOfBirth 19960907.0
    tourneyNum 2
    avgRank 404.72340425531917
    avgPoints 62.234042553191486
  ]
  node [
    id 112
    label "111511"
    name "Noah"
    surname "Rubin"
    country "USA"
    hand "R"
    dateOfBirth 19960221.0
    tourneyNum 3
    avgRank 178.38297872340425
    avgPoints 303.1914893617021
  ]
  node [
    id 113
    label "111513"
    name "Laslo"
    surname "Djere"
    country "SRB"
    hand "R"
    dateOfBirth 19950602.0
    tourneyNum 23
    avgRank 42.255319148936174
    avgPoints 1151.0851063829787
  ]
  node [
    id 114
    label "111515"
    name "Thai Son"
    surname "Kwiatkowski"
    country "USA"
    hand "U"
    dateOfBirth 19950213.0
    tourneyNum 1
    avgRank 234.0212765957447
    avgPoints 206.85106382978722
  ]
  node [
    id 115
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 20
    avgRank 82.91489361702128
    avgPoints 729.936170212766
  ]
  node [
    id 116
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 21
    avgRank 97.44680851063829
    avgPoints 581.8936170212766
  ]
  node [
    id 117
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 19
    avgRank 73.2127659574468
    avgPoints 757.0212765957447
  ]
  node [
    id 118
    label "103333"
    name "Ivo"
    surname "Karlovic"
    country "CRO"
    hand "R"
    dateOfBirth 19790228.0
    tourneyNum 15
    avgRank 87.23404255319149
    avgPoints 640.9148936170212
  ]
  node [
    id 119
    label "205734"
    name "Thiago"
    surname "Seyboth Wild"
    country "BRA"
    hand "R"
    dateOfBirth 20000310.0
    tourneyNum 3
    avgRank 343.36170212765956
    avgPoints 98.12765957446808
  ]
  node [
    id 120
    label "105385"
    name "Donald"
    surname "Young"
    country "USA"
    hand "L"
    dateOfBirth 19890723.0
    tourneyNum 2
    avgRank 213.31914893617022
    avgPoints 236.5744680851064
  ]
  node [
    id 121
    label "207797"
    name "Jonas"
    surname "Forejtek"
    country "CZE"
    hand "R"
    dateOfBirth 20010310.0
    tourneyNum 1
    avgRank 563.7692307692307
    avgPoints 38.07692307692308
  ]
  node [
    id 122
    label "105413"
    name "Andrej"
    surname "Martin"
    country "SVK"
    hand "R"
    dateOfBirth 19890920.0
    tourneyNum 2
    avgRank 157.91489361702128
    avgPoints 380.9148936170213
  ]
  node [
    id 123
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 27
    avgRank 50.234042553191486
    avgPoints 1001.2553191489362
  ]
  node [
    id 124
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 31
    avgRank 11.595744680851064
    avgPoints 2647.021276595745
  ]
  node [
    id 125
    label "105432"
    name "Prajnesh"
    surname "Gunneswaran"
    country "IND"
    hand "L"
    dateOfBirth 19891112.0
    tourneyNum 13
    avgRank 95.29787234042553
    avgPoints 593.5957446808511
  ]
  node [
    id 126
    label "111577"
    name "Jared"
    surname "Donaldson"
    country "USA"
    hand "R"
    dateOfBirth 19961009.0
    tourneyNum 2
    avgRank 443.0425531914894
    avgPoints 150.53191489361703
  ]
  node [
    id 127
    label "111574"
    name "Jc"
    surname "Aragone"
    country "USA"
    hand "R"
    dateOfBirth 19950628.0
    tourneyNum 1
    avgRank 270.0
    avgPoints 162.17021276595744
  ]
  node [
    id 128
    label "111576"
    name "Sumit"
    surname "Nagal"
    country "IND"
    hand "R"
    dateOfBirth 19970816.0
    tourneyNum 2
    avgRank 234.82978723404256
    avgPoints 234.10638297872342
  ]
  node [
    id 129
    label "105436"
    name "Markus"
    surname "Eriksson"
    country "SWE"
    hand "R"
    dateOfBirth 19891129.0
    tourneyNum 1
    avgRank 370.6170212765957
    avgPoints 76.06382978723404
  ]
  node [
    id 130
    label "111581"
    name "Michael"
    surname "Mmoh"
    country "USA"
    hand "R"
    dateOfBirth 19980110.0
    tourneyNum 2
    avgRank 173.89361702127658
    avgPoints 342.9574468085106
  ]
  node [
    id 131
    label "105434"
    name "Ruan"
    surname "Roelofse"
    country "RSA"
    hand "R"
    dateOfBirth 19891118.0
    tourneyNum 1
    avgRank 609.0
    avgPoints 16.72340425531915
  ]
  node [
    id 132
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 21
    avgRank 69.02127659574468
    avgPoints 809.8936170212766
  ]
  node [
    id 133
    label "207852"
    name "Wai Yu"
    surname "Kai"
    country "HKG"
    hand "U"
    dateOfBirth 20010320.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 134
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 15
    avgRank 8.27659574468085
    avgPoints 3509.68085106383
  ]
  node [
    id 135
    label "105477"
    name "Marco"
    surname "Trungelliti"
    country "ARG"
    hand "R"
    dateOfBirth 19900131.0
    tourneyNum 3
    avgRank 168.29787234042553
    avgPoints 341.25531914893617
  ]
  node [
    id 136
    label "105487"
    name "Facundo"
    surname "Bagnis"
    country "ARG"
    hand "L"
    dateOfBirth 19900227.0
    tourneyNum 5
    avgRank 146.87234042553192
    avgPoints 379.25531914893617
  ]
  node [
    id 137
    label "105493"
    name "Tomislav"
    surname "Brkic"
    country "BIH"
    hand "R"
    dateOfBirth 19900309.0
    tourneyNum 1
    avgRank 400.0425531914894
    avgPoints 87.57446808510639
  ]
  node [
    id 138
    label "105497"
    name "Jose"
    surname "Hernandez"
    country "DOM"
    hand "U"
    dateOfBirth 19900313.0
    tourneyNum 1
    avgRank 274.1914893617021
    avgPoints 155.53191489361703
  ]
  node [
    id 139
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 25
    avgRank 48.680851063829785
    avgPoints 1025.9787234042553
  ]
  node [
    id 140
    label "121896"
    name "Gabor"
    surname "Borsos"
    country "HUN"
    hand "U"
    dateOfBirth 19910630.0
    tourneyNum 1
    avgRank 1003.12
    avgPoints 7.56
  ]
  node [
    id 141
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 30
    avgRank 41.808510638297875
    avgPoints 1121.3829787234042
  ]
  node [
    id 142
    label "105539"
    name "Evgeny"
    surname "Donskoy"
    country "RUS"
    hand "R"
    dateOfBirth 19900509.0
    tourneyNum 6
    avgRank 118.40425531914893
    avgPoints 482.7659574468085
  ]
  node [
    id 143
    label "109640"
    name "Andrea"
    surname "Basso"
    country "ITA"
    hand "L"
    dateOfBirth 19931008.0
    tourneyNum 1
    avgRank 460.17021276595744
    avgPoints 42.93617021276596
  ]
  node [
    id 144
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 29
    avgRank 29.638297872340427
    avgPoints 1412.5531914893618
  ]
  node [
    id 145
    label "123983"
    name "Nerman"
    surname "Fatic"
    country "BIH"
    hand "U"
    dateOfBirth 19941024.0
    tourneyNum 1
    avgRank 430.6923076923077
    avgPoints 75.03846153846153
  ]
  node [
    id 146
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 22
    avgRank 74.87234042553192
    avgPoints 832.9787234042553
  ]
  node [
    id 147
    label "105561"
    name "Alessandro"
    surname "Giannessi"
    country "ITA"
    hand "L"
    dateOfBirth 19900530.0
    tourneyNum 2
    avgRank 157.7659574468085
    avgPoints 344.2340425531915
  ]
  node [
    id 148
    label "105575"
    name "Ricardas"
    surname "Berankis"
    country "LTU"
    hand "R"
    dateOfBirth 19900621.0
    tourneyNum 13
    avgRank 80.46808510638297
    avgPoints 727.3617021276596
  ]
  node [
    id 149
    label "105577"
    name "Vasek"
    surname "Pospisil"
    country "CAN"
    hand "R"
    dateOfBirth 19900623.0
    tourneyNum 9
    avgRank 160.2340425531915
    avgPoints 365.27659574468083
  ]
  node [
    id 150
    label "124014"
    name "Ernesto"
    surname "Escobedo"
    country "USA"
    hand "R"
    dateOfBirth 19960704.0
    tourneyNum 1
    avgRank 243.5744680851064
    avgPoints 192.9787234042553
  ]
  node [
    id 151
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 28
    avgRank 34.851063829787236
    avgPoints 1254.0851063829787
  ]
  node [
    id 152
    label "105585"
    name "Ze"
    surname "Zhang"
    country "CHN"
    hand "R"
    dateOfBirth 19900704.0
    tourneyNum 3
    avgRank 272.97872340425533
    avgPoints 179.63829787234042
  ]
  node [
    id 153
    label "109699"
    name "Julian"
    surname "Lenz"
    country "GER"
    hand "R"
    dateOfBirth 19930217.0
    tourneyNum 2
    avgRank 324.8723404255319
    avgPoints 107.44680851063829
  ]
  node [
    id 154
    label "105613"
    name "Norbert"
    surname "Gombos"
    country "SVK"
    hand "R"
    dateOfBirth 19900813.0
    tourneyNum 3
    avgRank 167.93617021276594
    avgPoints 358.21276595744683
  ]
  node [
    id 155
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 24
    avgRank 63.46808510638298
    avgPoints 973.6382978723404
  ]
  node [
    id 156
    label "105614"
    name "Bradley"
    surname "Klahn"
    country "USA"
    hand "L"
    dateOfBirth 19900820.0
    tourneyNum 14
    avgRank 98.93617021276596
    avgPoints 579.7446808510638
  ]
  node [
    id 157
    label "111761"
    name "Benjamin"
    surname "Lock"
    country "ZIM"
    hand "R"
    dateOfBirth 19930304.0
    tourneyNum 1
    avgRank 510.51063829787233
    avgPoints 34.659574468085104
  ]
  node [
    id 158
    label "105626"
    name "Takanyi"
    surname "Garanganga"
    country "ZIM"
    hand "R"
    dateOfBirth 19900906.0
    tourneyNum 1
    avgRank 500.25531914893617
    avgPoints 31.48936170212766
  ]
  node [
    id 159
    label "208029"
    name "Holger Vitus Nodskov"
    surname "Rune"
    country "DEN"
    hand "U"
    dateOfBirth 20030429.0
    tourneyNum 1
    avgRank 970.7307692307693
    avgPoints 7.0
  ]
  node [
    id 160
    label "105633"
    name "Mohamed"
    surname "Safwat"
    country "EGY"
    hand "R"
    dateOfBirth 19900919.0
    tourneyNum 2
    avgRank 213.2340425531915
    avgPoints 235.29787234042553
  ]
  node [
    id 161
    label "105634"
    name "Viktor"
    surname "Galovic"
    country "ITA"
    hand "R"
    dateOfBirth 19900919.0
    tourneyNum 2
    avgRank 223.93617021276594
    avgPoints 223.4468085106383
  ]
  node [
    id 162
    label "126120"
    name "Nino"
    surname "Serdarusic"
    country "CRO"
    hand "U"
    dateOfBirth 19961213.0
    tourneyNum 1
    avgRank 273.25531914893617
    avgPoints 155.59574468085106
  ]
  node [
    id 163
    label "105641"
    name "Blaz"
    surname "Rola"
    country "SLO"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 2
    avgRank 170.31914893617022
    avgPoints 325.17021276595744
  ]
  node [
    id 164
    label "109739"
    name "Maximilian"
    surname "Marterer"
    country "GER"
    hand "L"
    dateOfBirth 19950615.0
    tourneyNum 11
    avgRank 169.5744680851064
    avgPoints 380.59574468085106
  ]
  node [
    id 165
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 20
    avgRank 74.63829787234043
    avgPoints 745.9574468085107
  ]
  node [
    id 166
    label "126125"
    name "Maxime"
    surname "Janvier"
    country "FRA"
    hand "U"
    dateOfBirth 19961018.0
    tourneyNum 3
    avgRank 199.12765957446808
    avgPoints 257.4468085106383
  ]
  node [
    id 167
    label "111790"
    name "Brayden"
    surname "Schnur"
    country "CAN"
    hand "R"
    dateOfBirth 19950704.0
    tourneyNum 8
    avgRank 111.29787234042553
    avgPoints 521.7659574468086
  ]
  node [
    id 168
    label "124079"
    name "Pedro"
    surname "Martinez Portero"
    country "ESP"
    hand "R"
    dateOfBirth 19970426.0
    tourneyNum 2
    avgRank 154.48936170212767
    avgPoints 353.8723404255319
  ]
  node [
    id 169
    label "105649"
    name "Cedrik Marcel"
    surname "Stebe"
    country "GER"
    hand "L"
    dateOfBirth 19901009.0
    tourneyNum 10
    avgRank 280.1666666666667
    avgPoints 187.66666666666666
  ]
  node [
    id 170
    label "111794"
    name "Kamil"
    surname "Majchrzak"
    country "POL"
    hand "R"
    dateOfBirth 19960113.0
    tourneyNum 6
    avgRank 115.31914893617021
    avgPoints 507.5531914893617
  ]
  node [
    id 171
    label "111795"
    name "Filippo"
    surname "Baldi"
    country "ITA"
    hand "R"
    dateOfBirth 19960110.0
    tourneyNum 2
    avgRank 173.19148936170214
    avgPoints 318.70212765957444
  ]
  node [
    id 172
    label "111797"
    name "Nicolas"
    surname "Jarry"
    country "CHI"
    hand "R"
    dateOfBirth 19951011.0
    tourneyNum 23
    avgRank 68.38297872340425
    avgPoints 797.3191489361702
  ]
  node [
    id 173
    label "208055"
    name "Ajeet"
    surname "Rai"
    country "NZL"
    hand "R"
    dateOfBirth 19990118.0
    tourneyNum 1
    avgRank 886.6190476190476
    avgPoints 15.761904761904763
  ]
  node [
    id 174
    label "105656"
    name "Marcelo"
    surname "Arevalo"
    country "ESA"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 2
    avgRank 296.1276595744681
    avgPoints 145.80851063829786
  ]
  node [
    id 175
    label "105657"
    name "Marius"
    surname "Copil"
    country "ROU"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 20
    avgRank 95.85106382978724
    avgPoints 608.5531914893617
  ]
  node [
    id 176
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 24
    avgRank 70.57446808510639
    avgPoints 823.4893617021277
  ]
  node [
    id 177
    label "126149"
    name "Gianluca"
    surname "Mager"
    country "ITA"
    hand "R"
    dateOfBirth 19941201.0
    tourneyNum 1
    avgRank 151.04255319148936
    avgPoints 382.40425531914894
  ]
  node [
    id 178
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 25
    avgRank 56.744680851063826
    avgPoints 918.1063829787234
  ]
  node [
    id 179
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 28
    avgRank 18.595744680851062
    avgPoints 1879.3617021276596
  ]
  node [
    id 180
    label "126156"
    name "Antonie"
    surname "Hoang"
    country "FRA"
    hand "R"
    dateOfBirth 19951104.0
    tourneyNum 7
    avgRank 122.8936170212766
    avgPoints 465.6170212765957
  ]
  node [
    id 181
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 13
    avgRank 21.48936170212766
    avgPoints 1780.0
  ]
  node [
    id 182
    label "124116"
    name "Sebastian"
    surname "Ofner"
    country "AUT"
    hand "R"
    dateOfBirth 19960512.0
    tourneyNum 2
    avgRank 162.08510638297872
    avgPoints 330.1914893617021
  ]
  node [
    id 183
    label "105688"
    name "Manuel"
    surname "Sanchez"
    country "MEX"
    hand "R"
    dateOfBirth 19910105.0
    tourneyNum 1
    avgRank 645.9574468085107
    avgPoints 13.957446808510639
  ]
  node [
    id 184
    label "208103"
    name "Jiri"
    surname "Lehecka"
    country "CZE"
    hand "R"
    dateOfBirth 20011108.0
    tourneyNum 1
    avgRank 558.34375
    avgPoints 30.0
  ]
  node [
    id 185
    label "126190"
    name "Nicolas"
    surname "Alvarez"
    country "PER"
    hand "R"
    dateOfBirth 19960608.0
    tourneyNum 1
    avgRank 453.48936170212767
    avgPoints 52.59574468085106
  ]
  node [
    id 186
    label "136440"
    name "Dominik"
    surname "Koepfer"
    country "GER"
    hand "L"
    dateOfBirth 19940429.0
    tourneyNum 4
    avgRank 133.2127659574468
    avgPoints 445.48936170212767
  ]
  node [
    id 187
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 31
    avgRank 39.04255319148936
    avgPoints 1185.2553191489362
  ]
  node [
    id 188
    label "126204"
    name "Gerardo"
    surname "Lopez Villasenor"
    country "MEX"
    hand "U"
    dateOfBirth 19950512.0
    tourneyNum 1
    avgRank 546.468085106383
    avgPoints 25.02127659574468
  ]
  node [
    id 189
    label "126205"
    name "Tommy"
    surname "Paul"
    country "USA"
    hand "R"
    dateOfBirth 19970517.0
    tourneyNum 4
    avgRank 134.2340425531915
    avgPoints 454.40425531914894
  ]
  node [
    id 190
    label "122109"
    name "Sanjar"
    surname "Fayziev"
    country "UZB"
    hand "U"
    dateOfBirth 19940729.0
    tourneyNum 2
    avgRank 454.7659574468085
    avgPoints 48.829787234042556
  ]
  node [
    id 191
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 40.02127659574468
    avgPoints 1147.872340425532
  ]
  node [
    id 192
    label "105731"
    name "Steven"
    surname "Diez"
    country "CAN"
    hand "R"
    dateOfBirth 19910317.0
    tourneyNum 1
    avgRank 222.5744680851064
    avgPoints 245.70212765957447
  ]
  node [
    id 193
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 24
    avgRank 50.87234042553192
    avgPoints 979.8936170212766
  ]
  node [
    id 194
    label "144650"
    name "Rhett"
    surname "Purcell"
    country "GBR"
    hand "R"
    dateOfBirth 19960206.0
    tourneyNum 1
    avgRank 911.0
    avgPoints 13.238095238095237
  ]
  node [
    id 195
    label "201995"
    name "Cayetano"
    surname "March"
    country "ECU"
    hand "U"
    dateOfBirth 20000703.0
    tourneyNum 1
    avgRank 754.2857142857143
    avgPoints 22.238095238095237
  ]
  node [
    id 196
    label "105747"
    name "Karim Mohamed"
    surname "Maamoun"
    country "EGY"
    hand "R"
    dateOfBirth 19910409.0
    tourneyNum 1
    avgRank 407.6170212765957
    avgPoints 59.61702127659574
  ]
  node [
    id 197
    label "124186"
    name "Alexandre"
    surname "Muller"
    country "FRA"
    hand "U"
    dateOfBirth 19970201.0
    tourneyNum 1
    avgRank 285.9574468085106
    avgPoints 138.0212765957447
  ]
  node [
    id 198
    label "124187"
    name "Reilly"
    surname "Opelka"
    country "USA"
    hand "R"
    dateOfBirth 19970828.0
    tourneyNum 24
    avgRank 53.659574468085104
    avgPoints 985.4468085106383
  ]
  node [
    id 199
    label "126237"
    name "Gian Marco"
    surname "Moroni"
    country "ITA"
    hand "U"
    dateOfBirth 19980213.0
    tourneyNum 1
    avgRank 252.93617021276594
    avgPoints 183.17021276595744
  ]
  node [
    id 200
    label "105757"
    name "Sandro"
    surname "Ehrat"
    country "SUI"
    hand "R"
    dateOfBirth 19910418.0
    tourneyNum 2
    avgRank 423.06382978723406
    avgPoints 65.59574468085107
  ]
  node [
    id 201
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 21
    avgRank 35.1063829787234
    avgPoints 1320.127659574468
  ]
  node [
    id 202
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 23
    avgRank 36.40425531914894
    avgPoints 1390.595744680851
  ]
  node [
    id 203
    label "144707"
    name "Mikael"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19980909.0
    tourneyNum 6
    avgRank 130.59574468085106
    avgPoints 489.06382978723406
  ]
  node [
    id 204
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 21
    avgRank 64.44680851063829
    avgPoints 847.1489361702128
  ]
  node [
    id 205
    label "105806"
    name "Mirza"
    surname "Basic"
    country "BIH"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 4
    avgRank 213.2127659574468
    avgPoints 260.21276595744683
  ]
  node [
    id 206
    label "144719"
    name "Jaume"
    surname "Munar"
    country "ESP"
    hand "R"
    dateOfBirth 19970505.0
    tourneyNum 22
    avgRank 80.95744680851064
    avgPoints 687.3404255319149
  ]
  node [
    id 207
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 23
    avgRank 38.06382978723404
    avgPoints 1257.4893617021276
  ]
  node [
    id 208
    label "105812"
    name "David"
    surname "Agung Susanto"
    country "INA"
    hand "U"
    dateOfBirth 19910721.0
    tourneyNum 1
    avgRank 1291.095238095238
    avgPoints 3.9523809523809526
  ]
  node [
    id 209
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 18
    avgRank 77.25531914893617
    avgPoints 746.3404255319149
  ]
  node [
    id 210
    label "105819"
    name "Guido"
    surname "Andreozzi"
    country "ARG"
    hand "R"
    dateOfBirth 19910805.0
    tourneyNum 14
    avgRank 104.82978723404256
    avgPoints 552.5531914893617
  ]
  node [
    id 211
    label "206173"
    name "Jannik"
    surname "Sinner"
    country "ITA"
    hand "U"
    dateOfBirth 20010816.0
    tourneyNum 9
    avgRank 229.4468085106383
    avgPoints 311.468085106383
  ]
  node [
    id 212
    label "200031"
    name "Lucas"
    surname "Catarina"
    country "MON"
    hand "R"
    dateOfBirth 19960826.0
    tourneyNum 1
    avgRank 514.2127659574468
    avgPoints 32.276595744680854
  ]
  node [
    id 213
    label "105827"
    name "Laurynas"
    surname "Grigelis"
    country "LTU"
    hand "R"
    dateOfBirth 19910814.0
    tourneyNum 1
    avgRank 348.3191489361702
    avgPoints 109.7872340425532
  ]
  node [
    id 214
    label "208230"
    name "Alibek"
    surname "Kachmazov"
    country "RUS"
    hand "U"
    dateOfBirth 20020817.0
    tourneyNum 1
    avgRank 1157.7142857142858
    avgPoints 6.714285714285714
  ]
  node [
    id 215
    label "202090"
    name "Adrian"
    surname "Andreev"
    country "BUL"
    hand "U"
    dateOfBirth 20010512.0
    tourneyNum 1
    avgRank 795.2380952380952
    avgPoints 21.238095238095237
  ]
  node [
    id 216
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 14
    avgRank 97.65957446808511
    avgPoints 583.7234042553191
  ]
  node [
    id 217
    label "105842"
    name "Di"
    surname "Wu"
    country "CHN"
    hand "R"
    dateOfBirth 19910914.0
    tourneyNum 1
    avgRank 343.5744680851064
    avgPoints 92.74468085106383
  ]
  node [
    id 218
    label "202103"
    name "Francisco"
    surname "Cerundolo"
    country "ARG"
    hand "U"
    dateOfBirth 19980813.0
    tourneyNum 1
    avgRank 313.32142857142856
    avgPoints 137.71428571428572
  ]
  node [
    id 219
    label "144760"
    name "Amine"
    surname "Ahouda"
    country "MAR"
    hand "U"
    dateOfBirth 19970911.0
    tourneyNum 1
    avgRank 1600.3809523809523
    avgPoints 1.0
  ]
  node [
    id 220
    label "126328"
    name "Gabriel"
    surname "Donev"
    country "BUL"
    hand "U"
    dateOfBirth 19960629.0
    tourneyNum 1
    avgRank 856.1904761904761
    avgPoints 15.714285714285714
  ]
  node [
    id 221
    label "200059"
    name "Yibing"
    surname "Wu"
    country "CHN"
    hand "R"
    dateOfBirth 19991014.0
    tourneyNum 1
    avgRank 471.9574468085106
    avgPoints 78.44680851063829
  ]
  node [
    id 222
    label "208260"
    name "Zachary"
    surname "Svajda"
    country "USA"
    hand "U"
    dateOfBirth 20021129.0
    tourneyNum 1
    avgRank 1135.3333333333333
    avgPoints 7.571428571428571
  ]
  node [
    id 223
    label "126340"
    name "Viktor"
    surname "Durasovic"
    country "NOR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 1
    avgRank 353.6595744680851
    avgPoints 80.80851063829788
  ]
  node [
    id 224
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 14
    avgRank 3.5531914893617023
    avgPoints 6221.063829787234
  ]
  node [
    id 225
    label "105870"
    name "Yannick"
    surname "Hanfmann"
    country "GER"
    hand "U"
    dateOfBirth 19911113.0
    tourneyNum 2
    avgRank 180.80851063829786
    avgPoints 288.8085106382979
  ]
  node [
    id 226
    label "105877"
    name "Emilio"
    surname "Gomez"
    country "ECU"
    hand "R"
    dateOfBirth 19911128.0
    tourneyNum 1
    avgRank 209.87234042553192
    avgPoints 257.82978723404256
  ]
  node [
    id 227
    label "105882"
    name "Stefano"
    surname "Travaglia"
    country "ITA"
    hand "U"
    dateOfBirth 19911218.0
    tourneyNum 7
    avgRank 106.14893617021276
    avgPoints 544.0851063829788
  ]
  node [
    id 228
    label "200095"
    name "Nicola"
    surname "Kuhn"
    country "ESP"
    hand "R"
    dateOfBirth 20000320.0
    tourneyNum 2
    avgRank 227.61702127659575
    avgPoints 215.7659574468085
  ]
  node [
    id 229
    label "105899"
    name "Martin"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19920114.0
    tourneyNum 1
    avgRank 375.8510638297872
    avgPoints 87.0
  ]
  node [
    id 230
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 17
    avgRank 71.95744680851064
    avgPoints 803.1489361702128
  ]
  node [
    id 231
    label "105902"
    name "James"
    surname "Duckworth"
    country "AUS"
    hand "R"
    dateOfBirth 19920121.0
    tourneyNum 3
    avgRank 149.89361702127658
    avgPoints 389.0425531914894
  ]
  node [
    id 232
    label "144817"
    name "Marc Andrea"
    surname "Huesler"
    country "SUI"
    hand "U"
    dateOfBirth 19960624.0
    tourneyNum 2
    avgRank 302.8510638297872
    avgPoints 122.17021276595744
  ]
  node [
    id 233
    label "105906"
    name "Kevin"
    surname "Krawietz"
    country "GER"
    hand "R"
    dateOfBirth 19920124.0
    tourneyNum 1
    avgRank 343.06382978723406
    avgPoints 137.7659574468085
  ]
  node [
    id 234
    label "202165"
    name "Alberto"
    surname "Lim"
    country "PHI"
    hand "R"
    dateOfBirth 19990518.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 235
    label "122298"
    name "Botic"
    surname "Van De Zandschulp"
    country "NED"
    hand "U"
    dateOfBirth 19951004.0
    tourneyNum 1
    avgRank 381.17021276595744
    avgPoints 94.57446808510639
  ]
  node [
    id 236
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 26
    avgRank 52.829787234042556
    avgPoints 976.468085106383
  ]
  node [
    id 237
    label "144841"
    name "Christian"
    surname "Sigsgaard"
    country "DEN"
    hand "U"
    dateOfBirth 19970314.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 238
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 25
    avgRank 20.02127659574468
    avgPoints 1787.872340425532
  ]
  node [
    id 239
    label "105933"
    name "Roberto"
    surname "Quiroz"
    country "ECU"
    hand "L"
    dateOfBirth 19920223.0
    tourneyNum 1
    avgRank 228.6595744680851
    avgPoints 215.36170212765958
  ]
  node [
    id 240
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 21
    avgRank 63.48936170212766
    avgPoints 887.4255319148937
  ]
  node [
    id 241
    label "103893"
    name "Paolo"
    surname "Lorenzi"
    country "ITA"
    hand "R"
    dateOfBirth 19811215.0
    tourneyNum 10
    avgRank 112.61702127659575
    avgPoints 505.3404255319149
  ]
  node [
    id 242
    label "105943"
    name "Federico"
    surname "Gaio"
    country "ITA"
    hand "R"
    dateOfBirth 19920305.0
    tourneyNum 1
    avgRank 182.3404255319149
    avgPoints 295.29787234042556
  ]
  node [
    id 243
    label "122330"
    name "Alexander"
    surname "Bublik"
    country "KAZ"
    hand "R"
    dateOfBirth 19970617.0
    tourneyNum 17
    avgRank 89.76595744680851
    avgPoints 711.0212765957447
  ]
  node [
    id 244
    label "105954"
    name "Peter"
    surname "Nagy"
    country "HUN"
    hand "U"
    dateOfBirth 19920317.0
    tourneyNum 1
    avgRank 562.2380952380952
    avgPoints 45.76190476190476
  ]
  node [
    id 245
    label "105963"
    name "Arthur"
    surname "De Greef"
    country "BEL"
    hand "R"
    dateOfBirth 19920327.0
    tourneyNum 1
    avgRank 229.38297872340425
    avgPoints 215.95744680851064
  ]
  node [
    id 246
    label "103917"
    name "Nicolas"
    surname "Mahut"
    country "FRA"
    hand "R"
    dateOfBirth 19820121.0
    tourneyNum 3
    avgRank 195.74468085106383
    avgPoints 267.3404255319149
  ]
  node [
    id 247
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 20
    avgRank 77.74468085106383
    avgPoints 771.3829787234042
  ]
  node [
    id 248
    label "105967"
    name "Henri"
    surname "Laaksonen"
    country "SUI"
    hand "R"
    dateOfBirth 19920331.0
    tourneyNum 10
    avgRank 114.95744680851064
    avgPoints 502.25531914893617
  ]
  node [
    id 249
    label "144895"
    name "Corentin"
    surname "Moutet"
    country "FRA"
    hand "L"
    dateOfBirth 19990419.0
    tourneyNum 10
    avgRank 105.48936170212765
    avgPoints 547.8723404255319
  ]
  node [
    id 250
    label "105985"
    name "Darian"
    surname "King"
    country "BAR"
    hand "R"
    dateOfBirth 19920426.0
    tourneyNum 2
    avgRank 180.0212765957447
    avgPoints 289.8936170212766
  ]
  node [
    id 251
    label "105992"
    name "Ryan"
    surname "Harrison"
    country "USA"
    hand "R"
    dateOfBirth 19920507.0
    tourneyNum 7
    avgRank 193.85106382978722
    avgPoints 352.17021276595744
  ]
  node [
    id 252
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 22
    avgRank 75.68085106382979
    avgPoints 738.3829787234042
  ]
  node [
    id 253
    label "106005"
    name "Constant"
    surname "Lestienne"
    country "FRA"
    hand "R"
    dateOfBirth 19920523.0
    tourneyNum 2
    avgRank 194.48936170212767
    avgPoints 271.29787234042556
  ]
  node [
    id 254
    label "202262"
    name "Filip Cristian"
    surname "Jianu"
    country "ROU"
    hand "U"
    dateOfBirth 20010918.0
    tourneyNum 1
    avgRank 483.85714285714283
    avgPoints 65.42857142857143
  ]
  node [
    id 255
    label "134677"
    name "Juan"
    surname "Borba"
    country "PAR"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 256
    label "200218"
    name "Mate"
    surname "Valkusz"
    country "HUN"
    hand "R"
    dateOfBirth 19980813.0
    tourneyNum 1
    avgRank 403.7659574468085
    avgPoints 77.80851063829788
  ]
  node [
    id 257
    label "200221"
    name "Alejandro"
    surname "Davidovich Fokina"
    country "ESP"
    hand "R"
    dateOfBirth 19990605.0
    tourneyNum 7
    avgRank 138.25531914893617
    avgPoints 438.36170212765956
  ]
  node [
    id 258
    label "103970"
    name "David"
    surname "Ferrer"
    country "ESP"
    hand "R"
    dateOfBirth 19820402.0
    tourneyNum 6
    avgRank 162.47727272727272
    avgPoints 348.52272727272725
  ]
  node [
    id 259
    label "106021"
    name "Dragos"
    surname "Dima"
    country "ROU"
    hand "R"
    dateOfBirth 19920616.0
    tourneyNum 1
    avgRank 491.8510638297872
    avgPoints 65.85106382978724
  ]
  node [
    id 260
    label "106031"
    name "Raymond"
    surname "Sarmiento"
    country "USA"
    hand "R"
    dateOfBirth 19920724.0
    tourneyNum 1
    avgRank 577.6785714285714
    avgPoints 32.285714285714285
  ]
  node [
    id 261
    label "106032"
    name "Facundo"
    surname "Arguello"
    country "ARG"
    hand "R"
    dateOfBirth 19920804.0
    tourneyNum 1
    avgRank 216.5531914893617
    avgPoints 237.4468085106383
  ]
  node [
    id 262
    label "106034"
    name "Yasutaka"
    surname "Uchiyama"
    country "JPN"
    hand "R"
    dateOfBirth 19920805.0
    tourneyNum 4
    avgRank 153.12765957446808
    avgPoints 393.3404255319149
  ]
  node [
    id 263
    label "144946"
    name "Rayane"
    surname "Roumane"
    country "FRA"
    hand "U"
    dateOfBirth 20000911.0
    tourneyNum 1
    avgRank 473.1621621621622
    avgPoints 51.08108108108108
  ]
  node [
    id 264
    label "103990"
    name "Tommy"
    surname "Robredo"
    country "ESP"
    hand "R"
    dateOfBirth 19820501.0
    tourneyNum 1
    avgRank 206.4255319148936
    avgPoints 246.2340425531915
  ]
  node [
    id 265
    label "200247"
    name "Alexandar"
    surname "Lazarov"
    country "BUL"
    hand "R"
    dateOfBirth 19971106.0
    tourneyNum 2
    avgRank 503.3953488372093
    avgPoints 32.51162790697674
  ]
  node [
    id 266
    label "200250"
    name "Mehluli Don Ayanda"
    surname "Sibanda"
    country "ZIM"
    hand "R"
    dateOfBirth 19991106.0
    tourneyNum 1
    avgRank 948.6190476190476
    avgPoints 12.095238095238095
  ]
  node [
    id 267
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 19.893617021276597
    avgPoints 1776.9148936170213
  ]
  node [
    id 268
    label "126523"
    name "Bernabe"
    surname "Zapata Miralles"
    country "ESP"
    hand "R"
    dateOfBirth 19970112.0
    tourneyNum 2
    avgRank 236.85106382978722
    avgPoints 201.74468085106383
  ]
  node [
    id 269
    label "106045"
    name "Denis"
    surname "Kudla"
    country "USA"
    hand "R"
    dateOfBirth 19920817.0
    tourneyNum 22
    avgRank 93.46808510638297
    avgPoints 608.3829787234042
  ]
  node [
    id 270
    label "106058"
    name "Jack"
    surname "Sock"
    country "USA"
    hand "R"
    dateOfBirth 19920924.0
    tourneyNum 4
    avgRank 162.10526315789474
    avgPoints 357.63157894736844
  ]
  node [
    id 271
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 26
    avgRank 44.87234042553192
    avgPoints 1312.7021276595744
  ]
  node [
    id 272
    label "200273"
    name "Hady"
    surname "Habib"
    country "LIB"
    hand "U"
    dateOfBirth 19980821.0
    tourneyNum 1
    avgRank 710.2380952380952
    avgPoints 26.19047619047619
  ]
  node [
    id 273
    label "106071"
    name "Bernard"
    surname "Tomic"
    country "AUS"
    hand "R"
    dateOfBirth 19921021.0
    tourneyNum 15
    avgRank 117.53191489361703
    avgPoints 526.936170212766
  ]
  node [
    id 274
    label "106072"
    name "Alexey"
    surname "Vatutin"
    country "RUS"
    hand "U"
    dateOfBirth 19921027.0
    tourneyNum 2
    avgRank 203.4255319148936
    avgPoints 249.51063829787233
  ]
  node [
    id 275
    label "144985"
    name "Ergi"
    surname "Kirkin"
    country "TUR"
    hand "R"
    dateOfBirth 19990127.0
    tourneyNum 1
    avgRank 540.4651162790698
    avgPoints 28.767441860465116
  ]
  node [
    id 276
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 26
    avgRank 26.04255319148936
    avgPoints 1475.8936170212767
  ]
  node [
    id 277
    label "106075"
    name "Jozef"
    surname "Kovalik"
    country "SVK"
    hand "R"
    dateOfBirth 19921104.0
    tourneyNum 9
    avgRank 169.6595744680851
    avgPoints 366.0
  ]
  node [
    id 278
    label "126555"
    name "Tung Lin"
    surname "Wu"
    country "TPE"
    hand "R"
    dateOfBirth 19980512.0
    tourneyNum 1
    avgRank 284.0
    avgPoints 142.89361702127658
  ]
  node [
    id 279
    label "144984"
    name "Artem"
    surname "Dubrivnyy"
    country "RUS"
    hand "U"
    dateOfBirth 19990131.0
    tourneyNum 1
    avgRank 488.8085106382979
    avgPoints 41.297872340425535
  ]
  node [
    id 280
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 8
    avgRank 129.63829787234042
    avgPoints 448.17021276595744
  ]
  node [
    id 281
    label "208476"
    name "Kyle"
    surname "Johnson"
    country "USA"
    hand "U"
    dateOfBirth 19930930.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 282
    label "120424"
    name "Yannick"
    surname "Maden"
    country "GER"
    hand "U"
    dateOfBirth 19891028.0
    tourneyNum 6
    avgRank 117.70212765957447
    avgPoints 482.27659574468083
  ]
  node [
    id 283
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 18
    avgRank 73.08510638297872
    avgPoints 807.531914893617
  ]
  node [
    id 284
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 6
    avgRank 127.12765957446808
    avgPoints 458.3829787234043
  ]
  node [
    id 285
    label "202358"
    name "Chun Hsin"
    surname "Tseng"
    country "TPE"
    hand "R"
    dateOfBirth 20010808.0
    tourneyNum 2
    avgRank 387.78723404255317
    avgPoints 67.61702127659575
  ]
  node [
    id 286
    label "106106"
    name "George"
    surname "Tsivadze"
    country "GEO"
    hand "U"
    dateOfBirth 19930102.0
    tourneyNum 1
    avgRank 897.7619047619048
    avgPoints 13.428571428571429
  ]
  node [
    id 287
    label "106109"
    name "Alex"
    surname "Bolt"
    country "AUS"
    hand "L"
    dateOfBirth 19930105.0
    tourneyNum 4
    avgRank 143.5531914893617
    avgPoints 384.0
  ]
  node [
    id 288
    label "106110"
    name "Filip"
    surname "Horansky"
    country "SVK"
    hand "R"
    dateOfBirth 19930107.0
    tourneyNum 1
    avgRank 181.93617021276594
    avgPoints 286.78723404255317
  ]
  node [
    id 289
    label "200325"
    name "Emil"
    surname "Ruusuvuori"
    country "FIN"
    hand "R"
    dateOfBirth 19990402.0
    tourneyNum 1
    avgRank 280.2340425531915
    avgPoints 191.19148936170214
  ]
  node [
    id 290
    label "106121"
    name "Taro"
    surname "Daniel"
    country "JPN"
    hand "R"
    dateOfBirth 19930127.0
    tourneyNum 19
    avgRank 100.80851063829788
    avgPoints 571.4468085106383
  ]
  node [
    id 291
    label "202385"
    name "Jenson"
    surname "Brooksby"
    country "USA"
    hand "U"
    dateOfBirth 20001026.0
    tourneyNum 1
    avgRank 399.6756756756757
    avgPoints 86.54054054054055
  ]
  node [
    id 292
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 27
    avgRank 28.404255319148938
    avgPoints 1726.8936170212767
  ]
  node [
    id 293
    label "126609"
    name "Patrik"
    surname "Niklas Salminen"
    country "FIN"
    hand "U"
    dateOfBirth 19970305.0
    tourneyNum 1
    avgRank 574.0851063829788
    avgPoints 24.46808510638298
  ]
  node [
    id 294
    label "200343"
    name "David"
    surname "Szintai"
    country "HUN"
    hand "U"
    dateOfBirth 19970609.0
    tourneyNum 1
    avgRank 1636.65
    avgPoints 1.85
  ]
  node [
    id 295
    label "106137"
    name "Tristan"
    surname "Lamasine"
    country "FRA"
    hand "R"
    dateOfBirth 19930305.0
    tourneyNum 1
    avgRank 257.1914893617021
    avgPoints 174.72340425531914
  ]
  node [
    id 296
    label "202400"
    name "Alastair"
    surname "Gray"
    country "GBR"
    hand "R"
    dateOfBirth 19980622.0
    tourneyNum 1
    avgRank 1108.7
    avgPoints 6.45
  ]
  node [
    id 297
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 20
    avgRank 84.74468085106383
    avgPoints 672.2553191489362
  ]
  node [
    id 298
    label "106150"
    name "Jeson"
    surname "Patrombon"
    country "PHI"
    hand "R"
    dateOfBirth 19930327.0
    tourneyNum 1
    avgRank 1430.7142857142858
    avgPoints 2.0
  ]
  node [
    id 299
    label "200368"
    name "Jack Mingjie"
    surname "Lin"
    country "CAN"
    hand "U"
    dateOfBirth 19990415.0
    tourneyNum 1
    avgRank 1239.4166666666667
    avgPoints 3.0
  ]
  node [
    id 300
    label "122548"
    name "Edan"
    surname "Leshem"
    country "ISR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 1
    avgRank 422.29787234042556
    avgPoints 52.851063829787236
  ]
  node [
    id 301
    label "104122"
    name "Carlos"
    surname "Berlocq"
    country "ARG"
    hand "R"
    dateOfBirth 19830203.0
    tourneyNum 3
    avgRank 251.72340425531914
    avgPoints 243.68085106382978
  ]
  node [
    id 302
    label "126652"
    name "Jay"
    surname "Clarke"
    country "GBR"
    hand "R"
    dateOfBirth 19980727.0
    tourneyNum 3
    avgRank 180.72340425531914
    avgPoints 292.8085106382979
  ]
  node [
    id 303
    label "122570"
    name "Mubarak Shannan"
    surname "Zayid"
    country "QAT"
    hand "U"
    dateOfBirth 19950414.0
    tourneyNum 1
    avgRank 1964.6666666666667
    avgPoints 1.0
  ]
  node [
    id 304
    label "106186"
    name "Jason"
    surname "Kubler"
    country "AUS"
    hand "R"
    dateOfBirth 19930519.0
    tourneyNum 1
    avgRank 200.2340425531915
    avgPoints 273.25531914893617
  ]
  node [
    id 305
    label "134868"
    name "Tallon"
    surname "Griekspoor"
    country "NED"
    hand "R"
    dateOfBirth 19960702.0
    tourneyNum 3
    avgRank 194.27659574468086
    avgPoints 266.21276595744683
  ]
  node [
    id 306
    label "208597"
    name "Chak Lam Coleman"
    surname "Wong"
    country "HKG"
    hand "U"
    dateOfBirth 20040606.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 307
    label "106198"
    name "Hugo"
    surname "Dellien"
    country "BOL"
    hand "R"
    dateOfBirth 19930616.0
    tourneyNum 17
    avgRank 88.38297872340425
    avgPoints 639.5957446808511
  ]
  node [
    id 308
    label "106203"
    name "Pedro"
    surname "Sakamoto"
    country "BRA"
    hand "U"
    dateOfBirth 19930629.0
    tourneyNum 1
    avgRank 368.51063829787233
    avgPoints 74.14893617021276
  ]
  node [
    id 309
    label "106210"
    name "Jiri"
    surname "Vesely"
    country "CZE"
    hand "L"
    dateOfBirth 19930710.0
    tourneyNum 13
    avgRank 110.82978723404256
    avgPoints 523.7446808510638
  ]
  node [
    id 310
    label "106214"
    name "Oscar"
    surname "Otte"
    country "GER"
    hand "R"
    dateOfBirth 19930716.0
    tourneyNum 3
    avgRank 154.9787234042553
    avgPoints 352.4468085106383
  ]
  node [
    id 311
    label "106216"
    name "Bjorn"
    surname "Fratangelo"
    country "USA"
    hand "R"
    dateOfBirth 19930719.0
    tourneyNum 6
    avgRank 146.63829787234042
    avgPoints 390.3829787234043
  ]
  node [
    id 312
    label "106218"
    name "Marcos"
    surname "Giron"
    country "USA"
    hand "R"
    dateOfBirth 19930724.0
    tourneyNum 4
    avgRank 155.2340425531915
    avgPoints 378.3191489361702
  ]
  node [
    id 313
    label "202475"
    name "Philip"
    surname "Henning"
    country "RSA"
    hand "R"
    dateOfBirth 20001110.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 314
    label "106220"
    name "Dimitar"
    surname "Kuzmanov"
    country "BUL"
    hand "R"
    dateOfBirth 19930728.0
    tourneyNum 2
    avgRank 349.21276595744683
    avgPoints 93.8936170212766
  ]
  node [
    id 315
    label "106223"
    name "Aleksandre"
    surname "Metreveli"
    country "GEO"
    hand "R"
    dateOfBirth 19930810.0
    tourneyNum 1
    avgRank 667.4166666666666
    avgPoints 26.208333333333332
  ]
  node [
    id 316
    label "106227"
    name "Ji Sung"
    surname "Nam"
    country "KOR"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 1
    avgRank 310.6170212765957
    avgPoints 113.63829787234043
  ]
  node [
    id 317
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 22
    avgRank 65.2127659574468
    avgPoints 856.5531914893617
  ]
  node [
    id 318
    label "200436"
    name "Zsombor"
    surname "Piros"
    country "HUN"
    hand "U"
    dateOfBirth 19991013.0
    tourneyNum 1
    avgRank 385.6595744680851
    avgPoints 67.19148936170212
  ]
  node [
    id 319
    label "106232"
    name "Roberto"
    surname "Cid"
    country "DOM"
    hand "R"
    dateOfBirth 19930830.0
    tourneyNum 1
    avgRank 279.8085106382979
    avgPoints 152.0
  ]
  node [
    id 320
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 22
    avgRank 5.1063829787234045
    avgPoints 4782.978723404255
  ]
  node [
    id 321
    label "104198"
    name "Guillermo"
    surname "Garcia Lopez"
    country "ESP"
    hand "R"
    dateOfBirth 19830604.0
    tourneyNum 10
    avgRank 135.19148936170214
    avgPoints 420.9148936170213
  ]
  node [
    id 322
    label "106249"
    name "Joao"
    surname "Domingues"
    country "POR"
    hand "R"
    dateOfBirth 19931005.0
    tourneyNum 2
    avgRank 191.25531914893617
    avgPoints 272.6170212765957
  ]
  node [
    id 323
    label "200460"
    name "John Bryan Decasa"
    surname "Otico"
    country "PHI"
    hand "U"
    dateOfBirth 19990625.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 324
    label "200484"
    name "Rudolf"
    surname "Molleker"
    country "GER"
    hand "R"
    dateOfBirth 20001026.0
    tourneyNum 5
    avgRank 166.82978723404256
    avgPoints 316.74468085106383
  ]
  node [
    id 325
    label "106283"
    name "Mitchell"
    surname "Krueger"
    country "USA"
    hand "R"
    dateOfBirth 19940112.0
    tourneyNum 1
    avgRank 182.04255319148936
    avgPoints 286.4468085106383
  ]
  node [
    id 326
    label "122669"
    name "Juan Pablo"
    surname "Varillas Patino Samudio"
    country "PER"
    hand "R"
    dateOfBirth 19951006.0
    tourneyNum 1
    avgRank 300.1914893617021
    avgPoints 148.91489361702128
  ]
  node [
    id 327
    label "106293"
    name "Kimmer"
    surname "Coppejans"
    country "BEL"
    hand "R"
    dateOfBirth 19940207.0
    tourneyNum 3
    avgRank 169.25531914893617
    avgPoints 316.531914893617
  ]
  node [
    id 328
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 27
    avgRank 7.851063829787234
    avgPoints 3751.063829787234
  ]
  node [
    id 329
    label "106296"
    name "Gregoire"
    surname "Barrere"
    country "FRA"
    hand "R"
    dateOfBirth 19940216.0
    tourneyNum 7
    avgRank 108.34042553191489
    avgPoints 529.5531914893617
  ]
  node [
    id 330
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 21
    avgRank 25.51063829787234
    avgPoints 1468.723404255319
  ]
  node [
    id 331
    label "137018"
    name "Ayed"
    surname "Zatar"
    country "PAR"
    hand "R"
    dateOfBirth 19960329.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 332
    label "200514"
    name "Jurij"
    surname "Rodionov"
    country "AUT"
    hand "L"
    dateOfBirth 19990516.0
    tourneyNum 3
    avgRank 252.06382978723406
    avgPoints 192.36170212765958
  ]
  node [
    id 333
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 25
    avgRank 59.97872340425532
    avgPoints 887.9148936170212
  ]
  node [
    id 334
    label "132932"
    name "Franz"
    surname "Luna Lavidalie"
    country "GUA"
    hand "R"
    dateOfBirth 19971005.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 335
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 27
    avgRank 36.87234042553192
    avgPoints 1217.9787234042553
  ]
  node [
    id 336
    label "200526"
    name "Juan"
    surname "Lugo"
    country "VEN"
    hand "U"
    dateOfBirth 19990921.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 337
    label "106329"
    name "Thiago"
    surname "Monteiro"
    country "BRA"
    hand "L"
    dateOfBirth 19940531.0
    tourneyNum 13
    avgRank 105.29787234042553
    avgPoints 542.1702127659574
  ]
  node [
    id 338
    label "104291"
    name "Malek"
    surname "Jaziri"
    country "TUN"
    hand "R"
    dateOfBirth 19840120.0
    tourneyNum 17
    avgRank 110.25531914893617
    avgPoints 587.8936170212766
  ]
  node [
    id 339
    label "200553"
    name "Alen"
    surname "Avidzba"
    country "RUS"
    hand "R"
    dateOfBirth 20000224.0
    tourneyNum 1
    avgRank 417.8936170212766
    avgPoints 53.97872340425532
  ]
  node [
    id 340
    label "104297"
    name "Rogerio"
    surname "Dutra Silva"
    country "BRA"
    hand "R"
    dateOfBirth 19840203.0
    tourneyNum 2
    avgRank 200.17021276595744
    avgPoints 268.02127659574467
  ]
  node [
    id 341
    label "206703"
    name "Paul"
    surname "Jubb"
    country "GBR"
    hand "R"
    dateOfBirth 19991031.0
    tourneyNum 2
    avgRank 558.4074074074074
    avgPoints 37.03703703703704
  ]
  node [
    id 342
    label "106353"
    name "Pedja"
    surname "Krstin"
    country "SRB"
    hand "R"
    dateOfBirth 19940903.0
    tourneyNum 1
    avgRank 214.29787234042553
    avgPoints 236.2127659574468
  ]
  node [
    id 343
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 25
    avgRank 64.40425531914893
    avgPoints 838.5957446808511
  ]
  node [
    id 344
    label "106368"
    name "Ramkumar"
    surname "Ramanathan"
    country "IND"
    hand "R"
    dateOfBirth 19941108.0
    tourneyNum 5
    avgRank 160.6595744680851
    avgPoints 343.17021276595744
  ]
  node [
    id 345
    label "104327"
    name "Steve"
    surname "Darcis"
    country "BEL"
    hand "R"
    dateOfBirth 19840313.0
    tourneyNum 13
    avgRank 218.53191489361703
    avgPoints 231.72340425531914
  ]
  node [
    id 346
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 25
    avgRank 38.361702127659576
    avgPoints 1283.404255319149
  ]
  node [
    id 347
    label "106388"
    name "Pak Long"
    surname "Yeung"
    country "HKG"
    hand "U"
    dateOfBirth 19950214.0
    tourneyNum 1
    avgRank 1106.3809523809523
    avgPoints 7.095238095238095
  ]
  node [
    id 348
    label "106397"
    name "Wishaya"
    surname "Trongcharoenchaikul"
    country "THA"
    hand "R"
    dateOfBirth 19950408.0
    tourneyNum 1
    avgRank 555.5106382978723
    avgPoints 22.72340425531915
  ]
  node [
    id 349
    label "106398"
    name "Pedro"
    surname "Cachin"
    country "ARG"
    hand "R"
    dateOfBirth 19950412.0
    tourneyNum 1
    avgRank 264.1276595744681
    avgPoints 177.2340425531915
  ]
  node [
    id 350
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 18
    avgRank 37.638297872340424
    avgPoints 1219.6808510638298
  ]
  node [
    id 351
    label "200611"
    name "Elliot"
    surname "Benchetrit"
    country "FRA"
    hand "U"
    dateOfBirth 19981002.0
    tourneyNum 2
    avgRank 239.14893617021278
    avgPoints 198.31914893617022
  ]
  node [
    id 352
    label "200615"
    name "Alexei"
    surname "Popyrin"
    country "AUS"
    hand "R"
    dateOfBirth 19990805.0
    tourneyNum 20
    avgRank 106.76595744680851
    avgPoints 536.468085106383
  ]
  node [
    id 353
    label "106412"
    name "Lucas"
    surname "Gomez"
    country "MEX"
    hand "U"
    dateOfBirth 19950811.0
    tourneyNum 2
    avgRank 676.1489361702128
    avgPoints 13.085106382978724
  ]
  node [
    id 354
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 22
    avgRank 69.93617021276596
    avgPoints 784.4042553191489
  ]
  node [
    id 355
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 24
    avgRank 9.936170212765957
    avgPoints 3646.1063829787236
  ]
  node [
    id 356
    label "106423"
    name "Thanasi"
    surname "Kokkinakis"
    country "AUS"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 4
    avgRank 178.4255319148936
    avgPoints 299.0425531914894
  ]
  node [
    id 357
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 27
    avgRank 47.744680851063826
    avgPoints 1096.0
  ]
  node [
    id 358
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 23
    avgRank 17.04255319148936
    avgPoints 2133.723404255319
  ]
  node [
    id 359
    label "104386"
    name "Janko"
    surname "Tipsarevic"
    country "SRB"
    hand "R"
    dateOfBirth 19840622.0
    tourneyNum 11
    avgRank 302.97777777777776
    avgPoints 136.24444444444444
  ]
  node [
    id 360
    label "110536"
    name "Kevin"
    surname "King"
    country "USA"
    hand "U"
    dateOfBirth 19910228.0
    tourneyNum 1
    avgRank 342.0425531914894
    avgPoints 95.61702127659575
  ]
  node [
    id 361
    label "126939"
    name "Khumoun"
    surname "Sultanov"
    country "UZB"
    hand "U"
    dateOfBirth 19981027.0
    tourneyNum 1
    avgRank 376.27659574468083
    avgPoints 69.74468085106383
  ]
  node [
    id 362
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 8
    avgRank 131.93617021276594
    avgPoints 468.4255319148936
  ]
  node [
    id 363
    label "104424"
    name "Go"
    surname "Soeda"
    country "JPN"
    hand "R"
    dateOfBirth 19840905.0
    tourneyNum 1
    avgRank 167.27659574468086
    avgPoints 337.8723404255319
  ]
  edge [
    source 0
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 332
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 107
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 213
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 311
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 7
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 116
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 23
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 284
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 230
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 246
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 209
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 117
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 3
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 5
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 124
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 181
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 134
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 251
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 224
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 146
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 201
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 211
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 129
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 355
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 193
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 155
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 103
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 176
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 346
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 238
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 141
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 198
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 8
    target 93
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 146
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 84
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 103
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 357
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 240
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 103
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 250
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 151
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 136
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 320
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 20
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 174
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 20
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 357
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 20
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 103
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 166
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 319
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 138
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 253
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 344
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 89
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 142
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 176
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 103
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 333
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 108
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 51
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 177
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 326
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 109
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 344
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 93
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 108
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 176
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 353
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 206
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 117
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 46
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 178
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 258
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 31
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 53
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 202
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 172
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 31
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 328
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 31
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 292
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 165
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 238
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 191
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 244
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 140
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 52
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 328
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 33
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 231
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 191
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 350
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 224
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 33
    target 124
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 267
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 33
    target 126
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 53
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 144
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 320
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 225
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 355
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 146
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 166
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 317
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 34
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 77
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 34
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 103
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 179
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 96
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 52
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 35
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 139
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 191
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 155
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 141
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 35
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 202
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 207
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 35
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 88
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 148
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 247
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 169
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 252
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 328
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 343
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 40
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 320
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 40
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 75
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 40
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 257
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 139
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 211
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 292
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 41
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 151
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 41
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 311
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 172
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 46
    target 357
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 46
    target 68
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 257
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 207
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 46
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 346
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 271
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 46
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 260
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 314
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 155
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 48
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 108
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 184
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 231
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 209
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 151
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 51
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 309
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 51
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 246
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 139
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 113
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 324
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 77
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 52
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 355
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 52
    target 330
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 52
    target 325
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 333
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 52
    target 311
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 187
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 52
    target 328
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 52
    target 320
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 52
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 88
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 139
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 52
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 269
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 52
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 179
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 52
    target 224
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 52
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 363
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 207
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 53
    target 206
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 123
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 77
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 155
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 53
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 153
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 189
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 198
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 124
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 149
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 142
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 228
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 89
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 109
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 321
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 311
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 155
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 267
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 186
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 139
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 182
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 215
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 123
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 64
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 329
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 119
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 311
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 92
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 150
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 113
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 68
    target 252
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 68
    target 328
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 68
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 84
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 68
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 96
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 68
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 142
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 80
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 69
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 139
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 72
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 72
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 72
    target 159
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 236
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 75
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 267
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 75
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 165
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 75
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 75
    target 136
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 84
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 75
    target 113
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 297
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 75
    target 335
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 75
    target 253
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 292
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 77
    target 90
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 124
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 77
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 108
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 77
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 144
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 77
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 343
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 77
    target 187
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 77
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 276
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 77
    target 198
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 77
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 91
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 78
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 78
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 80
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 334
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 83
    target 251
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 151
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 84
    target 93
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 84
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 209
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 84
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 328
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 84
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 227
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 84
    target 358
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 84
    target 179
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 84
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 87
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 154
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 252
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 84
    target 152
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 108
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 84
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 113
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 111
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 109
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 90
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 88
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 88
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 354
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 88
    target 113
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 88
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 88
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 88
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 89
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 89
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 155
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 90
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 141
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 90
    target 115
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 90
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 108
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 90
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 92
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 276
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 93
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 290
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 93
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 343
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 93
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 191
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 93
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 178
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 93
    target 101
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 342
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 189
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 149
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 361
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 190
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 144
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 96
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 283
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 96
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 191
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 96
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 96
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 277
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 96
    target 169
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 199
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 149
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 96
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 355
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 97
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 354
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 97
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 98
    target 113
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 106
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 100
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 100
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 101
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 101
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 102
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 309
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 103
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 142
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 134
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 103
    target 125
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 103
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 193
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 103
    target 117
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 103
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 207
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 103
    target 317
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 103
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 139
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 262
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 104
    target 239
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 104
    target 195
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 105
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 105
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 335
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 106
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 169
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 200
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 284
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 165
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 108
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 322
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 132
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 108
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 113
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 108
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 149
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 109
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 109
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 109
    target 132
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 109
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 109
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 109
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 159
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 302
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 141
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 113
    target 142
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 202
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 113
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 144
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 113
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 147
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 247
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 113
    target 210
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 113
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 238
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 113
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 134
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 240
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 113
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 113
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 114
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 136
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 200
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 116
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 343
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 116
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 242
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 224
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 116
    target 165
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 181
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 117
    target 164
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 271
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 117
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 292
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 117
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 196
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 142
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 123
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 118
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 251
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 118
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 311
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 141
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 119
    target 147
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 135
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 120
    target 139
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 121
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 122
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 122
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 335
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 123
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 146
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 123
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 224
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 123
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 240
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 123
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 209
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 123
    target 141
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 123
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 262
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 238
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 123
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 151
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 292
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 124
    target 305
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 230
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 124
    target 176
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 124
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 333
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 124
    target 335
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 124
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 169
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 329
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 320
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 124
    target 141
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 124
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 149
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 130
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 127
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 314
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 188
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 139
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 134
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 134
    target 139
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 134
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 187
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 134
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 141
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 178
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 134
    target 135
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 134
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 135
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 135
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 135
    target 162
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 136
    target 164
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 136
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 136
    target 284
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 136
    target 162
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 138
    target 229
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 328
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 139
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 330
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 139
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 139
    target 187
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 139
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 139
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 207
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 141
    target 328
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 141
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 181
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 141
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 201
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 141
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 337
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 141
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 292
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 141
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 247
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 141
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 363
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 141
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 142
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 142
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 142
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 271
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 144
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 267
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 144
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 218
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 187
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 144
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 179
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 144
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 362
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 144
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 207
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 144
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 145
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 224
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 146
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 191
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 146
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 165
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 147
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 151
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 179
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 148
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 148
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 149
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 149
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 149
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 149
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 149
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 149
    target 155
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 176
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 179
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 190
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 152
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 153
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 238
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 155
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 155
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 292
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 155
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 357
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 155
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 155
    target 355
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 155
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 155
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 155
    target 243
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 155
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 276
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 156
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 158
    target 259
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 158
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 163
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 161
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 163
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 164
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 206
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 165
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 176
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 165
    target 228
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 165
    target 175
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 166
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 189
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 168
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 168
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 169
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 169
    target 309
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 169
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 169
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 169
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 169
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 169
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 296
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 171
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 317
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 172
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 332
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 174
    target 185
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 174
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 187
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 267
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 175
    target 324
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 297
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 176
    target 178
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 176
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 176
    target 236
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 176
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 207
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 176
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 176
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 176
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 187
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 178
    target 349
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 236
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 178
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 351
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 329
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 178
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 178
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 355
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 179
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 240
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 179
    target 328
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 179
    target 191
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 224
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 179
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 354
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 179
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 179
    target 207
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 179
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 179
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 252
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 180
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 247
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 181
    target 330
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 181
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 320
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 181
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 181
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 198
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 182
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 182
    target 289
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 183
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 185
    target 281
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 186
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 186
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 186
    target 187
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 186
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 186
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 186
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 186
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 267
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 187
    target 198
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 187
    target 257
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 332
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 276
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 187
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 360
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 191
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 187
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 189
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 189
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 189
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 190
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 190
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 201
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 240
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 191
    target 246
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 191
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 269
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 193
    target 333
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 193
    target 240
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 193
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 193
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 280
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 194
    target 208
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 197
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 276
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 198
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 198
    target 207
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 357
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 198
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 262
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 198
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 199
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 359
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 201
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 292
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 201
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 360
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 201
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 201
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 357
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 202
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 328
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 202
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 202
    target 238
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 202
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 249
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 204
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 203
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 203
    target 300
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 247
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 204
    target 344
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 311
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 277
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 358
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 206
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 206
    target 216
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 207
    target 258
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 207
    target 349
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 207
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 207
    target 225
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 207
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 207
    target 320
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 207
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 258
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 243
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 209
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 290
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 210
    target 328
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 210
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 210
    target 307
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 210
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 256
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 211
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 213
    target 219
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 214
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 250
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 216
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 280
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 216
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 314
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 220
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 221
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 223
    target 315
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 328
    weight 5
    lowerId 2
    higherId 3
  ]
  edge [
    source 224
    target 358
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 224
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 320
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 224
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 310
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 302
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 292
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 224
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 224
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 228
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 252
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 230
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 231
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 234
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 236
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 238
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 236
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 271
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 267
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 274
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 328
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 238
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 317
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 238
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 307
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 240
    target 246
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 328
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 240
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 241
    target 251
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 241
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 241
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 241
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 267
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 243
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 243
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 245
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 246
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 246
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 247
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 247
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 352
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 247
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 279
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 248
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 248
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 248
    target 251
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 248
    target 271
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 248
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 249
    target 274
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 249
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 249
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 249
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 250
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 251
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 251
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 251
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 251
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 251
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 252
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 252
    target 271
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 252
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 252
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 252
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 252
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 252
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 254
    target 266
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 255
    target 353
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 257
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 257
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 261
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 262
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 265
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 354
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 267
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 267
    target 324
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 271
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 267
    target 320
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 267
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 346
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 267
    target 357
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 267
    target 292
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 267
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 267
    target 280
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 267
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 292
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 269
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 270
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 271
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 271
    target 357
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 271
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 271
    target 276
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 273
    target 343
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 273
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 273
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 276
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 343
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 276
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 322
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 276
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 276
    target 307
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 276
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 277
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 277
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 278
    target 347
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 280
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 280
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 280
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 280
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 280
    target 322
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 301
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 357
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 283
    target 307
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 283
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 292
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 283
    target 284
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 286
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 283
    target 315
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 284
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 284
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 289
    target 320
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 290
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 290
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 290
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 290
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 290
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 292
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 357
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 292
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 292
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 292
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 292
    target 320
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 292
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 293
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 294
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 307
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 297
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 297
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 297
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 301
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 301
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 302
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 302
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 303
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 305
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 307
    target 317
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 307
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 307
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 307
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 307
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 307
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 309
    target 329
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 309
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 309
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 310
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 310
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 311
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 312
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 317
    target 349
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 317
    target 335
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 317
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 317
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 318
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 320
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 320
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 320
    target 335
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 320
    target 328
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 321
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 322
    target 328
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 328
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 328
    target 355
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 328
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 328
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 328
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 329
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 358
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 330
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 330
    target 355
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 330
    target 356
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 331
    target 353
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 332
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 346
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 335
    target 355
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 335
    target 346
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 335
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 335
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 335
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 335
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 337
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 338
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 338
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 343
    target 350
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 343
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 343
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 345
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 345
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 346
    target 355
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 346
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 346
    target 357
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 350
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 350
    target 355
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 350
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 352
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 354
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 355
    target 357
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 355
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 357
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
]
