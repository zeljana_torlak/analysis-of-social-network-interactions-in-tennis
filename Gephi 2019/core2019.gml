graph [
  node [
    id 0
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 28
    avgRank 18.595744680851062
    avgPoints 1879.3617021276596
  ]
  node [
    id 1
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 21
    avgRank 97.44680851063829
    avgPoints 581.8936170212766
  ]
  node [
    id 2
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 22
    avgRank 74.87234042553192
    avgPoints 832.9787234042553
  ]
  node [
    id 3
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 31
    avgRank 38.08510638297872
    avgPoints 1230.0212765957447
  ]
  node [
    id 4
    label "111797"
    name "Nicolas"
    surname "Jarry"
    country "CHI"
    hand "R"
    dateOfBirth 19951011.0
    tourneyNum 23
    avgRank 68.38297872340425
    avgPoints 797.3191489361702
  ]
  node [
    id 5
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 20
    avgRank 82.91489361702128
    avgPoints 729.936170212766
  ]
  node [
    id 6
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 15
    avgRank 8.27659574468085
    avgPoints 3509.68085106383
  ]
  node [
    id 7
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 26
    avgRank 52.829787234042556
    avgPoints 976.468085106383
  ]
  node [
    id 8
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 19.893617021276597
    avgPoints 1776.9148936170213
  ]
  node [
    id 9
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 31
    avgRank 11.595744680851064
    avgPoints 2647.021276595745
  ]
  node [
    id 10
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 24
    avgRank 50.87234042553192
    avgPoints 979.8936170212766
  ]
  node [
    id 11
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 25
    avgRank 59.97872340425532
    avgPoints 887.9148936170212
  ]
  node [
    id 12
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 28
    avgRank 38.212765957446805
    avgPoints 1202.9787234042553
  ]
  node [
    id 13
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 25
    avgRank 48.680851063829785
    avgPoints 1025.9787234042553
  ]
  node [
    id 14
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 22
    avgRank 69.93617021276596
    avgPoints 784.4042553191489
  ]
  node [
    id 15
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 18
    avgRank 13.74468085106383
    avgPoints 2471.063829787234
  ]
  node [
    id 16
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 28
    avgRank 52.723404255319146
    avgPoints 954.6595744680851
  ]
  node [
    id 17
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 40.02127659574468
    avgPoints 1147.872340425532
  ]
  node [
    id 18
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 22
    avgRank 79.65957446808511
    avgPoints 856.0425531914893
  ]
  node [
    id 19
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 25
    avgRank 20.02127659574468
    avgPoints 1787.872340425532
  ]
  node [
    id 20
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 20
    avgRank 77.74468085106383
    avgPoints 771.3829787234042
  ]
  node [
    id 21
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 20
    avgRank 84.74468085106383
    avgPoints 672.2553191489362
  ]
  node [
    id 22
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 24
    avgRank 9.936170212765957
    avgPoints 3646.1063829787236
  ]
  node [
    id 23
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 18
    avgRank 37.638297872340424
    avgPoints 1219.6808510638298
  ]
  node [
    id 24
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 18
    avgRank 73.80851063829788
    avgPoints 745.3617021276596
  ]
  node [
    id 25
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 18
    avgRank 1.1914893617021276
    avgPoints 10754.04255319149
  ]
  node [
    id 26
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 22
    avgRank 65.2127659574468
    avgPoints 856.5531914893617
  ]
  node [
    id 27
    label "124187"
    name "Reilly"
    surname "Opelka"
    country "USA"
    hand "R"
    dateOfBirth 19970828.0
    tourneyNum 24
    avgRank 53.659574468085104
    avgPoints 985.4468085106383
  ]
  node [
    id 28
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 14
    avgRank 3.5531914893617023
    avgPoints 6221.063829787234
  ]
  node [
    id 29
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 17
    avgRank 52.51063829787234
    avgPoints 960.7446808510638
  ]
  node [
    id 30
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 30
    avgRank 54.787234042553195
    avgPoints 938.1702127659574
  ]
  node [
    id 31
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 30
    avgRank 41.808510638297875
    avgPoints 1121.3829787234042
  ]
  node [
    id 32
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 20
    avgRank 20.382978723404257
    avgPoints 2191.595744680851
  ]
  node [
    id 33
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 24
    avgRank 63.46808510638298
    avgPoints 973.6382978723404
  ]
  node [
    id 34
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 25
    avgRank 38.361702127659576
    avgPoints 1283.404255319149
  ]
  node [
    id 35
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 26
    avgRank 15.446808510638299
    avgPoints 2115.2127659574467
  ]
  node [
    id 36
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 25
    avgRank 12.595744680851064
    avgPoints 2402.2340425531916
  ]
  node [
    id 37
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 22
    avgRank 75.68085106382979
    avgPoints 738.3829787234042
  ]
  node [
    id 38
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 27
    avgRank 28.404255319148938
    avgPoints 1726.8936170212767
  ]
  node [
    id 39
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 25
    avgRank 67.25531914893617
    avgPoints 847.8936170212766
  ]
  node [
    id 40
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 23
    avgRank 17.04255319148936
    avgPoints 2133.723404255319
  ]
  node [
    id 41
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 21
    avgRank 63.48936170212766
    avgPoints 887.4255319148937
  ]
  node [
    id 42
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 27
    avgRank 50.234042553191486
    avgPoints 1001.2553191489362
  ]
  node [
    id 43
    label "144719"
    name "Jaume"
    surname "Munar"
    country "ESP"
    hand "R"
    dateOfBirth 19970505.0
    tourneyNum 22
    avgRank 80.95744680851064
    avgPoints 687.3404255319149
  ]
  node [
    id 44
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 27
    avgRank 7.851063829787234
    avgPoints 3751.063829787234
  ]
  node [
    id 45
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 20
    avgRank 74.63829787234043
    avgPoints 745.9574468085107
  ]
  node [
    id 46
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 22
    avgRank 60.744680851063826
    avgPoints 874.5531914893617
  ]
  node [
    id 47
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 29
    avgRank 52.787234042553195
    avgPoints 966.2553191489362
  ]
  node [
    id 48
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 25
    avgRank 64.40425531914893
    avgPoints 838.5957446808511
  ]
  node [
    id 49
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 24
    avgRank 70.57446808510639
    avgPoints 823.4893617021277
  ]
  node [
    id 50
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 28
    avgRank 34.851063829787236
    avgPoints 1254.0851063829787
  ]
  node [
    id 51
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 21
    avgRank 25.51063829787234
    avgPoints 1468.723404255319
  ]
  node [
    id 52
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 24
    avgRank 56.87234042553192
    avgPoints 935.1914893617021
  ]
  node [
    id 53
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 21
    avgRank 35.1063829787234
    avgPoints 1320.127659574468
  ]
  node [
    id 54
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 26
    avgRank 44.87234042553192
    avgPoints 1312.7021276595744
  ]
  node [
    id 55
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 22
    avgRank 5.1063829787234045
    avgPoints 4782.978723404255
  ]
  node [
    id 56
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 21
    avgRank 69.02127659574468
    avgPoints 809.8936170212766
  ]
  node [
    id 57
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 21
    avgRank 64.44680851063829
    avgPoints 847.1489361702128
  ]
  node [
    id 58
    label "111513"
    name "Laslo"
    surname "Djere"
    country "SRB"
    hand "R"
    dateOfBirth 19950602.0
    tourneyNum 23
    avgRank 42.255319148936174
    avgPoints 1151.0851063829787
  ]
  node [
    id 59
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 25
    avgRank 56.744680851063826
    avgPoints 918.1063829787234
  ]
  node [
    id 60
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 17
    avgRank 71.95744680851064
    avgPoints 803.1489361702128
  ]
  node [
    id 61
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 25
    avgRank 5.042553191489362
    avgPoints 4747.234042553191
  ]
  node [
    id 62
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 23
    avgRank 38.06382978723404
    avgPoints 1257.4893617021276
  ]
  node [
    id 63
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 22
    avgRank 16.914893617021278
    avgPoints 2027.340425531915
  ]
  node [
    id 64
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 23
    avgRank 36.40425531914894
    avgPoints 1390.595744680851
  ]
  node [
    id 65
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 18
    avgRank 1.8085106382978724
    avgPoints 8636.170212765957
  ]
  node [
    id 66
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 27
    avgRank 49.91489361702128
    avgPoints 1003.8510638297872
  ]
  node [
    id 67
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 18
    avgRank 74.27659574468085
    avgPoints 779.9787234042553
  ]
  node [
    id 68
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 20
    avgRank 28.127659574468087
    avgPoints 1520.9574468085107
  ]
  node [
    id 69
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 27
    avgRank 54.340425531914896
    avgPoints 961.5957446808511
  ]
  node [
    id 70
    label "200615"
    name "Alexei"
    surname "Popyrin"
    country "AUS"
    hand "R"
    dateOfBirth 19990805.0
    tourneyNum 20
    avgRank 106.76595744680851
    avgPoints 536.468085106383
  ]
  node [
    id 71
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 31
    avgRank 39.04255319148936
    avgPoints 1185.2553191489362
  ]
  node [
    id 72
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 26
    avgRank 26.04255319148936
    avgPoints 1475.8936170212767
  ]
  node [
    id 73
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 13
    avgRank 21.48936170212766
    avgPoints 1780.0
  ]
  node [
    id 74
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 29
    avgRank 29.638297872340427
    avgPoints 1412.5531914893618
  ]
  node [
    id 75
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 27
    avgRank 47.744680851063826
    avgPoints 1096.0
  ]
  node [
    id 76
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 32
    avgRank 24.72340425531915
    avgPoints 1574.5744680851064
  ]
  node [
    id 77
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 18
    avgRank 73.08510638297872
    avgPoints 807.531914893617
  ]
  node [
    id 78
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 27
    avgRank 36.87234042553192
    avgPoints 1217.9787234042553
  ]
  edge [
    source 0
    target 22
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 28
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 0
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 74
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 14
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 62
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 0
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 74
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 48
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 12
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 42
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 2
    target 15
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 17
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 65
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 6
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 3
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 18
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 10
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 62
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 3
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 69
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 4
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 61
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 4
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 22
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 10
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 13
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 19
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 7
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 59
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 49
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 14
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 8
    target 74
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 54
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 8
    target 55
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 8
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 65
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 8
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 68
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 35
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 9
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 38
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 9
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 65
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 11
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 78
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 55
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 18
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 76
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 11
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 25
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 11
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 67
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 18
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 44
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 13
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 76
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 51
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 13
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 27
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 15
    target 46
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 75
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 15
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 58
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 37
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 16
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 66
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 16
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 65
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 46
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 76
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 22
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 18
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 49
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 18
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 19
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 65
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 18
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 33
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 19
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 64
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 58
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 73
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 20
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 57
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 20
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 70
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 76
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 21
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 21
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 25
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 22
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 78
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 22
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 44
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 22
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 22
    target 51
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 65
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 22
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 22
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 22
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 48
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 74
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 24
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 35
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 25
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 76
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 25
    target 51
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 25
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 44
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 25
    target 55
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 25
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 72
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 27
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 27
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 27
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 27
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 27
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 44
    weight 5
    lowerId 2
    higherId 3
  ]
  edge [
    source 28
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 55
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 28
    target 65
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 28
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 68
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 28
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 76
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 74
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 30
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 77
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 62
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 58
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 44
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 31
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 53
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 76
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 31
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 31
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 38
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 31
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 38
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 33
    target 76
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 33
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 75
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 33
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 78
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 34
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 38
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 35
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 47
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 74
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 48
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 71
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 35
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 62
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 36
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 37
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 54
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 37
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 37
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 53
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 55
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 38
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 45
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 64
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 63
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 39
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 69
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 78
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 51
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 64
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 43
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 41
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 41
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 58
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 41
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 78
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 42
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 45
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 65
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 44
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 44
    target 64
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 44
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 61
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 44
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 44
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 44
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 47
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 45
    target 49
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 45
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 45
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 72
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 46
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 48
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 46
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 59
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 47
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 47
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 48
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 63
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 48
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 49
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 62
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 49
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 52
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 55
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 52
    target 64
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 52
    target 75
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 52
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 68
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 53
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 74
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 54
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 54
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 72
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 54
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 69
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 55
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 73
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 55
    target 63
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 55
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 78
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 62
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 55
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 64
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 58
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 74
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 58
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 71
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 59
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 59
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 69
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 61
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 64
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 61
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 69
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 62
    target 76
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 62
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 74
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 62
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 76
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 63
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 64
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 76
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 64
    target 73
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 65
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 74
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 65
    target 68
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 65
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 76
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 65
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 68
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 68
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 69
    target 75
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 70
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 74
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 71
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 71
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 77
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 75
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
]
