graph [
  node [
    id 0
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 28
    avgRank 18.595744680851062
    avgPoints 1879.3617021276596
  ]
  node [
    id 1
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 27
    avgRank 50.234042553191486
    avgPoints 1001.2553191489362
  ]
  node [
    id 2
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 21
    avgRank 97.44680851063829
    avgPoints 581.8936170212766
  ]
  node [
    id 3
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 22
    avgRank 74.87234042553192
    avgPoints 832.9787234042553
  ]
  node [
    id 4
    label "106214"
    name "Oscar"
    surname "Otte"
    country "GER"
    hand "R"
    dateOfBirth 19930716.0
    tourneyNum 3
    avgRank 154.9787234042553
    avgPoints 352.4468085106383
  ]
  node [
    id 5
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 25
    avgRank 5.042553191489362
    avgPoints 4747.234042553191
  ]
  node [
    id 6
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 15
    avgRank 8.27659574468085
    avgPoints 3509.68085106383
  ]
  node [
    id 7
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 24
    avgRank 9.936170212765957
    avgPoints 3646.1063829787236
  ]
  node [
    id 8
    label "126652"
    name "Jay"
    surname "Clarke"
    country "GBR"
    hand "R"
    dateOfBirth 19980727.0
    tourneyNum 3
    avgRank 180.72340425531914
    avgPoints 292.8085106382979
  ]
  node [
    id 9
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 22
    avgRank 16.914893617021278
    avgPoints 2027.340425531915
  ]
  node [
    id 10
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 27
    avgRank 7.851063829787234
    avgPoints 3751.063829787234
  ]
  node [
    id 11
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 26
    avgRank 52.829787234042556
    avgPoints 976.468085106383
  ]
  node [
    id 12
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 18
    avgRank 1.8085106382978724
    avgPoints 8636.170212765957
  ]
  node [
    id 13
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 18
    avgRank 73.80851063829788
    avgPoints 745.3617021276596
  ]
  node [
    id 14
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 24
    avgRank 50.87234042553192
    avgPoints 979.8936170212766
  ]
  node [
    id 15
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 20
    avgRank 28.127659574468087
    avgPoints 1520.9574468085107
  ]
  node [
    id 16
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 18
    avgRank 1.1914893617021276
    avgPoints 10754.04255319149
  ]
  node [
    id 17
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 14
    avgRank 97.65957446808511
    avgPoints 583.7234042553191
  ]
  node [
    id 18
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 22
    avgRank 65.2127659574468
    avgPoints 856.5531914893617
  ]
  node [
    id 19
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 31
    avgRank 39.04255319148936
    avgPoints 1185.2553191489362
  ]
  node [
    id 20
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 14
    avgRank 3.5531914893617023
    avgPoints 6221.063829787234
  ]
  node [
    id 21
    label "104797"
    name "Denis"
    surname "Istomin"
    country "UZB"
    hand "R"
    dateOfBirth 19860907.0
    tourneyNum 12
    avgRank 134.06382978723406
    avgPoints 445.78723404255317
  ]
  node [
    id 22
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 25
    avgRank 59.97872340425532
    avgPoints 887.9148936170212
  ]
  node [
    id 23
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 30
    avgRank 54.787234042553195
    avgPoints 938.1702127659574
  ]
  node [
    id 24
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 29
    avgRank 52.787234042553195
    avgPoints 966.2553191489362
  ]
  node [
    id 25
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 26
    avgRank 26.04255319148936
    avgPoints 1475.8936170212767
  ]
  node [
    id 26
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 24
    avgRank 63.46808510638298
    avgPoints 973.6382978723404
  ]
  node [
    id 27
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 25
    avgRank 38.361702127659576
    avgPoints 1283.404255319149
  ]
  node [
    id 28
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 24
    avgRank 70.57446808510639
    avgPoints 823.4893617021277
  ]
  node [
    id 29
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 5
    avgRank 26.361702127659573
    avgPoints 2800.531914893617
  ]
  node [
    id 30
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 25
    avgRank 48.680851063829785
    avgPoints 1025.9787234042553
  ]
  node [
    id 31
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 26
    avgRank 15.446808510638299
    avgPoints 2115.2127659574467
  ]
  node [
    id 32
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 21
    avgRank 25.51063829787234
    avgPoints 1468.723404255319
  ]
  node [
    id 33
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 22
    avgRank 75.68085106382979
    avgPoints 738.3829787234042
  ]
  node [
    id 34
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 27
    avgRank 28.404255319148938
    avgPoints 1726.8936170212767
  ]
  node [
    id 35
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 18
    avgRank 13.74468085106383
    avgPoints 2471.063829787234
  ]
  node [
    id 36
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 18
    avgRank 44.808510638297875
    avgPoints 1111.276595744681
  ]
  node [
    id 37
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 25
    avgRank 67.25531914893617
    avgPoints 847.8936170212766
  ]
  node [
    id 38
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 21
    avgRank 35.1063829787234
    avgPoints 1320.127659574468
  ]
  node [
    id 39
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 23
    avgRank 17.04255319148936
    avgPoints 2133.723404255319
  ]
  node [
    id 40
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 22
    avgRank 5.1063829787234045
    avgPoints 4782.978723404255
  ]
  node [
    id 41
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 22
    avgRank 79.65957446808511
    avgPoints 856.0425531914893
  ]
  node [
    id 42
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 32
    avgRank 24.72340425531915
    avgPoints 1574.5744680851064
  ]
  node [
    id 43
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 18
    avgRank 73.08510638297872
    avgPoints 807.531914893617
  ]
  node [
    id 44
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 21
    avgRank 63.48936170212766
    avgPoints 887.4255319148937
  ]
  node [
    id 45
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 27
    avgRank 36.87234042553192
    avgPoints 1217.9787234042553
  ]
  node [
    id 46
    label "111576"
    name "Sumit"
    surname "Nagal"
    country "IND"
    hand "R"
    dateOfBirth 19970816.0
    tourneyNum 2
    avgRank 234.82978723404256
    avgPoints 234.10638297872342
  ]
  edge [
    source 0
    target 7
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 10
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 5
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 1
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 36
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 45
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 3
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 1
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 44
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 12
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 10
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 5
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 7
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 7
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 14
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 30
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 16
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 7
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 9
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 45
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 10
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 7
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 12
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 7
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 10
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 37
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 9
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 20
    weight 5
    lowerId 2
    higherId 3
  ]
  edge [
    source 10
    target 12
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 10
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 30
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 10
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 16
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 10
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 16
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 20
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 12
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 15
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 22
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 44
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 38
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 31
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 16
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 42
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 16
    target 32
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 16
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 22
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 16
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 40
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 16
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 20
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 36
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 31
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 19
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 40
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 20
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 21
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 24
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 31
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 34
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 26
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 27
    target 45
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 30
    target 32
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 30
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 34
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 31
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 36
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 32
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 40
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 34
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 45
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 37
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 44
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 45
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
]
