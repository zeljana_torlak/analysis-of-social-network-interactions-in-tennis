graph [
  node [
    id 0
    label "200711"
    name "Nicolas"
    surname "Mejia"
    country "COL"
    hand "R"
    dateOfBirth 20000211.0
    tourneyNum 1
    avgRank 1278.6470588235295
    avgPoints 7.529411764705882
  ]
  node [
    id 1
    label "110602"
    name "Dennis"
    surname "Novak"
    country "AUT"
    hand "R"
    dateOfBirth 19930828.0
    tourneyNum 8
    avgRank 169.93617021276594
    avgPoints 346.8085106382979
  ]
  node [
    id 2
    label "104460"
    name "Dustin"
    surname "Brown"
    country "GER"
    hand "R"
    dateOfBirth 19841208.0
    tourneyNum 4
    avgRank 196.04255319148936
    avgPoints 305.02127659574467
  ]
  node [
    id 3
    label "104467"
    name "Lamine"
    surname "Ouahab"
    country "ALG"
    hand "R"
    dateOfBirth 19841222.0
    tourneyNum 3
    avgRank 521.6595744680851
    avgPoints 64.59574468085107
  ]
  node [
    id 4
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 26
    avgRank 49.723404255319146
    avgPoints 1035.8510638297873
  ]
  node [
    id 5
    label "104494"
    name "Adrian"
    surname "Ungur"
    country "ROU"
    hand "R"
    dateOfBirth 19850125.0
    tourneyNum 2
    avgRank 902.8913043478261
    avgPoints 39.0
  ]
  node [
    id 6
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 17
    avgRank 77.72340425531915
    avgPoints 1223.8297872340424
  ]
  node [
    id 7
    label "104534"
    name "Dudi"
    surname "Sela"
    country "ISR"
    hand "R"
    dateOfBirth 19850404.0
    tourneyNum 10
    avgRank 158.27659574468086
    avgPoints 422.02127659574467
  ]
  node [
    id 8
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 7
    avgRank 92.93617021276596
    avgPoints 992.4468085106383
  ]
  node [
    id 9
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 24
    avgRank 11.319148936170214
    avgPoints 3030.744680851064
  ]
  node [
    id 10
    label "104547"
    name "Horacio"
    surname "Zeballos"
    country "ARG"
    hand "L"
    dateOfBirth 19850427.0
    tourneyNum 15
    avgRank 124.76595744680851
    avgPoints 511.9148936170213
  ]
  node [
    id 11
    label "104563"
    name "Luca"
    surname "Vanni"
    country "ITA"
    hand "R"
    dateOfBirth 19850604.0
    tourneyNum 1
    avgRank 212.7659574468085
    avgPoints 277.59574468085106
  ]
  node [
    id 12
    label "104571"
    name "Marcos"
    surname "Baghdatis"
    country "CYP"
    hand "R"
    dateOfBirth 19850617.0
    tourneyNum 17
    avgRank 102.61702127659575
    avgPoints 575.063829787234
  ]
  node [
    id 13
    label "104586"
    name "Lukas"
    surname "Rosol"
    country "CZE"
    hand "R"
    dateOfBirth 19850724.0
    tourneyNum 3
    avgRank 211.06382978723406
    avgPoints 274.17021276595744
  ]
  node [
    id 14
    label "104593"
    name "Daniel"
    surname "Gimeno Traver"
    country "ESP"
    hand "R"
    dateOfBirth 19850807.0
    tourneyNum 1
    avgRank 222.53191489361703
    avgPoints 264.8723404255319
  ]
  node [
    id 15
    label "104597"
    name "Nicolas"
    surname "Almagro"
    country "ESP"
    hand "R"
    dateOfBirth 19850821.0
    tourneyNum 1
    avgRank 416.09375
    avgPoints 173.25
  ]
  node [
    id 16
    label "104607"
    name "Tomas"
    surname "Berdych"
    country "CZE"
    hand "R"
    dateOfBirth 19850917.0
    tourneyNum 12
    avgRank 44.91489361702128
    avgPoints 1391.7021276595744
  ]
  node [
    id 17
    label "127143"
    name "Jirat"
    surname "Navasirisomboon"
    country "THA"
    hand "R"
    dateOfBirth 19961117.0
    tourneyNum 2
    avgRank 1276.4565217391305
    avgPoints 5.282608695652174
  ]
  node [
    id 18
    label "104620"
    name "Simone"
    surname "Bolelli"
    country "ITA"
    hand "R"
    dateOfBirth 19851008.0
    tourneyNum 4
    avgRank 151.5531914893617
    avgPoints 373.0851063829787
  ]
  node [
    id 19
    label "133297"
    name "Yosuke"
    surname "Watanuki"
    country "JPN"
    hand "U"
    dateOfBirth 19980412.0
    tourneyNum 3
    avgRank 286.5531914893617
    avgPoints 186.12765957446808
  ]
  node [
    id 20
    label "104629"
    name "Adrian"
    surname "Menendez Maceiras"
    country "ESP"
    hand "R"
    dateOfBirth 19851028.0
    tourneyNum 4
    avgRank 130.72340425531914
    avgPoints 440.1914893617021
  ]
  node [
    id 21
    label "108739"
    name "Wilfredo"
    surname "Gonzalez"
    country "GUA"
    hand "R"
    dateOfBirth 19930106.0
    tourneyNum 1
    avgRank 845.5652173913044
    avgPoints 19.804347826086957
  ]
  node [
    id 22
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 17
    avgRank 62.212765957446805
    avgPoints 922.8723404255319
  ]
  node [
    id 23
    label "104656"
    name "Haydn"
    surname "Lewis"
    country "BAR"
    hand "L"
    dateOfBirth 19860102.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 24
    label "104660"
    name "Sergiy"
    surname "Stakhovsky"
    country "UKR"
    hand "R"
    dateOfBirth 19860106.0
    tourneyNum 6
    avgRank 129.4255319148936
    avgPoints 442.7659574468085
  ]
  node [
    id 25
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 8
    avgRank 408.0851063829787
    avgPoints 382.93617021276594
  ]
  node [
    id 26
    label "108763"
    name "Alberto Emmanuel"
    surname "Alvarado Larin"
    country "ESA"
    hand "U"
    dateOfBirth 19941201.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 27
    label "104672"
    name "Vincent"
    surname "Millot"
    country "FRA"
    hand "L"
    dateOfBirth 19860130.0
    tourneyNum 1
    avgRank 314.27659574468083
    avgPoints 170.27659574468086
  ]
  node [
    id 28
    label "104678"
    name "Viktor"
    surname "Troicki"
    country "SRB"
    hand "R"
    dateOfBirth 19860210.0
    tourneyNum 13
    avgRank 118.95744680851064
    avgPoints 558.1063829787234
  ]
  node [
    id 29
    label "104719"
    name "Marcel"
    surname "Granollers"
    country "ESP"
    hand "R"
    dateOfBirth 19860412.0
    tourneyNum 5
    avgRank 116.02127659574468
    avgPoints 500.8723404255319
  ]
  node [
    id 30
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 20
    avgRank 7.702127659574468
    avgPoints 3760.0
  ]
  node [
    id 31
    label "207139"
    name "Palaphoom"
    surname "Kovapitukted"
    country "THA"
    hand "U"
    dateOfBirth 19991205.0
    tourneyNum 2
    avgRank 1418.8695652173913
    avgPoints 2.847826086956522
  ]
  node [
    id 32
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 23
    avgRank 4.042553191489362
    avgPoints 5277.234042553191
  ]
  node [
    id 33
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 10
    avgRank 1.3191489361702127
    avgPoints 8804.36170212766
  ]
  node [
    id 34
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 26
    avgRank 29.21276595744681
    avgPoints 1468.723404255319
  ]
  node [
    id 35
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 29
    avgRank 33.97872340425532
    avgPoints 1327.2340425531916
  ]
  node [
    id 36
    label "127300"
    name "Djurabek"
    surname "Karimov"
    country "UZB"
    hand "U"
    dateOfBirth 19980604.0
    tourneyNum 1
    avgRank 521.5434782608696
    avgPoints 64.84782608695652
  ]
  node [
    id 37
    label "207176"
    name "Muhammad"
    surname "Althaf Dhaifullah"
    country "INA"
    hand "R"
    dateOfBirth 20001119.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 38
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 21
    avgRank 37.57446808510638
    avgPoints 1218.936170212766
  ]
  node [
    id 39
    label "104797"
    name "Denis"
    surname "Istomin"
    country "UZB"
    hand "R"
    dateOfBirth 19860907.0
    tourneyNum 20
    avgRank 84.7872340425532
    avgPoints 677.8085106382979
  ]
  node [
    id 40
    label "104810"
    name "Zhe"
    surname "Li"
    country "CHN"
    hand "R"
    dateOfBirth 19860920.0
    tourneyNum 1
    avgRank 287.8723404255319
    avgPoints 179.61702127659575
  ]
  node [
    id 41
    label "127339"
    name "Borna"
    surname "Gojo"
    country "CRO"
    hand "U"
    dateOfBirth 19980227.0
    tourneyNum 1
    avgRank 557.5106382978723
    avgPoints 57.0
  ]
  node [
    id 42
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 23
    avgRank 61.361702127659576
    avgPoints 881.8085106382979
  ]
  node [
    id 43
    label "104882"
    name "Blaz"
    surname "Kavcic"
    country "SLO"
    hand "R"
    dateOfBirth 19870305.0
    tourneyNum 8
    avgRank 183.0212765957447
    avgPoints 336.59574468085106
  ]
  node [
    id 44
    label "104890"
    name "Andreas"
    surname "Haider Maurer"
    country "AUT"
    hand "R"
    dateOfBirth 19870322.0
    tourneyNum 9
    avgRank 503.1276595744681
    avgPoints 72.53191489361703
  ]
  node [
    id 45
    label "104897"
    name "Matthias"
    surname "Bachinger"
    country "GER"
    hand "R"
    dateOfBirth 19870402.0
    tourneyNum 4
    avgRank 154.89361702127658
    avgPoints 366.3191489361702
  ]
  node [
    id 46
    label "104898"
    name "Robin"
    surname "Haase"
    country "NED"
    hand "R"
    dateOfBirth 19870406.0
    tourneyNum 30
    avgRank 44.97872340425532
    avgPoints 1076.595744680851
  ]
  node [
    id 47
    label "104907"
    name "Jose Rubin"
    surname "Statham"
    country "NZL"
    hand "R"
    dateOfBirth 19870425.0
    tourneyNum 1
    avgRank 427.48936170212767
    avgPoints 100.87234042553192
  ]
  node [
    id 48
    label "104918"
    name "Andy"
    surname "Murray"
    country "GBR"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 6
    avgRank 207.2340425531915
    avgPoints 762.2340425531914
  ]
  node [
    id 49
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 25
    avgRank 48.59574468085106
    avgPoints 1016.5106382978723
  ]
  node [
    id 50
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 16
    avgRank 9.404255319148936
    avgPoints 4464.68085106383
  ]
  node [
    id 51
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 27
    avgRank 16.72340425531915
    avgPoints 2073.191489361702
  ]
  node [
    id 52
    label "104932"
    name "Kenny"
    surname "De Schepper"
    country "FRA"
    hand "L"
    dateOfBirth 19870529.0
    tourneyNum 2
    avgRank 187.46808510638297
    avgPoints 300.6170212765957
  ]
  node [
    id 53
    label "104947"
    name "Marsel"
    surname "Ilhan"
    country "TUR"
    hand "R"
    dateOfBirth 19870611.0
    tourneyNum 1
    avgRank 391.3829787234043
    avgPoints 116.91489361702128
  ]
  node [
    id 54
    label "109054"
    name "Daniel"
    surname "Masur"
    country "GER"
    hand "R"
    dateOfBirth 19940611.0
    tourneyNum 2
    avgRank 330.0851063829787
    avgPoints 145.48936170212767
  ]
  node [
    id 55
    label "104970"
    name "Matteo"
    surname "Viola"
    country "ITA"
    hand "R"
    dateOfBirth 19870707.0
    tourneyNum 1
    avgRank 277.27659574468083
    avgPoints 184.85106382978722
  ]
  node [
    id 56
    label "104978"
    name "Daniel"
    surname "Brands"
    country "GER"
    hand "R"
    dateOfBirth 19870717.0
    tourneyNum 1
    avgRank 278.4255319148936
    avgPoints 208.40425531914894
  ]
  node [
    id 57
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 30
    avgRank 57.08510638297872
    avgPoints 918.1063829787234
  ]
  node [
    id 58
    label "207400"
    name "Mark"
    surname "Chigaazira"
    country "ZIM"
    hand "R"
    dateOfBirth 19980429.0
    tourneyNum 1
    avgRank 1324.1739130434783
    avgPoints 2.9565217391304346
  ]
  node [
    id 59
    label "111153"
    name "Christopher"
    surname "Eubanks"
    country "USA"
    hand "R"
    dateOfBirth 19960505.0
    tourneyNum 1
    avgRank 228.04255319148936
    avgPoints 250.72340425531914
  ]
  node [
    id 60
    label "105015"
    name "Franco"
    surname "Skugor"
    country "CRO"
    hand "R"
    dateOfBirth 19870920.0
    tourneyNum 4
    avgRank 365.0425531914894
    avgPoints 126.46808510638297
  ]
  node [
    id 61
    label "207415"
    name "Daniel"
    surname "Michalski"
    country "POL"
    hand "U"
    dateOfBirth 20000111.0
    tourneyNum 1
    avgRank 1178.6170212765958
    avgPoints 6.276595744680851
  ]
  node [
    id 62
    label "207421"
    name "Alex"
    surname "Fairbanks"
    country "SRI"
    hand "U"
    dateOfBirth 19960212.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 63
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 25
    avgRank 30.595744680851062
    avgPoints 1666.3829787234042
  ]
  node [
    id 64
    label "111167"
    name "Ugo"
    surname "Nastasi"
    country "LUX"
    hand "R"
    dateOfBirth 19930416.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 65
    label "121411"
    name "Moez"
    surname "Chargui"
    country "TUN"
    hand "R"
    dateOfBirth 19930110.0
    tourneyNum 1
    avgRank 429.27659574468083
    avgPoints 92.42553191489361
  ]
  node [
    id 66
    label "105030"
    name "Michael"
    surname "Venus"
    country "NZL"
    hand "R"
    dateOfBirth 19871016.0
    tourneyNum 2
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 67
    label "105041"
    name "Lukas"
    surname "Lacko"
    country "SVK"
    hand "R"
    dateOfBirth 19871103.0
    tourneyNum 16
    avgRank 93.36170212765957
    avgPoints 623.1276595744681
  ]
  node [
    id 68
    label "127570"
    name "Rodrigo"
    surname "Arus"
    country "URU"
    hand "U"
    dateOfBirth 19950220.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 69
    label "111190"
    name "Zhizhen"
    surname "Zhang"
    country "CHN"
    hand "U"
    dateOfBirth 19961016.0
    tourneyNum 1
    avgRank 391.25531914893617
    avgPoints 110.38297872340425
  ]
  node [
    id 70
    label "111192"
    name "Lucas"
    surname "Miedler"
    country "AUT"
    hand "R"
    dateOfBirth 19960621.0
    tourneyNum 1
    avgRank 281.6595744680851
    avgPoints 193.6595744680851
  ]
  node [
    id 71
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 22
    avgRank 60.61702127659574
    avgPoints 881.2765957446809
  ]
  node [
    id 72
    label "105053"
    name "Santiago"
    surname "Giraldo"
    country "COL"
    hand "R"
    dateOfBirth 19871127.0
    tourneyNum 3
    avgRank 296.78723404255317
    avgPoints 168.82978723404256
  ]
  node [
    id 73
    label "111197"
    name "Jonathan"
    surname "Mridha"
    country "SWE"
    hand "R"
    dateOfBirth 19950408.0
    tourneyNum 1
    avgRank 734.5434782608696
    avgPoints 36.630434782608695
  ]
  node [
    id 74
    label "111200"
    name "Elias"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 9
    avgRank 125.61702127659575
    avgPoints 456.8723404255319
  ]
  node [
    id 75
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 18
    avgRank 25.97872340425532
    avgPoints 1604.9148936170213
  ]
  node [
    id 76
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 23
    avgRank 77.63829787234043
    avgPoints 733.7446808510638
  ]
  node [
    id 77
    label "105063"
    name "Andrea"
    surname "Arnaboldi"
    country "ITA"
    hand "L"
    dateOfBirth 19871227.0
    tourneyNum 1
    avgRank 210.4255319148936
    avgPoints 265.0851063829787
  ]
  node [
    id 78
    label "105064"
    name "Thomaz"
    surname "Bellucci"
    country "BRA"
    hand "L"
    dateOfBirth 19871230.0
    tourneyNum 5
    avgRank 224.74468085106383
    avgPoints 273.48936170212767
  ]
  node [
    id 79
    label "105065"
    name "Tim"
    surname "Smyczek"
    country "USA"
    hand "R"
    dateOfBirth 19871230.0
    tourneyNum 12
    avgRank 126.72340425531915
    avgPoints 452.1276595744681
  ]
  node [
    id 80
    label "117356"
    name "Cem"
    surname "Ilkel"
    country "TUR"
    hand "R"
    dateOfBirth 19950821.0
    tourneyNum 3
    avgRank 247.36170212765958
    avgPoints 218.7872340425532
  ]
  node [
    id 81
    label "105074"
    name "Ruben"
    surname "Bemelmans"
    country "BEL"
    hand "L"
    dateOfBirth 19880114.0
    tourneyNum 16
    avgRank 115.76595744680851
    avgPoints 496.1276595744681
  ]
  node [
    id 82
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 30
    avgRank 43.02127659574468
    avgPoints 1220.7446808510638
  ]
  node [
    id 83
    label "207479"
    name "Soufiane"
    surname "El Mesbahi"
    country "MAR"
    hand "R"
    dateOfBirth 20010222.0
    tourneyNum 1
    avgRank 1672.046511627907
    avgPoints 1.5813953488372092
  ]
  node [
    id 84
    label "105085"
    name "Artem"
    surname "Smirnov"
    country "UKR"
    hand "U"
    dateOfBirth 19880202.0
    tourneyNum 1
    avgRank 541.1914893617021
    avgPoints 58.12765957446808
  ]
  node [
    id 85
    label "105111"
    name "Ivan"
    surname "Endara"
    country "ECU"
    hand "U"
    dateOfBirth 19880304.0
    tourneyNum 1
    avgRank 950.4347826086956
    avgPoints 18.847826086956523
  ]
  node [
    id 86
    label "105132"
    name "Jurgen"
    surname "Zopp"
    country "EST"
    hand "R"
    dateOfBirth 19880329.0
    tourneyNum 7
    avgRank 134.12765957446808
    avgPoints 429.531914893617
  ]
  node [
    id 87
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 22
    avgRank 20.19148936170213
    avgPoints 1868.6170212765958
  ]
  node [
    id 88
    label "105147"
    name "Tatsuma"
    surname "Ito"
    country "JPN"
    hand "R"
    dateOfBirth 19880518.0
    tourneyNum 1
    avgRank 164.29787234042553
    avgPoints 342.06382978723406
  ]
  node [
    id 89
    label "105155"
    name "Pedro"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19880527.0
    tourneyNum 2
    avgRank 125.57446808510639
    avgPoints 460.8723404255319
  ]
  node [
    id 90
    label "105156"
    name "Federico"
    surname "Zeballos"
    country "BOL"
    hand "U"
    dateOfBirth 19880530.0
    tourneyNum 1
    avgRank 543.8913043478261
    avgPoints 59.58695652173913
  ]
  node [
    id 91
    label "105163"
    name "Gleb"
    surname "Sakharov"
    country "FRA"
    hand "R"
    dateOfBirth 19880610.0
    tourneyNum 1
    avgRank 215.38297872340425
    avgPoints 265.9574468085106
  ]
  node [
    id 92
    label "105166"
    name "Peter"
    surname "Polansky"
    country "CAN"
    hand "R"
    dateOfBirth 19880615.0
    tourneyNum 11
    avgRank 125.29787234042553
    avgPoints 452.63829787234044
  ]
  node [
    id 93
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 30
    avgRank 31.361702127659573
    avgPoints 1416.8085106382978
  ]
  node [
    id 94
    label "105208"
    name "Ernests"
    surname "Gulbis"
    country "LAT"
    hand "R"
    dateOfBirth 19880830.0
    tourneyNum 6
    avgRank 148.7872340425532
    avgPoints 413.468085106383
  ]
  node [
    id 95
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 24
    avgRank 83.53191489361703
    avgPoints 815.6595744680851
  ]
  node [
    id 96
    label "105217"
    name "Thiemo"
    surname "De Bakker"
    country "NED"
    hand "R"
    dateOfBirth 19880919.0
    tourneyNum 3
    avgRank 280.531914893617
    avgPoints 191.7872340425532
  ]
  node [
    id 97
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 15
    avgRank 5.595744680851064
    avgPoints 4739.04255319149
  ]
  node [
    id 98
    label "105226"
    name "Attila"
    surname "Balazs"
    country "HUN"
    hand "R"
    dateOfBirth 19880927.0
    tourneyNum 2
    avgRank 192.51063829787233
    avgPoints 288.3829787234043
  ]
  node [
    id 99
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 22
    avgRank 5.446808510638298
    avgPoints 4522.446808510638
  ]
  node [
    id 100
    label "105238"
    name "Alexandr"
    surname "Dolgopolov"
    country "UKR"
    hand "R"
    dateOfBirth 19881107.0
    tourneyNum 5
    avgRank 126.48936170212765
    avgPoints 706.6382978723404
  ]
  node [
    id 101
    label "105273"
    name "Mauricio"
    surname "Echazu"
    country "PER"
    hand "R"
    dateOfBirth 19890121.0
    tourneyNum 1
    avgRank 690.7608695652174
    avgPoints 34.34782608695652
  ]
  node [
    id 102
    label "105292"
    name "Alejandro"
    surname "Gonzalez"
    country "COL"
    hand "R"
    dateOfBirth 19890207.0
    tourneyNum 2
    avgRank 334.02127659574467
    avgPoints 140.10638297872342
  ]
  node [
    id 103
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 11
    avgRank 93.97872340425532
    avgPoints 609.5531914893617
  ]
  node [
    id 104
    label "105299"
    name "Dzmitry"
    surname "Zhyrmont"
    country "BLR"
    hand "U"
    dateOfBirth 19890301.0
    tourneyNum 1
    avgRank 484.5
    avgPoints 85.06521739130434
  ]
  node [
    id 105
    label "133975"
    name "Benjamin"
    surname "Hassan"
    country "GER"
    hand "U"
    dateOfBirth 19950204.0
    tourneyNum 3
    avgRank 427.48936170212767
    avgPoints 89.80851063829788
  ]
  node [
    id 106
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 28
    avgRank 54.829787234042556
    avgPoints 925.5106382978723
  ]
  node [
    id 107
    label "111456"
    name "Mackenzie"
    surname "Mcdonald"
    country "USA"
    hand "R"
    dateOfBirth 19950416.0
    tourneyNum 13
    avgRank 106.87234042553192
    avgPoints 566.6808510638298
  ]
  node [
    id 108
    label "111459"
    name "Arjun"
    surname "Kadhe"
    country "IND"
    hand "R"
    dateOfBirth 19940107.0
    tourneyNum 1
    avgRank 405.48936170212767
    avgPoints 103.72340425531915
  ]
  node [
    id 109
    label "111460"
    name "Quentin"
    surname "Halys"
    country "FRA"
    hand "R"
    dateOfBirth 19961026.0
    tourneyNum 4
    avgRank 132.80851063829786
    avgPoints 431.2340425531915
  ]
  node [
    id 110
    label "125802"
    name "Ilya"
    surname "Ivashka"
    country "BLR"
    hand "R"
    dateOfBirth 19940224.0
    tourneyNum 13
    avgRank 122.51063829787235
    avgPoints 493.2340425531915
  ]
  node [
    id 111
    label "123755"
    name "Daniel Elahi"
    surname "Galan Riveros"
    country "COL"
    hand "U"
    dateOfBirth 19960618.0
    tourneyNum 4
    avgRank 234.04255319148936
    avgPoints 236.7872340425532
  ]
  node [
    id 112
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 29
    avgRank 50.87234042553192
    avgPoints 990.0
  ]
  node [
    id 113
    label "123768"
    name "Collin"
    surname "Altamirano"
    country "USA"
    hand "R"
    dateOfBirth 19951207.0
    tourneyNum 1
    avgRank 458.74468085106383
    avgPoints 100.0
  ]
  node [
    id 114
    label "207738"
    name "Brandon"
    surname "Perez"
    country "VEN"
    hand "U"
    dateOfBirth 20000114.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 115
    label "105341"
    name "Thomas"
    surname "Fabbiano"
    country "ITA"
    hand "R"
    dateOfBirth 19890526.0
    tourneyNum 12
    avgRank 102.91489361702128
    avgPoints 559.3617021276596
  ]
  node [
    id 116
    label "105349"
    name "Harri"
    surname "Heliovaara"
    country "FIN"
    hand "R"
    dateOfBirth 19890604.0
    tourneyNum 3
    avgRank 416.1276595744681
    avgPoints 103.97872340425532
  ]
  node [
    id 117
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 19
    avgRank 63.40425531914894
    avgPoints 904.468085106383
  ]
  node [
    id 118
    label "105359"
    name "Jason"
    surname "Jung"
    country "TPE"
    hand "R"
    dateOfBirth 19890615.0
    tourneyNum 3
    avgRank 145.80851063829786
    avgPoints 402.6595744680851
  ]
  node [
    id 119
    label "123795"
    name "Altug"
    surname "Celikbilek"
    country "TUR"
    hand "U"
    dateOfBirth 19960907.0
    tourneyNum 1
    avgRank 440.78723404255317
    avgPoints 83.97872340425532
  ]
  node [
    id 120
    label "111511"
    name "Noah"
    surname "Rubin"
    country "USA"
    hand "R"
    dateOfBirth 19960221.0
    tourneyNum 6
    avgRank 169.17021276595744
    avgPoints 350.29787234042556
  ]
  node [
    id 121
    label "111513"
    name "Laslo"
    surname "Djere"
    country "SRB"
    hand "R"
    dateOfBirth 19950602.0
    tourneyNum 18
    avgRank 92.76595744680851
    avgPoints 621.4468085106383
  ]
  node [
    id 122
    label "111515"
    name "Thai Son"
    surname "Kwiatkowski"
    country "USA"
    hand "U"
    dateOfBirth 19950213.0
    tourneyNum 1
    avgRank 442.59574468085106
    avgPoints 97.46808510638297
  ]
  node [
    id 123
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 11
    avgRank 96.14893617021276
    avgPoints 669.1489361702128
  ]
  node [
    id 124
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 25
    avgRank 53.638297872340424
    avgPoints 943.7234042553191
  ]
  node [
    id 125
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 20
    avgRank 64.74468085106383
    avgPoints 844.1063829787234
  ]
  node [
    id 126
    label "103333"
    name "Ivo"
    surname "Karlovic"
    country "CRO"
    hand "R"
    dateOfBirth 19790228.0
    tourneyNum 16
    avgRank 101.93617021276596
    avgPoints 572.7659574468086
  ]
  node [
    id 127
    label "205734"
    name "Thiago"
    surname "Seyboth Wild"
    country "BRA"
    hand "R"
    dateOfBirth 20000310.0
    tourneyNum 1
    avgRank 521.1489361702128
    avgPoints 63.702127659574465
  ]
  node [
    id 128
    label "105385"
    name "Donald"
    surname "Young"
    country "USA"
    hand "L"
    dateOfBirth 19890723.0
    tourneyNum 12
    avgRank 185.93617021276594
    avgPoints 372.0425531914894
  ]
  node [
    id 129
    label "105413"
    name "Andrej"
    surname "Martin"
    country "SVK"
    hand "R"
    dateOfBirth 19890920.0
    tourneyNum 2
    avgRank 162.89361702127658
    avgPoints 346.9148936170213
  ]
  node [
    id 130
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 23
    avgRank 95.31914893617021
    avgPoints 604.2765957446809
  ]
  node [
    id 131
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 25
    avgRank 31.148936170212767
    avgPoints 1597.9787234042553
  ]
  node [
    id 132
    label "105432"
    name "Prajnesh"
    surname "Gunneswaran"
    country "IND"
    hand "L"
    dateOfBirth 19891112.0
    tourneyNum 6
    avgRank 181.51063829787233
    avgPoints 332.5531914893617
  ]
  node [
    id 133
    label "111577"
    name "Jared"
    surname "Donaldson"
    country "USA"
    hand "R"
    dateOfBirth 19961009.0
    tourneyNum 19
    avgRank 76.14893617021276
    avgPoints 771.4893617021277
  ]
  node [
    id 134
    label "111576"
    name "Sumit"
    surname "Nagal"
    country "IND"
    hand "R"
    dateOfBirth 19970816.0
    tourneyNum 2
    avgRank 283.06382978723406
    avgPoints 193.36170212765958
  ]
  node [
    id 135
    label "111578"
    name "Stefan"
    surname "Kozlov"
    country "USA"
    hand "R"
    dateOfBirth 19980201.0
    tourneyNum 1
    avgRank 248.31914893617022
    avgPoints 238.51063829787233
  ]
  node [
    id 136
    label "105436"
    name "Markus"
    surname "Eriksson"
    country "SWE"
    hand "R"
    dateOfBirth 19891129.0
    tourneyNum 1
    avgRank 366.531914893617
    avgPoints 121.87234042553192
  ]
  node [
    id 137
    label "111581"
    name "Michael"
    surname "Mmoh"
    country "USA"
    hand "R"
    dateOfBirth 19980110.0
    tourneyNum 6
    avgRank 130.51063829787233
    avgPoints 450.51063829787233
  ]
  node [
    id 138
    label "207836"
    name "Muzammil"
    surname "Murtaza"
    country "PAK"
    hand "R"
    dateOfBirth 19991112.0
    tourneyNum 1
    avgRank 1584.2173913043478
    avgPoints 2.0217391304347827
  ]
  node [
    id 139
    label "111574"
    name "Jc"
    surname "Aragone"
    country "USA"
    hand "R"
    dateOfBirth 19950628.0
    tourneyNum 1
    avgRank 308.02127659574467
    avgPoints 171.2127659574468
  ]
  node [
    id 140
    label "105441"
    name "John Patrick"
    surname "Smith"
    country "AUS"
    hand "L"
    dateOfBirth 19891204.0
    tourneyNum 4
    avgRank 214.93617021276594
    avgPoints 257.531914893617
  ]
  node [
    id 141
    label "134114"
    name "Jad"
    surname "Ballout"
    country "TPE"
    hand "U"
    dateOfBirth 19960922.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 142
    label "134120"
    name "Kuan Yi"
    surname "Lee"
    country "TPE"
    hand "U"
    dateOfBirth 19960909.0
    tourneyNum 1
    avgRank 597.7608695652174
    avgPoints 49.76086956521739
  ]
  node [
    id 143
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 26
    avgRank 41.06382978723404
    avgPoints 1124.3617021276596
  ]
  node [
    id 144
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 20
    avgRank 19.80851063829787
    avgPoints 2191.595744680851
  ]
  node [
    id 145
    label "105467"
    name "Jordi"
    surname "Munoz Abreu"
    country "VEN"
    hand "U"
    dateOfBirth 19900117.0
    tourneyNum 1
    avgRank 1540.8695652173913
    avgPoints 2.108695652173913
  ]
  node [
    id 146
    label "105477"
    name "Marco"
    surname "Trungelliti"
    country "ARG"
    hand "R"
    dateOfBirth 19900131.0
    tourneyNum 3
    avgRank 174.7659574468085
    avgPoints 351.5744680851064
  ]
  node [
    id 147
    label "105479"
    name "Alejandro"
    surname "Mendoza"
    country "BOL"
    hand "R"
    dateOfBirth 19900202.0
    tourneyNum 1
    avgRank 1187.1304347826087
    avgPoints 6.934782608695652
  ]
  node [
    id 148
    label "105487"
    name "Facundo"
    surname "Bagnis"
    country "ARG"
    hand "L"
    dateOfBirth 19900227.0
    tourneyNum 4
    avgRank 171.2127659574468
    avgPoints 336.63829787234044
  ]
  node [
    id 149
    label "123921"
    name "Calvin"
    surname "Hemery"
    country "FRA"
    hand "R"
    dateOfBirth 19950128.0
    tourneyNum 5
    avgRank 167.74468085106383
    avgPoints 344.0425531914894
  ]
  node [
    id 150
    label "105493"
    name "Tomislav"
    surname "Brkic"
    country "BIH"
    hand "R"
    dateOfBirth 19900309.0
    tourneyNum 1
    avgRank 261.6595744680851
    avgPoints 202.5744680851064
  ]
  node [
    id 151
    label "105497"
    name "Jose"
    surname "Hernandez"
    country "DOM"
    hand "U"
    dateOfBirth 19900313.0
    tourneyNum 2
    avgRank 276.74468085106383
    avgPoints 185.9787234042553
  ]
  node [
    id 152
    label "105499"
    name "Christopher"
    surname "Diaz Figueroa"
    country "GUA"
    hand "R"
    dateOfBirth 19900314.0
    tourneyNum 1
    avgRank 677.1304347826087
    avgPoints 36.84782608695652
  ]
  node [
    id 153
    label "105502"
    name "N Sriram"
    surname "Balaji"
    country "IND"
    hand "R"
    dateOfBirth 19900318.0
    tourneyNum 1
    avgRank 598.8510638297872
    avgPoints 51.787234042553195
  ]
  node [
    id 154
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 11
    avgRank 141.0212765957447
    avgPoints 452.1914893617021
  ]
  node [
    id 155
    label "105515"
    name "Jordi"
    surname "Samper Montana"
    country "ESP"
    hand "U"
    dateOfBirth 19900405.0
    tourneyNum 1
    avgRank 385.5531914893617
    avgPoints 107.02127659574468
  ]
  node [
    id 156
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 25
    avgRank 57.5531914893617
    avgPoints 896.6170212765958
  ]
  node [
    id 157
    label "105539"
    name "Evgeny"
    surname "Donskoy"
    country "RUS"
    hand "R"
    dateOfBirth 19900509.0
    tourneyNum 22
    avgRank 85.17021276595744
    avgPoints 670.5531914893617
  ]
  node [
    id 158
    label "134217"
    name "Skyler"
    surname "Butts"
    country "HKG"
    hand "U"
    dateOfBirth 19930918.0
    tourneyNum 1
    avgRank 1230.108695652174
    avgPoints 5.304347826086956
  ]
  node [
    id 159
    label "103499"
    name "Aqeel"
    surname "Khan"
    country "PAK"
    hand "R"
    dateOfBirth 19800130.0
    tourneyNum 2
    avgRank 1788.9347826086957
    avgPoints 1.0
  ]
  node [
    id 160
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 24
    avgRank 63.829787234042556
    avgPoints 828.7659574468086
  ]
  node [
    id 161
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 2
    avgRank 410.8
    avgPoints 175.24444444444444
  ]
  node [
    id 162
    label "105561"
    name "Alessandro"
    surname "Giannessi"
    country "ITA"
    hand "L"
    dateOfBirth 19900530.0
    tourneyNum 1
    avgRank 180.19148936170214
    avgPoints 315.4468085106383
  ]
  node [
    id 163
    label "105566"
    name "Shahin"
    surname "Khaledan"
    country "IRI"
    hand "R"
    dateOfBirth 19900607.0
    tourneyNum 1
    avgRank 1076.3695652173913
    avgPoints 12.0
  ]
  node [
    id 164
    label "207971"
    name "Xavier"
    surname "Lawrence"
    country "BAR"
    hand "U"
    dateOfBirth 19981222.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 165
    label "207972"
    name "Minjong"
    surname "Park"
    country "KOR"
    hand "U"
    dateOfBirth 20011022.0
    tourneyNum 1
    avgRank 1447.1923076923076
    avgPoints 2.3076923076923075
  ]
  node [
    id 166
    label "207973"
    name "Ainius"
    surname "Sabaliauskas"
    country "LTU"
    hand "U"
    dateOfBirth 20030726.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 167
    label "105575"
    name "Ricardas"
    surname "Berankis"
    country "LTU"
    hand "R"
    dateOfBirth 19900621.0
    tourneyNum 17
    avgRank 106.53191489361703
    avgPoints 553.0425531914893
  ]
  node [
    id 168
    label "103529"
    name "Aisam Ul Haq"
    surname "Qureshi"
    country "PAK"
    hand "R"
    dateOfBirth 19800317.0
    tourneyNum 2
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 169
    label "105578"
    name "Cristian"
    surname "Rodriguez"
    country "COL"
    hand "R"
    dateOfBirth 19900624.0
    tourneyNum 1
    avgRank 437.3404255319149
    avgPoints 88.51063829787235
  ]
  node [
    id 170
    label "105577"
    name "Vasek"
    surname "Pospisil"
    country "CAN"
    hand "R"
    dateOfBirth 19900623.0
    tourneyNum 15
    avgRank 83.40425531914893
    avgPoints 670.2978723404256
  ]
  node [
    id 171
    label "124014"
    name "Ernesto"
    surname "Escobedo"
    country "USA"
    hand "R"
    dateOfBirth 19960704.0
    tourneyNum 8
    avgRank 176.04255319148936
    avgPoints 336.6595744680851
  ]
  node [
    id 172
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 25
    avgRank 66.59574468085107
    avgPoints 830.0851063829788
  ]
  node [
    id 173
    label "105585"
    name "Ze"
    surname "Zhang"
    country "CHN"
    hand "R"
    dateOfBirth 19900704.0
    tourneyNum 3
    avgRank 228.80851063829786
    avgPoints 241.19148936170214
  ]
  node [
    id 174
    label "105587"
    name "Tom"
    surname "Kocevar Desman"
    country "SLO"
    hand "R"
    dateOfBirth 19900710.0
    tourneyNum 1
    avgRank 781.7608695652174
    avgPoints 28.26086956521739
  ]
  node [
    id 175
    label "105589"
    name "Gerald"
    surname "Melzer"
    country "AUT"
    hand "L"
    dateOfBirth 19900713.0
    tourneyNum 7
    avgRank 149.51063829787233
    avgPoints 419.4255319148936
  ]
  node [
    id 176
    label "124022"
    name "Chun Hun"
    surname "Wong"
    country "HKG"
    hand "R"
    dateOfBirth 19951030.0
    tourneyNum 1
    avgRank 1035.108695652174
    avgPoints 9.673913043478262
  ]
  node [
    id 177
    label "109698"
    name "Christoffer"
    surname "Konigsfeldt"
    country "DEN"
    hand "R"
    dateOfBirth 19890712.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 178
    label "105606"
    name "Mohsen"
    surname "Hossein Zade"
    country "IRI"
    hand "U"
    dateOfBirth 19900805.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 179
    label "103565"
    name "Stephane"
    surname "Robert"
    country "FRA"
    hand "R"
    dateOfBirth 19800517.0
    tourneyNum 2
    avgRank 169.3404255319149
    avgPoints 335.0851063829787
  ]
  node [
    id 180
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 22
    avgRank 47.297872340425535
    avgPoints 1113.8936170212767
  ]
  node [
    id 181
    label "105614"
    name "Bradley"
    surname "Klahn"
    country "USA"
    hand "L"
    dateOfBirth 19900820.0
    tourneyNum 8
    avgRank 133.29787234042553
    avgPoints 465.21276595744683
  ]
  node [
    id 182
    label "105613"
    name "Norbert"
    surname "Gombos"
    country "SVK"
    hand "R"
    dateOfBirth 19900813.0
    tourneyNum 4
    avgRank 193.68085106382978
    avgPoints 307.3404255319149
  ]
  node [
    id 183
    label "111761"
    name "Benjamin"
    surname "Lock"
    country "ZIM"
    hand "R"
    dateOfBirth 19930304.0
    tourneyNum 2
    avgRank 458.29787234042556
    avgPoints 79.42553191489361
  ]
  node [
    id 184
    label "126102"
    name "Nicolae"
    surname "Frunza"
    country "ROU"
    hand "R"
    dateOfBirth 19971115.0
    tourneyNum 1
    avgRank 639.8510638297872
    avgPoints 42.46808510638298
  ]
  node [
    id 185
    label "105626"
    name "Takanyi"
    surname "Garanganga"
    country "ZIM"
    hand "R"
    dateOfBirth 19900906.0
    tourneyNum 3
    avgRank 487.531914893617
    avgPoints 70.80851063829788
  ]
  node [
    id 186
    label "208028"
    name "Francisco"
    surname "Llanes"
    country "URU"
    hand "U"
    dateOfBirth 20020416.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 187
    label "208029"
    name "Holger Vitus Nodskov"
    surname "Rune"
    country "DEN"
    hand "U"
    dateOfBirth 20030429.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 188
    label "105633"
    name "Mohamed"
    surname "Safwat"
    country "EGY"
    hand "R"
    dateOfBirth 19900919.0
    tourneyNum 4
    avgRank 196.17021276595744
    avgPoints 282.21276595744683
  ]
  node [
    id 189
    label "105634"
    name "Viktor"
    surname "Galovic"
    country "ITA"
    hand "R"
    dateOfBirth 19900919.0
    tourneyNum 3
    avgRank 203.2127659574468
    avgPoints 275.1276595744681
  ]
  node [
    id 190
    label "126120"
    name "Nino"
    surname "Serdarusic"
    country "CRO"
    hand "U"
    dateOfBirth 19961213.0
    tourneyNum 1
    avgRank 231.89361702127658
    avgPoints 240.7872340425532
  ]
  node [
    id 191
    label "105641"
    name "Blaz"
    surname "Rola"
    country "SLO"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 1
    avgRank 219.17021276595744
    avgPoints 254.06382978723406
  ]
  node [
    id 192
    label "109739"
    name "Maximilian"
    surname "Marterer"
    country "GER"
    hand "L"
    dateOfBirth 19950615.0
    tourneyNum 23
    avgRank 66.63829787234043
    avgPoints 808.1063829787234
  ]
  node [
    id 193
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 18
    avgRank 80.7872340425532
    avgPoints 696.5531914893617
  ]
  node [
    id 194
    label "126125"
    name "Maxime"
    surname "Janvier"
    country "FRA"
    hand "U"
    dateOfBirth 19961018.0
    tourneyNum 1
    avgRank 280.3829787234043
    avgPoints 187.29787234042553
  ]
  node [
    id 195
    label "111790"
    name "Brayden"
    surname "Schnur"
    country "CAN"
    hand "R"
    dateOfBirth 19950704.0
    tourneyNum 1
    avgRank 215.59574468085106
    avgPoints 260.1489361702128
  ]
  node [
    id 196
    label "124079"
    name "Pedro"
    surname "Martinez Portero"
    country "ESP"
    hand "R"
    dateOfBirth 19970426.0
    tourneyNum 2
    avgRank 203.40425531914894
    avgPoints 284.82978723404256
  ]
  node [
    id 197
    label "206000"
    name "Michel"
    surname "Saade"
    country "LIB"
    hand "U"
    dateOfBirth 19980929.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 198
    label "105649"
    name "Cedrik Marcel"
    surname "Stebe"
    country "GER"
    hand "L"
    dateOfBirth 19901009.0
    tourneyNum 2
    avgRank 419.9148936170213
    avgPoints 325.36170212765956
  ]
  node [
    id 199
    label "111794"
    name "Kamil"
    surname "Majchrzak"
    country "POL"
    hand "R"
    dateOfBirth 19960113.0
    tourneyNum 3
    avgRank 198.2340425531915
    avgPoints 280.4468085106383
  ]
  node [
    id 200
    label "111795"
    name "Filippo"
    surname "Baldi"
    country "ITA"
    hand "R"
    dateOfBirth 19960110.0
    tourneyNum 1
    avgRank 291.21276595744683
    avgPoints 189.25531914893617
  ]
  node [
    id 201
    label "126127"
    name "Benjamin"
    surname "Bonzi"
    country "FRA"
    hand "U"
    dateOfBirth 19960609.0
    tourneyNum 1
    avgRank 260.2340425531915
    avgPoints 208.0
  ]
  node [
    id 202
    label "111797"
    name "Nicolas"
    surname "Jarry"
    country "CHI"
    hand "R"
    dateOfBirth 19951011.0
    tourneyNum 23
    avgRank 60.234042553191486
    avgPoints 892.5106382978723
  ]
  node [
    id 203
    label "103607"
    name "Victor"
    surname "Estrella"
    country "DOM"
    hand "R"
    dateOfBirth 19800802.0
    tourneyNum 9
    avgRank 217.89361702127658
    avgPoints 291.9574468085106
  ]
  node [
    id 204
    label "105656"
    name "Marcelo"
    surname "Arevalo"
    country "ESA"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 2
    avgRank 182.12765957446808
    avgPoints 314.63829787234044
  ]
  node [
    id 205
    label "105657"
    name "Marius"
    surname "Copil"
    country "ROU"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 23
    avgRank 82.38297872340425
    avgPoints 689.0
  ]
  node [
    id 206
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 6
    avgRank 131.29787234042553
    avgPoints 454.7659574468085
  ]
  node [
    id 207
    label "111805"
    name "Seong Chan"
    surname "Hong"
    country "KOR"
    hand "R"
    dateOfBirth 19970630.0
    tourneyNum 1
    avgRank 669.7446808510638
    avgPoints 36.08510638297872
  ]
  node [
    id 208
    label "138431"
    name "Darko"
    surname "Bojanovic"
    country "BIH"
    hand "U"
    dateOfBirth 19970208.0
    tourneyNum 1
    avgRank 1030.9347826086957
    avgPoints 11.173913043478262
  ]
  node [
    id 209
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 18
    avgRank 91.1063829787234
    avgPoints 644.5744680851063
  ]
  node [
    id 210
    label "105671"
    name "Gastao"
    surname "Elias"
    country "POR"
    hand "R"
    dateOfBirth 19901124.0
    tourneyNum 7
    avgRank 161.04255319148936
    avgPoints 401.17021276595744
  ]
  node [
    id 211
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 17
    avgRank 11.914893617021276
    avgPoints 2930.1063829787236
  ]
  node [
    id 212
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 19
    avgRank 24.659574468085108
    avgPoints 1639.5744680851064
  ]
  node [
    id 213
    label "124116"
    name "Sebastian"
    surname "Ofner"
    country "AUT"
    hand "R"
    dateOfBirth 19960512.0
    tourneyNum 3
    avgRank 166.70212765957447
    avgPoints 341.8936170212766
  ]
  node [
    id 214
    label "105688"
    name "Manuel"
    surname "Sanchez"
    country "MEX"
    hand "R"
    dateOfBirth 19910105.0
    tourneyNum 1
    avgRank 706.1276595744681
    avgPoints 33.87234042553192
  ]
  node [
    id 215
    label "105691"
    name "Scott"
    surname "Griekspoor"
    country "NED"
    hand "R"
    dateOfBirth 19910110.0
    tourneyNum 1
    avgRank 264.1063829787234
    avgPoints 206.14893617021278
  ]
  node [
    id 216
    label "122078"
    name "Dmitry"
    surname "Popko"
    country "KAZ"
    hand "R"
    dateOfBirth 19961024.0
    tourneyNum 2
    avgRank 375.40425531914894
    avgPoints 129.89361702127658
  ]
  node [
    id 217
    label "126174"
    name "Youssef"
    surname "Hossam"
    country "EGY"
    hand "U"
    dateOfBirth 19980603.0
    tourneyNum 2
    avgRank 357.5744680851064
    avgPoints 127.48936170212765
  ]
  node [
    id 218
    label "124126"
    name "Sarp"
    surname "Agabigun"
    country "TUR"
    hand "R"
    dateOfBirth 19970612.0
    tourneyNum 1
    avgRank 951.2608695652174
    avgPoints 13.652173913043478
  ]
  node [
    id 219
    label "105707"
    name "Nicolaas"
    surname "Scholtz"
    country "RSA"
    hand "R"
    dateOfBirth 19910205.0
    tourneyNum 1
    avgRank 438.06382978723406
    avgPoints 97.19148936170212
  ]
  node [
    id 220
    label "126190"
    name "Nicolas"
    surname "Alvarez"
    country "PER"
    hand "R"
    dateOfBirth 19960608.0
    tourneyNum 1
    avgRank 716.7234042553191
    avgPoints 34.744680851063826
  ]
  node [
    id 221
    label "136440"
    name "Dominik"
    surname "Koepfer"
    country "GER"
    hand "L"
    dateOfBirth 19940429.0
    tourneyNum 2
    avgRank 218.80851063829786
    avgPoints 261.72340425531917
  ]
  node [
    id 222
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 19
    avgRank 66.17021276595744
    avgPoints 825.7872340425532
  ]
  node [
    id 223
    label "126204"
    name "Gerardo"
    surname "Lopez Villasenor"
    country "MEX"
    hand "U"
    dateOfBirth 19950512.0
    tourneyNum 2
    avgRank 816.45
    avgPoints 22.9
  ]
  node [
    id 224
    label "126205"
    name "Tommy"
    surname "Paul"
    country "USA"
    hand "R"
    dateOfBirth 19970517.0
    tourneyNum 2
    avgRank 219.12765957446808
    avgPoints 267.531914893617
  ]
  node [
    id 225
    label "122109"
    name "Sanjar"
    surname "Fayziev"
    country "UZB"
    hand "U"
    dateOfBirth 19940729.0
    tourneyNum 2
    avgRank 457.5744680851064
    avgPoints 81.12765957446808
  ]
  node [
    id 226
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 54.255319148936174
    avgPoints 944.6595744680851
  ]
  node [
    id 227
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 21
    avgRank 71.57446808510639
    avgPoints 769.5957446808511
  ]
  node [
    id 228
    label "144645"
    name "Zdenek"
    surname "Kolar"
    country "CZE"
    hand "U"
    dateOfBirth 19961009.0
    tourneyNum 1
    avgRank 225.40425531914894
    avgPoints 245.17021276595744
  ]
  node [
    id 229
    label "105747"
    name "Karim Mohamed"
    surname "Maamoun"
    country "EGY"
    hand "R"
    dateOfBirth 19910409.0
    tourneyNum 3
    avgRank 349.8510638297872
    avgPoints 133.27659574468086
  ]
  node [
    id 230
    label "124187"
    name "Reilly"
    surname "Opelka"
    country "USA"
    hand "R"
    dateOfBirth 19970828.0
    tourneyNum 2
    avgRank 160.08510638297872
    avgPoints 379.3404255319149
  ]
  node [
    id 231
    label "105757"
    name "Sandro"
    surname "Ehrat"
    country "SUI"
    hand "R"
    dateOfBirth 19910418.0
    tourneyNum 1
    avgRank 855.3809523809524
    avgPoints 31.285714285714285
  ]
  node [
    id 232
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 19
    avgRank 8.23404255319149
    avgPoints 3956.808510638298
  ]
  node [
    id 233
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 10
    avgRank 145.38297872340425
    avgPoints 406.8085106382979
  ]
  node [
    id 234
    label "144707"
    name "Mikael"
    surname "Ymer"
    country "SWE"
    hand "R"
    dateOfBirth 19980909.0
    tourneyNum 5
    avgRank 331.4468085106383
    avgPoints 142.46808510638297
  ]
  node [
    id 235
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 3
    avgRank 219.70212765957447
    avgPoints 335.93617021276594
  ]
  node [
    id 236
    label "105806"
    name "Mirza"
    surname "Basic"
    country "BIH"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 20
    avgRank 87.85106382978724
    avgPoints 659.3191489361702
  ]
  node [
    id 237
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 22
    avgRank 15.872340425531915
    avgPoints 2140.744680851064
  ]
  node [
    id 238
    label "144719"
    name "Jaume"
    surname "Munar"
    country "ESP"
    hand "R"
    dateOfBirth 19970505.0
    tourneyNum 11
    avgRank 122.2127659574468
    avgPoints 523.936170212766
  ]
  node [
    id 239
    label "105812"
    name "David"
    surname "Agung Susanto"
    country "INA"
    hand "U"
    dateOfBirth 19910721.0
    tourneyNum 1
    avgRank 1260.7391304347825
    avgPoints 4.8478260869565215
  ]
  node [
    id 240
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 20
    avgRank 60.255319148936174
    avgPoints 870.8510638297872
  ]
  node [
    id 241
    label "105819"
    name "Guido"
    surname "Andreozzi"
    country "ARG"
    hand "R"
    dateOfBirth 19910805.0
    tourneyNum 6
    avgRank 128.68085106382978
    avgPoints 483.02127659574467
  ]
  node [
    id 242
    label "200031"
    name "Lucas"
    surname "Catarina"
    country "MON"
    hand "R"
    dateOfBirth 19960826.0
    tourneyNum 1
    avgRank 400.36170212765956
    avgPoints 103.8936170212766
  ]
  node [
    id 243
    label "105827"
    name "Laurynas"
    surname "Grigelis"
    country "LTU"
    hand "R"
    dateOfBirth 19910814.0
    tourneyNum 2
    avgRank 267.6808510638298
    avgPoints 197.06382978723406
  ]
  node [
    id 244
    label "103781"
    name "Jurgen"
    surname "Melzer"
    country "AUT"
    hand "L"
    dateOfBirth 19810522.0
    tourneyNum 2
    avgRank 527.5957446808511
    avgPoints 92.29787234042553
  ]
  node [
    id 245
    label "202090"
    name "Adrian"
    surname "Andreev"
    country "BUL"
    hand "U"
    dateOfBirth 20010512.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 246
    label "200045"
    name "Anthony"
    surname "Susanto"
    country "INA"
    hand "U"
    dateOfBirth 19970523.0
    tourneyNum 1
    avgRank 1769.9259259259259
    avgPoints 1.2222222222222223
  ]
  node [
    id 247
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 3
    avgRank 195.46808510638297
    avgPoints 333.51063829787233
  ]
  node [
    id 248
    label "105842"
    name "Di"
    surname "Wu"
    country "CHN"
    hand "R"
    dateOfBirth 19910914.0
    tourneyNum 3
    avgRank 290.29787234042556
    avgPoints 172.7872340425532
  ]
  node [
    id 249
    label "144760"
    name "Amine"
    surname "Ahouda"
    country "MAR"
    hand "U"
    dateOfBirth 19970911.0
    tourneyNum 3
    avgRank 1173.0434782608695
    avgPoints 11.26086956521739
  ]
  node [
    id 250
    label "200059"
    name "Yibing"
    surname "Wu"
    country "CHN"
    hand "R"
    dateOfBirth 19991014.0
    tourneyNum 6
    avgRank 337.0
    avgPoints 136.74468085106383
  ]
  node [
    id 251
    label "122236"
    name "Roman"
    surname "Hassanov"
    country "KAZ"
    hand "R"
    dateOfBirth 19960907.0
    tourneyNum 1
    avgRank 758.7173913043479
    avgPoints 27.17391304347826
  ]
  node [
    id 252
    label "132482"
    name "Liam"
    surname "Caruana"
    country "USA"
    hand "U"
    dateOfBirth 19980122.0
    tourneyNum 2
    avgRank 516.531914893617
    avgPoints 70.48936170212765
  ]
  node [
    id 253
    label "126340"
    name "Viktor"
    surname "Durasovic"
    country "NOR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 1
    avgRank 431.8510638297872
    avgPoints 87.7872340425532
  ]
  node [
    id 254
    label "144776"
    name "Christophe"
    surname "Tholl"
    country "LUX"
    hand "U"
    dateOfBirth 19941219.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 255
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 13
    avgRank 2.127659574468085
    avgPoints 7868.936170212766
  ]
  node [
    id 256
    label "200075"
    name "Yshai"
    surname "Oliel"
    country "ISR"
    hand "L"
    dateOfBirth 20000105.0
    tourneyNum 1
    avgRank 611.695652173913
    avgPoints 45.97826086956522
  ]
  node [
    id 257
    label "105870"
    name "Yannick"
    surname "Hanfmann"
    country "GER"
    hand "U"
    dateOfBirth 19911113.0
    tourneyNum 6
    avgRank 129.9787234042553
    avgPoints 442.72340425531917
  ]
  node [
    id 258
    label "202130"
    name "Emil"
    surname "Reinberg"
    country "USA"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 1
    avgRank 961.0
    avgPoints 12.521739130434783
  ]
  node [
    id 259
    label "105882"
    name "Stefano"
    surname "Travaglia"
    country "ITA"
    hand "U"
    dateOfBirth 19911218.0
    tourneyNum 7
    avgRank 134.38297872340425
    avgPoints 429.8936170212766
  ]
  node [
    id 260
    label "200095"
    name "Nicola"
    surname "Kuhn"
    country "ESP"
    hand "R"
    dateOfBirth 20000320.0
    tourneyNum 1
    avgRank 270.82978723404256
    avgPoints 201.51063829787233
  ]
  node [
    id 261
    label "144807"
    name "Kenneth"
    surname "Raisma"
    country "EST"
    hand "L"
    dateOfBirth 19980403.0
    tourneyNum 1
    avgRank 797.9782608695652
    avgPoints 23.956521739130434
  ]
  node [
    id 262
    label "105899"
    name "Martin"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19920114.0
    tourneyNum 3
    avgRank 294.72340425531917
    avgPoints 168.59574468085106
  ]
  node [
    id 263
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 21
    avgRank 52.276595744680854
    avgPoints 1041.1702127659576
  ]
  node [
    id 264
    label "105902"
    name "James"
    surname "Duckworth"
    country "AUS"
    hand "R"
    dateOfBirth 19920121.0
    tourneyNum 6
    avgRank 664.8297872340426
    avgPoints 87.87234042553192
  ]
  node [
    id 265
    label "144817"
    name "Marc Andrea"
    surname "Huesler"
    country "SUI"
    hand "U"
    dateOfBirth 19960624.0
    tourneyNum 3
    avgRank 481.51063829787233
    avgPoints 77.48936170212765
  ]
  node [
    id 266
    label "202165"
    name "Alberto"
    surname "Lim"
    country "PHI"
    hand "R"
    dateOfBirth 19990518.0
    tourneyNum 1
    avgRank 1479.7173913043478
    avgPoints 2.5217391304347827
  ]
  node [
    id 267
    label "144826"
    name "Aziz"
    surname "Dougaz"
    country "TUN"
    hand "U"
    dateOfBirth 19970326.0
    tourneyNum 1
    avgRank 596.936170212766
    avgPoints 49.234042553191486
  ]
  node [
    id 268
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 25
    avgRank 51.12765957446808
    avgPoints 974.936170212766
  ]
  node [
    id 269
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 29
    avgRank 52.42553191489362
    avgPoints 1126.4255319148936
  ]
  node [
    id 270
    label "105933"
    name "Roberto"
    surname "Quiroz"
    country "ECU"
    hand "L"
    dateOfBirth 19920223.0
    tourneyNum 3
    avgRank 231.12765957446808
    avgPoints 240.9787234042553
  ]
  node [
    id 271
    label "105935"
    name "Duilio"
    surname "Beretta"
    country "PER"
    hand "R"
    dateOfBirth 19920225.0
    tourneyNum 1
    avgRank 1380.3695652173913
    avgPoints 2.739130434782609
  ]
  node [
    id 272
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 17
    avgRank 43.46808510638298
    avgPoints 1250.8936170212767
  ]
  node [
    id 273
    label "103893"
    name "Paolo"
    surname "Lorenzi"
    country "ITA"
    hand "R"
    dateOfBirth 19811215.0
    tourneyNum 15
    avgRank 86.14893617021276
    avgPoints 698.7872340425532
  ]
  node [
    id 274
    label "105943"
    name "Federico"
    surname "Gaio"
    country "ITA"
    hand "R"
    dateOfBirth 19920305.0
    tourneyNum 2
    avgRank 260.9574468085106
    avgPoints 207.06382978723406
  ]
  node [
    id 275
    label "103898"
    name "Julien"
    surname "Benneteau"
    country "FRA"
    hand "R"
    dateOfBirth 19811220.0
    tourneyNum 11
    avgRank 71.5111111111111
    avgPoints 813.6
  ]
  node [
    id 276
    label "122330"
    name "Alexander"
    surname "Bublik"
    country "KAZ"
    hand "R"
    dateOfBirth 19970617.0
    tourneyNum 4
    avgRank 166.19148936170214
    avgPoints 352.8723404255319
  ]
  node [
    id 277
    label "105948"
    name "Federico"
    surname "Coria"
    country "ARG"
    hand "R"
    dateOfBirth 19920309.0
    tourneyNum 1
    avgRank 324.3404255319149
    avgPoints 146.51063829787233
  ]
  node [
    id 278
    label "105952"
    name "Renzo"
    surname "Olivo"
    country "ARG"
    hand "R"
    dateOfBirth 19920315.0
    tourneyNum 1
    avgRank 304.7659574468085
    avgPoints 200.89361702127658
  ]
  node [
    id 279
    label "202210"
    name "Edris"
    surname "Fetisleam"
    country "ROU"
    hand "U"
    dateOfBirth 19990725.0
    tourneyNum 1
    avgRank 1164.1739130434783
    avgPoints 7.869565217391305
  ]
  node [
    id 280
    label "105960"
    name "Evan"
    surname "King"
    country "USA"
    hand "L"
    dateOfBirth 19920325.0
    tourneyNum 1
    avgRank 237.29787234042553
    avgPoints 238.5744680851064
  ]
  node [
    id 281
    label "105961"
    name "David"
    surname "Souto"
    country "VEN"
    hand "L"
    dateOfBirth 19920326.0
    tourneyNum 1
    avgRank 1058.6774193548388
    avgPoints 12.258064516129032
  ]
  node [
    id 282
    label "103917"
    name "Nicolas"
    surname "Mahut"
    country "FRA"
    hand "R"
    dateOfBirth 19820121.0
    tourneyNum 9
    avgRank 138.87234042553192
    avgPoints 426.51063829787233
  ]
  node [
    id 283
    label "105967"
    name "Henri"
    surname "Laaksonen"
    country "SUI"
    hand "R"
    dateOfBirth 19920331.0
    tourneyNum 6
    avgRank 142.7659574468085
    avgPoints 401.3191489361702
  ]
  node [
    id 284
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 2
    avgRank 176.72340425531914
    avgPoints 328.8510638297872
  ]
  node [
    id 285
    label "144895"
    name "Corentin"
    surname "Moutet"
    country "FRA"
    hand "L"
    dateOfBirth 19990419.0
    tourneyNum 11
    avgRank 135.10638297872342
    avgPoints 421.3191489361702
  ]
  node [
    id 286
    label "105985"
    name "Darian"
    surname "King"
    country "BAR"
    hand "R"
    dateOfBirth 19920426.0
    tourneyNum 3
    avgRank 182.04255319148936
    avgPoints 304.82978723404256
  ]
  node [
    id 287
    label "105992"
    name "Ryan"
    surname "Harrison"
    country "USA"
    hand "R"
    dateOfBirth 19920507.0
    tourneyNum 22
    avgRank 55.59574468085106
    avgPoints 926.9148936170212
  ]
  node [
    id 288
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 32
    avgRank 34.04255319148936
    avgPoints 1336.4893617021276
  ]
  node [
    id 289
    label "106005"
    name "Constant"
    surname "Lestienne"
    country "FRA"
    hand "R"
    dateOfBirth 19920523.0
    tourneyNum 2
    avgRank 206.04255319148936
    avgPoints 287.0
  ]
  node [
    id 290
    label "200218"
    name "Mate"
    surname "Valkusz"
    country "HUN"
    hand "R"
    dateOfBirth 19980813.0
    tourneyNum 1
    avgRank 466.06382978723406
    avgPoints 111.8936170212766
  ]
  node [
    id 291
    label "103970"
    name "David"
    surname "Ferrer"
    country "ESP"
    hand "R"
    dateOfBirth 19820402.0
    tourneyNum 18
    avgRank 77.82978723404256
    avgPoints 901.9148936170212
  ]
  node [
    id 292
    label "106021"
    name "Dragos"
    surname "Dima"
    country "ROU"
    hand "R"
    dateOfBirth 19920616.0
    tourneyNum 1
    avgRank 381.17021276595744
    avgPoints 116.51063829787235
  ]
  node [
    id 293
    label "126502"
    name "Luis Diego"
    surname "Chavez Villalpando"
    country "ESP"
    hand "U"
    dateOfBirth 19951024.0
    tourneyNum 1
    avgRank 1581.0
    avgPoints 2.108695652173913
  ]
  node [
    id 294
    label "106026"
    name "Yuki"
    surname "Bhambri"
    country "IND"
    hand "R"
    dateOfBirth 19920704.0
    tourneyNum 10
    avgRank 104.08510638297872
    avgPoints 570.6595744680851
  ]
  node [
    id 295
    label "103990"
    name "Tommy"
    surname "Robredo"
    country "ESP"
    hand "R"
    dateOfBirth 19820501.0
    tourneyNum 2
    avgRank 202.85106382978722
    avgPoints 278.0851063829787
  ]
  node [
    id 296
    label "200250"
    name "Mehluli Don Ayanda"
    surname "Sibanda"
    country "ZIM"
    hand "R"
    dateOfBirth 19991106.0
    tourneyNum 1
    avgRank 954.8148148148148
    avgPoints 13.481481481481481
  ]
  node [
    id 297
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 16.27659574468085
    avgPoints 2088.0851063829787
  ]
  node [
    id 298
    label "106044"
    name "Nicolas"
    surname "Kicker"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 12
    avgRank 121.44680851063829
    avgPoints 505.4255319148936
  ]
  node [
    id 299
    label "106045"
    name "Denis"
    surname "Kudla"
    country "USA"
    hand "R"
    dateOfBirth 19920817.0
    tourneyNum 14
    avgRank 101.91489361702128
    avgPoints 614.1914893617021
  ]
  node [
    id 300
    label "126526"
    name "Soren"
    surname "Hess Olesen"
    country "DEN"
    hand "U"
    dateOfBirth 19910109.0
    tourneyNum 1
    avgRank 1231.2903225806451
    avgPoints 6.580645161290323
  ]
  node [
    id 301
    label "126523"
    name "Bernabe"
    surname "Zapata Miralles"
    country "ESP"
    hand "R"
    dateOfBirth 19970112.0
    tourneyNum 1
    avgRank 281.9574468085106
    avgPoints 180.7659574468085
  ]
  node [
    id 302
    label "126530"
    name "Peter"
    surname "Bothwell"
    country "IRL"
    hand "R"
    dateOfBirth 19950928.0
    tourneyNum 1
    avgRank 757.5957446808511
    avgPoints 28.02127659574468
  ]
  node [
    id 303
    label "126535"
    name "Carlos"
    surname "Taberner"
    country "ESP"
    hand "R"
    dateOfBirth 19970808.0
    tourneyNum 2
    avgRank 237.17021276595744
    avgPoints 241.2127659574468
  ]
  node [
    id 304
    label "106058"
    name "Jack"
    surname "Sock"
    country "USA"
    hand "R"
    dateOfBirth 19920924.0
    tourneyNum 21
    avgRank 31.93617021276596
    avgPoints 1900.531914893617
  ]
  node [
    id 305
    label "200273"
    name "Hady"
    surname "Habib"
    country "LIB"
    hand "U"
    dateOfBirth 19980821.0
    tourneyNum 3
    avgRank 798.0869565217391
    avgPoints 28.347826086956523
  ]
  node [
    id 306
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 25
    avgRank 50.61702127659574
    avgPoints 1287.5106382978724
  ]
  node [
    id 307
    label "200274"
    name "Alex"
    surname "Diaz"
    country "PUR"
    hand "U"
    dateOfBirth 19970802.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 308
    label "104022"
    name "Mikhail"
    surname "Youzhny"
    country "RUS"
    hand "R"
    dateOfBirth 19820625.0
    tourneyNum 18
    avgRank 99.0909090909091
    avgPoints 579.939393939394
  ]
  node [
    id 309
    label "106071"
    name "Bernard"
    surname "Tomic"
    country "AUS"
    hand "R"
    dateOfBirth 19921021.0
    tourneyNum 8
    avgRank 142.80851063829786
    avgPoints 439.1063829787234
  ]
  node [
    id 310
    label "106072"
    name "Alexey"
    surname "Vatutin"
    country "RUS"
    hand "U"
    dateOfBirth 19921027.0
    tourneyNum 2
    avgRank 163.95744680851064
    avgPoints 342.78723404255317
  ]
  node [
    id 311
    label "200278"
    name "Simon"
    surname "Carr"
    country "IRL"
    hand "R"
    dateOfBirth 19991107.0
    tourneyNum 1
    avgRank 942.195652173913
    avgPoints 14.08695652173913
  ]
  node [
    id 312
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 22
    avgRank 79.72340425531915
    avgPoints 834.9148936170212
  ]
  node [
    id 313
    label "106075"
    name "Jozef"
    surname "Kovalik"
    country "SVK"
    hand "R"
    dateOfBirth 19921104.0
    tourneyNum 6
    avgRank 117.95744680851064
    avgPoints 517.3829787234042
  ]
  node [
    id 314
    label "138845"
    name "Anthony Jackie"
    surname "Tang"
    country "HKG"
    hand "U"
    dateOfBirth 19981126.0
    tourneyNum 1
    avgRank 1387.4130434782608
    avgPoints 3.0869565217391304
  ]
  node [
    id 315
    label "138846"
    name "Hong Kit Jack"
    surname "Wong"
    country "HKG"
    hand "U"
    dateOfBirth 19980830.0
    tourneyNum 2
    avgRank 959.0
    avgPoints 14.23404255319149
  ]
  node [
    id 316
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 2
    avgRank 204.40425531914894
    avgPoints 285.25531914893617
  ]
  node [
    id 317
    label "120424"
    name "Yannick"
    surname "Maden"
    country "GER"
    hand "U"
    dateOfBirth 19891028.0
    tourneyNum 8
    avgRank 129.53191489361703
    avgPoints 441.40425531914894
  ]
  node [
    id 318
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 9
    avgRank 143.14893617021278
    avgPoints 414.93617021276594
  ]
  node [
    id 319
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 3
    avgRank 187.7872340425532
    avgPoints 300.93617021276594
  ]
  node [
    id 320
    label "202358"
    name "Chun Hsin"
    surname "Tseng"
    country "TPE"
    hand "R"
    dateOfBirth 20010808.0
    tourneyNum 2
    avgRank 671.225
    avgPoints 51.725
  ]
  node [
    id 321
    label "106105"
    name "Guilherme"
    surname "Clezar"
    country "BRA"
    hand "R"
    dateOfBirth 19921231.0
    tourneyNum 2
    avgRank 223.06382978723406
    avgPoints 247.53191489361703
  ]
  node [
    id 322
    label "106106"
    name "George"
    surname "Tsivadze"
    country "GEO"
    hand "U"
    dateOfBirth 19930102.0
    tourneyNum 1
    avgRank 1091.7826086956522
    avgPoints 8.23913043478261
  ]
  node [
    id 323
    label "106109"
    name "Alex"
    surname "Bolt"
    country "AUS"
    hand "L"
    dateOfBirth 19930105.0
    tourneyNum 7
    avgRank 177.04255319148936
    avgPoints 321.8085106382979
  ]
  node [
    id 324
    label "106110"
    name "Filip"
    surname "Horansky"
    country "SVK"
    hand "R"
    dateOfBirth 19930107.0
    tourneyNum 3
    avgRank 269.6595744680851
    avgPoints 201.93617021276594
  ]
  node [
    id 325
    label "200325"
    name "Emil"
    surname "Ruusuvuori"
    country "FIN"
    hand "R"
    dateOfBirth 19990402.0
    tourneyNum 3
    avgRank 473.06382978723406
    avgPoints 82.06382978723404
  ]
  node [
    id 326
    label "106120"
    name "Ricardo"
    surname "Ojeda Lara"
    country "ESP"
    hand "U"
    dateOfBirth 19930126.0
    tourneyNum 3
    avgRank 238.74468085106383
    avgPoints 236.6595744680851
  ]
  node [
    id 327
    label "106121"
    name "Taro"
    surname "Daniel"
    country "JPN"
    hand "R"
    dateOfBirth 19930127.0
    tourneyNum 20
    avgRank 87.08510638297872
    avgPoints 656.8085106382979
  ]
  node [
    id 328
    label "202382"
    name "Simen Sunde"
    surname "Bratholm"
    country "NOR"
    hand "R"
    dateOfBirth 19990304.0
    tourneyNum 1
    avgRank 1516.8823529411766
    avgPoints 1.911764705882353
  ]
  node [
    id 329
    label "202385"
    name "Jenson"
    surname "Brooksby"
    country "USA"
    hand "U"
    dateOfBirth 20001026.0
    tourneyNum 1
    avgRank 1354.1739130434783
    avgPoints 3.847826086956522
  ]
  node [
    id 330
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 20
    avgRank 81.29787234042553
    avgPoints 739.2127659574468
  ]
  node [
    id 331
    label "126613"
    name "Bogdan"
    surname "Borza"
    country "ROU"
    hand "L"
    dateOfBirth 19970405.0
    tourneyNum 1
    avgRank 774.804347826087
    avgPoints 25.434782608695652
  ]
  node [
    id 332
    label "106138"
    name "Yaraslau"
    surname "Shyla"
    country "BLR"
    hand "R"
    dateOfBirth 19930305.0
    tourneyNum 1
    avgRank 499.78723404255317
    avgPoints 70.95744680851064
  ]
  node [
    id 333
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 18
    avgRank 82.59574468085107
    avgPoints 680.2127659574468
  ]
  node [
    id 334
    label "106150"
    name "Jeson"
    surname "Patrombon"
    country "PHI"
    hand "R"
    dateOfBirth 19930327.0
    tourneyNum 2
    avgRank 1609.0869565217392
    avgPoints 1.5
  ]
  node [
    id 335
    label "122548"
    name "Edan"
    surname "Leshem"
    country "ISR"
    hand "U"
    dateOfBirth 19970319.0
    tourneyNum 2
    avgRank 358.72340425531917
    avgPoints 130.04255319148936
  ]
  node [
    id 336
    label "106167"
    name "Joris"
    surname "De Loore"
    country "BEL"
    hand "R"
    dateOfBirth 19930421.0
    tourneyNum 2
    avgRank 351.97872340425533
    avgPoints 127.14893617021276
  ]
  node [
    id 337
    label "104122"
    name "Carlos"
    surname "Berlocq"
    country "ARG"
    hand "R"
    dateOfBirth 19830203.0
    tourneyNum 5
    avgRank 140.93617021276594
    avgPoints 415.4255319148936
  ]
  node [
    id 338
    label "126652"
    name "Jay"
    surname "Clarke"
    country "GBR"
    hand "R"
    dateOfBirth 19980727.0
    tourneyNum 3
    avgRank 216.9787234042553
    avgPoints 259.63829787234044
  ]
  node [
    id 339
    label "106174"
    name "Gonzalo"
    surname "Lama"
    country "CHI"
    hand "R"
    dateOfBirth 19930427.0
    tourneyNum 1
    avgRank 502.82978723404256
    avgPoints 72.59574468085107
  ]
  node [
    id 340
    label "106175"
    name "Ricardo"
    surname "Rodriguez"
    country "VEN"
    hand "R"
    dateOfBirth 19930428.0
    tourneyNum 2
    avgRank 761.468085106383
    avgPoints 27.06382978723404
  ]
  node [
    id 341
    label "200384"
    name "Hugo"
    surname "Gaston"
    country "FRA"
    hand "U"
    dateOfBirth 20000926.0
    tourneyNum 1
    avgRank 1230.7608695652175
    avgPoints 4.956521739130435
  ]
  node [
    id 342
    label "106177"
    name "Oriol"
    surname "Roca Batalla"
    country "ESP"
    hand "R"
    dateOfBirth 19930430.0
    tourneyNum 1
    avgRank 324.5744680851064
    avgPoints 153.70212765957447
  ]
  node [
    id 343
    label "106186"
    name "Jason"
    surname "Kubler"
    country "AUS"
    hand "R"
    dateOfBirth 19930519.0
    tourneyNum 4
    avgRank 148.2340425531915
    avgPoints 425.9574468085106
  ]
  node [
    id 344
    label "200401"
    name "Amir Hossein"
    surname "Badi"
    country "IRI"
    hand "U"
    dateOfBirth 19981209.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 345
    label "134868"
    name "Tallon"
    surname "Griekspoor"
    country "NED"
    hand "R"
    dateOfBirth 19960702.0
    tourneyNum 1
    avgRank 220.91489361702128
    avgPoints 250.46808510638297
  ]
  node [
    id 346
    label "106198"
    name "Hugo"
    surname "Dellien"
    country "BOL"
    hand "R"
    dateOfBirth 19930616.0
    tourneyNum 1
    avgRank 153.29787234042553
    avgPoints 414.06382978723406
  ]
  node [
    id 347
    label "202462"
    name "Yasitha"
    surname "De Silva"
    country "SRI"
    hand "U"
    dateOfBirth 19950703.0
    tourneyNum 1
    avgRank 1686.3636363636363
    avgPoints 1.0
  ]
  node [
    id 348
    label "106210"
    name "Jiri"
    surname "Vesely"
    country "CZE"
    hand "L"
    dateOfBirth 19930710.0
    tourneyNum 19
    avgRank 81.70212765957447
    avgPoints 697.3191489361702
  ]
  node [
    id 349
    label "134886"
    name "Sharmal"
    surname "Dissanayake"
    country "SRI"
    hand "R"
    dateOfBirth 19960514.0
    tourneyNum 1
    avgRank 1397.7608695652175
    avgPoints 2.5652173913043477
  ]
  node [
    id 350
    label "106214"
    name "Oscar"
    surname "Otte"
    country "GER"
    hand "R"
    dateOfBirth 19930716.0
    tourneyNum 2
    avgRank 165.59574468085106
    avgPoints 346.3191489361702
  ]
  node [
    id 351
    label "106216"
    name "Bjorn"
    surname "Fratangelo"
    country "USA"
    hand "R"
    dateOfBirth 19930719.0
    tourneyNum 6
    avgRank 131.4255319148936
    avgPoints 446.6170212765957
  ]
  node [
    id 352
    label "106218"
    name "Marcos"
    surname "Giron"
    country "USA"
    hand "R"
    dateOfBirth 19930724.0
    tourneyNum 1
    avgRank 350.27659574468083
    avgPoints 127.74468085106383
  ]
  node [
    id 353
    label "106220"
    name "Dimitar"
    surname "Kuzmanov"
    country "BUL"
    hand "R"
    dateOfBirth 19930728.0
    tourneyNum 1
    avgRank 305.7659574468085
    avgPoints 161.12765957446808
  ]
  node [
    id 354
    label "104180"
    name "Gilles"
    surname "Muller"
    country "LUX"
    hand "L"
    dateOfBirth 19830509.0
    tourneyNum 19
    avgRank 82.55319148936171
    avgPoints 913.9787234042553
  ]
  node [
    id 355
    label "200436"
    name "Zsombor"
    surname "Piros"
    country "HUN"
    hand "U"
    dateOfBirth 19991013.0
    tourneyNum 3
    avgRank 439.17021276595744
    avgPoints 86.65957446808511
  ]
  node [
    id 356
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 1
    avgRank 195.85106382978722
    avgPoints 346.36170212765956
  ]
  node [
    id 357
    label "106232"
    name "Roberto"
    surname "Cid"
    country "DOM"
    hand "R"
    dateOfBirth 19930830.0
    tourneyNum 1
    avgRank 292.82978723404256
    avgPoints 187.31914893617022
  ]
  node [
    id 358
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 25
    avgRank 7.276595744680851
    avgPoints 3811.3829787234044
  ]
  node [
    id 359
    label "104198"
    name "Guillermo"
    surname "Garcia Lopez"
    country "ESP"
    hand "R"
    dateOfBirth 19830604.0
    tourneyNum 23
    avgRank 78.74468085106383
    avgPoints 724.8723404255319
  ]
  node [
    id 360
    label "106249"
    name "Joao"
    surname "Domingues"
    country "POR"
    hand "R"
    dateOfBirth 19931005.0
    tourneyNum 2
    avgRank 219.85106382978722
    avgPoints 255.74468085106383
  ]
  node [
    id 361
    label "106250"
    name "Luis"
    surname "Patino"
    country "MEX"
    hand "R"
    dateOfBirth 19931006.0
    tourneyNum 3
    avgRank 839.3191489361702
    avgPoints 19.78723404255319
  ]
  node [
    id 362
    label "106252"
    name "Joao Pedro"
    surname "Sorgi"
    country "BRA"
    hand "R"
    dateOfBirth 19931018.0
    tourneyNum 2
    avgRank 533.1063829787234
    avgPoints 70.14893617021276
  ]
  node [
    id 363
    label "200460"
    name "John Bryan Decasa"
    surname "Otico"
    country "PHI"
    hand "U"
    dateOfBirth 19990625.0
    tourneyNum 2
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 364
    label "200483"
    name "Peter August"
    surname "Anker"
    country "NOR"
    hand "U"
    dateOfBirth 19990603.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 365
    label "200484"
    name "Rudolf"
    surname "Molleker"
    country "GER"
    hand "R"
    dateOfBirth 20001026.0
    tourneyNum 3
    avgRank 347.8510638297872
    avgPoints 173.29787234042553
  ]
  node [
    id 366
    label "104233"
    name "Frederik"
    surname "Nielsen"
    country "DEN"
    hand "R"
    dateOfBirth 19830827.0
    tourneyNum 2
    avgRank 586.0425531914893
    avgPoints 49.638297872340424
  ]
  node [
    id 367
    label "106281"
    name "Liam"
    surname "Broady"
    country "GBR"
    hand "L"
    dateOfBirth 19940104.0
    tourneyNum 3
    avgRank 216.9787234042553
    avgPoints 267.17021276595744
  ]
  node [
    id 368
    label "106283"
    name "Mitchell"
    surname "Krueger"
    country "USA"
    hand "R"
    dateOfBirth 19940112.0
    tourneyNum 3
    avgRank 233.93617021276594
    avgPoints 233.74468085106383
  ]
  node [
    id 369
    label "122669"
    name "Juan Pablo"
    surname "Varillas Patino Samudio"
    country "PER"
    hand "R"
    dateOfBirth 19951006.0
    tourneyNum 2
    avgRank 399.531914893617
    avgPoints 104.59574468085107
  ]
  node [
    id 370
    label "206642"
    name "Yassir"
    surname "Kilani"
    country "MAR"
    hand "R"
    dateOfBirth 20000810.0
    tourneyNum 1
    avgRank 1865.7777777777778
    avgPoints 1.0
  ]
  node [
    id 371
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 29
    avgRank 39.255319148936174
    avgPoints 1440.4893617021276
  ]
  node [
    id 372
    label "106296"
    name "Gregoire"
    surname "Barrere"
    country "FRA"
    hand "R"
    dateOfBirth 19940216.0
    tourneyNum 4
    avgRank 244.53191489361703
    avgPoints 277.3829787234043
  ]
  node [
    id 373
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 24
    avgRank 20.148936170212767
    avgPoints 1880.6382978723404
  ]
  node [
    id 374
    label "104251"
    name "Ti"
    surname "Chen"
    country "TPE"
    hand "R"
    dateOfBirth 19831003.0
    tourneyNum 1
    avgRank 508.0851063829787
    avgPoints 74.19148936170212
  ]
  node [
    id 375
    label "104252"
    name "Florian"
    surname "Mayer"
    country "GER"
    hand "R"
    dateOfBirth 19831005.0
    tourneyNum 15
    avgRank 163.63829787234042
    avgPoints 471.5744680851064
  ]
  node [
    id 376
    label "200514"
    name "Jurij"
    surname "Rodionov"
    country "AUT"
    hand "L"
    dateOfBirth 19990516.0
    tourneyNum 1
    avgRank 359.17021276595744
    avgPoints 153.40425531914894
  ]
  node [
    id 377
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 23
    avgRank 32.48936170212766
    avgPoints 1327.2340425531916
  ]
  node [
    id 378
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 28
    avgRank 32.808510638297875
    avgPoints 1337.659574468085
  ]
  node [
    id 379
    label "106324"
    name "Vaclav"
    surname "Safranek"
    country "CZE"
    hand "R"
    dateOfBirth 19940520.0
    tourneyNum 2
    avgRank 270.8510638297872
    avgPoints 193.7872340425532
  ]
  node [
    id 380
    label "106325"
    name "Jabor"
    surname "Al Mutawa"
    country "QAT"
    hand "U"
    dateOfBirth 19940521.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 381
    label "106328"
    name "Christian"
    surname "Harrison"
    country "USA"
    hand "R"
    dateOfBirth 19940529.0
    tourneyNum 2
    avgRank 234.89361702127658
    avgPoints 234.91489361702128
  ]
  node [
    id 382
    label "106329"
    name "Thiago"
    surname "Monteiro"
    country "BRA"
    hand "L"
    dateOfBirth 19940531.0
    tourneyNum 10
    avgRank 121.42553191489361
    avgPoints 471.5744680851064
  ]
  node [
    id 383
    label "106333"
    name "Julien"
    surname "Cagnina"
    country "BEL"
    hand "R"
    dateOfBirth 19940604.0
    tourneyNum 1
    avgRank 453.5652173913044
    avgPoints 129.47826086956522
  ]
  node [
    id 384
    label "104291"
    name "Malek"
    surname "Jaziri"
    country "TUN"
    hand "R"
    dateOfBirth 19840120.0
    tourneyNum 23
    avgRank 69.72340425531915
    avgPoints 816.0425531914893
  ]
  node [
    id 385
    label "104297"
    name "Rogerio"
    surname "Dutra Silva"
    country "BRA"
    hand "R"
    dateOfBirth 19840203.0
    tourneyNum 11
    avgRank 135.19148936170214
    avgPoints 426.59574468085106
  ]
  node [
    id 386
    label "106348"
    name "Evgeny"
    surname "Karlovskiy"
    country "RUS"
    hand "R"
    dateOfBirth 19940807.0
    tourneyNum 1
    avgRank 276.17021276595744
    avgPoints 194.5744680851064
  ]
  node [
    id 387
    label "106353"
    name "Pedja"
    surname "Krstin"
    country "SRB"
    hand "R"
    dateOfBirth 19940903.0
    tourneyNum 2
    avgRank 204.46808510638297
    avgPoints 273.3191489361702
  ]
  node [
    id 388
    label "104308"
    name "Michal"
    surname "Przysiezny"
    country "POL"
    hand "R"
    dateOfBirth 19840216.0
    tourneyNum 1
    avgRank 1028.4468085106382
    avgPoints 16.23404255319149
  ]
  node [
    id 389
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 23
    avgRank 52.06382978723404
    avgPoints 967.7446808510638
  ]
  node [
    id 390
    label "106361"
    name "Adam"
    surname "Pavlasek"
    country "CZE"
    hand "R"
    dateOfBirth 19941008.0
    tourneyNum 2
    avgRank 201.5531914893617
    avgPoints 278.06382978723406
  ]
  node [
    id 391
    label "126845"
    name "Max"
    surname "Purcell"
    country "AUS"
    hand "U"
    dateOfBirth 19980403.0
    tourneyNum 1
    avgRank 254.08510638297872
    avgPoints 213.68085106382978
  ]
  node [
    id 392
    label "126846"
    name "Aleksandar"
    surname "Vukic"
    country "AUS"
    hand "R"
    dateOfBirth 19960406.0
    tourneyNum 1
    avgRank 456.5744680851064
    avgPoints 79.74468085106383
  ]
  node [
    id 393
    label "200574"
    name "Alexander"
    surname "Donski"
    country "CAN"
    hand "U"
    dateOfBirth 19980801.0
    tourneyNum 1
    avgRank 1552.6521739130435
    avgPoints 2.260869565217391
  ]
  node [
    id 394
    label "106368"
    name "Ramkumar"
    surname "Ramanathan"
    country "IND"
    hand "R"
    dateOfBirth 19941108.0
    tourneyNum 7
    avgRank 129.91489361702128
    avgPoints 438.1063829787234
  ]
  node [
    id 395
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 22
    avgRank 20.29787234042553
    avgPoints 1863.0212765957447
  ]
  node [
    id 396
    label "106391"
    name "Adrian"
    surname "Bodmer"
    country "SUI"
    hand "U"
    dateOfBirth 19950306.0
    tourneyNum 1
    avgRank 581.0212765957447
    avgPoints 53.91489361702128
  ]
  node [
    id 397
    label "106393"
    name "Frederico"
    surname "Ferreira Silva"
    country "POR"
    hand "L"
    dateOfBirth 19950318.0
    tourneyNum 1
    avgRank 313.27659574468083
    avgPoints 154.5531914893617
  ]
  node [
    id 398
    label "106396"
    name "Cheng Yu"
    surname "Yu"
    country "TPE"
    hand "U"
    dateOfBirth 19950402.0
    tourneyNum 1
    avgRank 788.9130434782609
    avgPoints 24.76086956521739
  ]
  node [
    id 399
    label "106397"
    name "Wishaya"
    surname "Trongcharoenchaikul"
    country "THA"
    hand "R"
    dateOfBirth 19950408.0
    tourneyNum 3
    avgRank 449.0851063829787
    avgPoints 81.57446808510639
  ]
  node [
    id 400
    label "106398"
    name "Pedro"
    surname "Cachin"
    country "ARG"
    hand "R"
    dateOfBirth 19950412.0
    tourneyNum 1
    avgRank 273.36170212765956
    avgPoints 191.31914893617022
  ]
  node [
    id 401
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 15
    avgRank 25.361702127659573
    avgPoints 1646.063829787234
  ]
  node [
    id 402
    label "200611"
    name "Elliot"
    surname "Benchetrit"
    country "FRA"
    hand "U"
    dateOfBirth 19981002.0
    tourneyNum 1
    avgRank 324.6170212765957
    avgPoints 146.6595744680851
  ]
  node [
    id 403
    label "200615"
    name "Alexei"
    surname "Popyrin"
    country "AUS"
    hand "R"
    dateOfBirth 19990805.0
    tourneyNum 4
    avgRank 322.3191489361702
    avgPoints 195.5531914893617
  ]
  node [
    id 404
    label "106410"
    name "Jorge Brian"
    surname "Panta Herreros"
    country "PER"
    hand "U"
    dateOfBirth 19950722.0
    tourneyNum 1
    avgRank 898.6739130434783
    avgPoints 16.195652173913043
  ]
  node [
    id 405
    label "200619"
    name "Sebastian"
    surname "Arcila"
    country "PUR"
    hand "U"
    dateOfBirth 19980626.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 406
    label "200620"
    name "Ignacio"
    surname "Garcia"
    country "PUR"
    hand "U"
    dateOfBirth 19990518.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 407
    label "106412"
    name "Lucas"
    surname "Gomez"
    country "MEX"
    hand "U"
    dateOfBirth 19950811.0
    tourneyNum 3
    avgRank 750.1276595744681
    avgPoints 27.70212765957447
  ]
  node [
    id 408
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 13
    avgRank 194.61702127659575
    avgPoints 367.9574468085106
  ]
  node [
    id 409
    label "206768"
    name "Rafael Eduardo"
    surname "Gonzalez Retana"
    country "ESA"
    hand "R"
    dateOfBirth 20000126.0
    tourneyNum 1
    avgRank 2272.0
    avgPoints 0.0
  ]
  node [
    id 410
    label "200624"
    name "Sebastian"
    surname "Korda"
    country "USA"
    hand "U"
    dateOfBirth 20000705.0
    tourneyNum 1
    avgRank 733.1276595744681
    avgPoints 32.702127659574465
  ]
  node [
    id 411
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 27
    avgRank 43.06382978723404
    avgPoints 1241.4893617021276
  ]
  node [
    id 412
    label "106423"
    name "Thanasi"
    surname "Kokkinakis"
    country "AUS"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 6
    avgRank 176.7659574468085
    avgPoints 322.48936170212767
  ]
  node [
    id 413
    label "106426"
    name "Christian"
    surname "Garin"
    country "CHI"
    hand "R"
    dateOfBirth 19960530.0
    tourneyNum 2
    avgRank 176.82978723404256
    avgPoints 378.3829787234043
  ]
  node [
    id 414
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 24
    avgRank 27.25531914893617
    avgPoints 1685.7659574468084
  ]
  node [
    id 415
    label "110536"
    name "Kevin"
    surname "King"
    country "USA"
    hand "U"
    dateOfBirth 19910228.0
    tourneyNum 2
    avgRank 216.27659574468086
    avgPoints 265.74468085106383
  ]
  node [
    id 416
    label "120792"
    name "M Abid Ali Khan"
    surname "Akbar"
    country "PAK"
    hand "U"
    dateOfBirth 19900722.0
    tourneyNum 1
    avgRank 1806.695652173913
    avgPoints 1.0434782608695652
  ]
  node [
    id 417
    label "126939"
    name "Khumoun"
    surname "Sultanov"
    country "UZB"
    hand "U"
    dateOfBirth 19981027.0
    tourneyNum 1
    avgRank 682.5531914893617
    avgPoints 40.48936170212766
  ]
  node [
    id 418
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 2
    avgRank 224.27659574468086
    avgPoints 248.4255319148936
  ]
  edge [
    source 0
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 332
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 92
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 376
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 322
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 269
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 5
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 87
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 99
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 272
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 89
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 155
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 269
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 330
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 94
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 154
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 199
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 123
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 345
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 110
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 268
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 6
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 232
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 131
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 219
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 247
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 390
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 415
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 99
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 172
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 130
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 9
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 287
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 9
    target 38
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 99
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 97
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 9
    target 32
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 9
    target 336
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 82
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 209
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 120
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 227
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 212
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 131
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 181
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 94
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 95
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 172
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 87
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 319
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 257
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 120
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 290
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 349
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 347
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 334
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 360
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 208
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 285
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 204
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 213
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 281
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 340
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 361
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 223
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 111
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 118
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 259
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 310
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 87
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 94
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 385
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 82
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 139
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 76
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 30
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 226
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 30
    target 144
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 30
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 157
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 30
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 237
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 414
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 269
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 30
    target 131
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 371
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 30
    target 377
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 358
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 30
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 182
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 255
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 50
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 30
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 30
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 244
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 363
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 105
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 305
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 124
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 75
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 312
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 32
    target 401
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 291
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 32
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 411
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 414
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 156
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 377
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 395
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 99
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 131
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 94
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 384
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 371
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 46
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 92
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 282
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 87
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 50
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 32
    target 403
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 288
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 297
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 33
    target 99
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 131
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 33
    target 358
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 33
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 371
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 50
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 97
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 33
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 371
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 206
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 227
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 42
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 34
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 395
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 112
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 34
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 375
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 82
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 289
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 395
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 35
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 371
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 189
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 414
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 133
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 226
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 269
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 112
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 212
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 46
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 35
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 35
    target 411
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 110
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 92
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 94
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 99
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 266
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 273
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 38
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 99
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 297
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 38
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 402
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 247
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 378
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 38
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 133
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 245
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 284
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 168
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 159
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 276
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 121
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 87
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 377
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 161
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 76
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 250
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 371
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 180
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 51
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 93
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 42
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 115
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 50
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 42
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 378
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 157
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 45
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 372
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 112
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 87
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 205
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 96
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 345
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 109
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 180
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 211
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 46
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 308
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 189
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 110
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 248
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 250
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 395
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 48
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 378
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 48
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 57
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 49
    target 112
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 202
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 49
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 51
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 49
    target 378
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 49
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 330
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 49
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 82
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 112
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 414
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 144
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 50
    target 395
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 87
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 117
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 232
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 93
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 99
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 50
    target 240
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 131
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 92
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 255
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 50
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 106
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 327
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 51
    target 95
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 240
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 51
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 378
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 360
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 260
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 110
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 124
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 120
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 99
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 109
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 97
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 268
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 51
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 56
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 140
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 112
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 57
    target 131
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 57
    target 97
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 76
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 57
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 308
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 57
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 202
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 57
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 79
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 57
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 157
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 146
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 399
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 263
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 63
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 93
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 63
    target 222
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 63
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 160
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 63
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 99
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 63
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 407
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 297
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 63
    target 131
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 63
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 184
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 116
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 66
    target 87
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 173
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 299
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 67
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 67
    target 201
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 132
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 107
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 68
    target 409
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 70
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 226
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 71
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 100
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 75
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 115
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 79
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 71
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 377
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 71
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 179
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 92
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 71
    target 403
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 87
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 73
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 231
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 107
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 146
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 93
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 74
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 240
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 75
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 255
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 75
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 137
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 123
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 222
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 75
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 233
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 154
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 75
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 396
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 99
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 372
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 277
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 82
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 76
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 120
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 157
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 76
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 76
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 77
    target 100
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 203
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 400
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 78
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 403
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 82
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 167
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 79
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 79
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 185
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 80
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 80
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 149
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 81
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 133
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 82
    target 367
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 270
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 333
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 385
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 202
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 321
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 414
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 193
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 82
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 82
    target 97
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 179
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 82
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 227
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 131
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 82
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 82
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 83
    target 292
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 85
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 243
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 166
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 192
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 86
    target 257
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 148
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 86
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 289
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 86
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 143
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 87
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 97
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 414
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 87
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 373
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 87
    target 133
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 87
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 211
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 87
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 377
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 87
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 131
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 121
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 87
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 107
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 87
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 88
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 89
    target 106
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 89
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 89
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 90
    target 369
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 91
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 92
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 162
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 133
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 92
    target 93
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 92
    target 111
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 92
    target 137
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 167
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 93
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 96
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 143
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 93
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 161
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 413
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 103
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 306
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 93
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 389
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 93
    target 386
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 93
    target 235
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 93
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 94
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 94
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 94
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 94
    target 304
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 94
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 131
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 126
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 117
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 110
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 95
    target 156
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 95
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 95
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 181
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 95
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 96
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 131
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 97
    target 291
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 97
    target 226
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 97
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 212
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 97
    target 255
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 272
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 97
    target 288
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 97
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 211
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 97
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 99
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 352
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 316
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 97
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 97
    target 414
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 97
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 98
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 98
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 394
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 227
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 99
    target 170
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 99
    target 106
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 287
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 99
    target 237
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 99
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 377
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 99
    target 216
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 378
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 99
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 144
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 99
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 154
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 99
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 205
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 99
    target 131
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 99
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 99
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 100
    target 297
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 100
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 101
    target 293
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 102
    target 286
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 102
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 133
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 103
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 318
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 103
    target 264
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 103
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 104
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 105
    target 398
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 105
    target 315
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 105
    target 176
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 105
    target 399
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 128
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 353
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 389
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 106
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 211
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 196
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 236
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 106
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 411
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 106
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 324
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 125
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 148
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 124
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 170
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 106
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 106
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 107
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 107
    target 171
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 202
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 107
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 107
    target 212
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 107
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 107
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 107
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 108
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 109
    target 318
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 109
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 109
    target 112
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 134
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 326
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 175
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 358
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 110
    target 121
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 154
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 110
    target 319
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 110
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 111
    target 321
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 111
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 268
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 112
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 112
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 117
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 373
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 112
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 368
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 202
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 112
    target 237
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 112
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 144
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 112
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 255
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 112
    target 118
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 112
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 112
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 113
    target 235
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 114
    target 186
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 269
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 115
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 288
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 115
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 181
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 115
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 115
    target 185
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 243
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 116
    target 229
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 116
    target 188
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 403
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 117
    target 354
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 117
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 124
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 130
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 117
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 156
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 117
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 329
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 117
    target 255
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 117
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 118
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 119
    target 183
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 120
    target 144
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 120
    target 412
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 120
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 120
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 120
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 126
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 273
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 121
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 123
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 189
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 224
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 121
    target 125
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 121
    target 222
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 122
    target 264
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 393
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 263
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 123
    target 236
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 123
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 213
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 358
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 123
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 123
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 123
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 304
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 124
    target 230
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 143
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 407
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 126
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 268
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 124
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 202
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 124
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 124
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 124
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 125
    target 255
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 125
    target 154
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 199
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 237
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 125
    target 157
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 205
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 125
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 130
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 283
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 125
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 125
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 285
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 126
    target 133
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 295
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 156
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 126
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 126
    target 128
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 127
    target 337
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 394
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 128
    target 157
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 128
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 129
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 129
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 129
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 129
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 129
    target 233
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 268
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 130
    target 313
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 130
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 144
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 167
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 130
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 385
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 193
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 372
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 330
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 130
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 130
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 130
    target 170
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 291
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 131
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 211
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 131
    target 377
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 131
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 373
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 131
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 412
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 144
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 131
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 272
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 131
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 206
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 131
    target 143
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 236
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 131
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 250
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 132
    target 160
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 132
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 263
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 133
    target 280
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 385
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 202
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 133
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 133
    target 371
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 134
    target 173
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 135
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 136
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 136
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 193
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 137
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 137
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 138
    target 417
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 140
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 140
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 141
    target 374
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 142
    target 305
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 143
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 299
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 387
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 411
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 143
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 237
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 143
    target 171
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 160
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 156
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 381
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 394
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 143
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 143
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 411
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 144
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 144
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 377
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 144
    target 194
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 358
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 144
    target 381
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 250
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 144
    target 255
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 144
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 145
    target 152
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 146
    target 273
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 146
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 146
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 147
    target 404
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 148
    target 265
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 149
    target 412
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 149
    target 359
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 149
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 149
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 150
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 151
    target 362
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 151
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 152
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 153
    target 387
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 174
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 206
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 226
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 154
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 259
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 205
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 154
    target 238
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 154
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 418
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 255
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 156
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 310
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 156
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 157
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 365
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 211
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 156
    target 289
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 156
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 286
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 288
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 157
    target 272
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 157
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 244
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 167
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 157
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 157
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 158
    target 344
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 159
    target 207
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 159
    target 225
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 259
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 180
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 160
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 378
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 160
    target 210
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 413
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 221
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 172
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 160
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 160
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 162
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 163
    target 314
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 164
    target 169
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 165
    target 416
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 323
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 261
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 325
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 167
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 167
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 168
    target 418
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 414
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 170
    target 233
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 170
    target 180
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 170
    target 268
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 170
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 170
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 170
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 170
    target 172
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 170
    target 212
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 171
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 171
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 171
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 171
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 358
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 172
    target 202
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 196
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 324
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 172
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 269
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 172
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 172
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 172
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 173
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 203
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 175
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 175
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 176
    target 305
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 177
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 178
    target 315
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 291
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 180
    target 232
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 180
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 222
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 180
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 180
    target 224
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 299
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 180
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 180
    target 238
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 180
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 180
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 181
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 181
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 182
    target 303
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 182
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 182
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 183
    target 388
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 183
    target 199
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 185
    target 199
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 187
    target 217
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 188
    target 328
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 188
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 188
    target 325
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 189
    target 192
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 189
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 190
    target 192
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 191
    target 199
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 198
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 268
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 192
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 209
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 192
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 192
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 210
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 359
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 193
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 297
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 193
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 193
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 227
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 193
    target 263
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 195
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 197
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 199
    target 205
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 200
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 259
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 333
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 202
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 297
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 202
    target 209
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 326
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 237
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 202
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 202
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 203
    target 298
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 203
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 204
    target 262
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 204
    target 352
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 254
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 236
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 205
    target 288
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 205
    target 249
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 318
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 205
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 205
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 222
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 206
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 338
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 225
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 248
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 209
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 209
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 210
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 210
    target 234
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 210
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 210
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 210
    target 408
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 211
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 211
    target 263
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 211
    target 232
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 211
    target 371
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 211
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 211
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 211
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 226
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 212
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 211
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 211
    target 274
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 233
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 234
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 242
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 236
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 255
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 212
    target 294
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 367
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 226
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 215
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 212
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 212
    target 244
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 213
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 213
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 214
    target 406
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 214
    target 405
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 216
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 216
    target 396
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 217
    target 364
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 218
    target 296
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 219
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 220
    target 361
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 221
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 221
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 308
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 230
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 227
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 304
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 222
    target 241
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 412
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 343
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 320
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 283
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 222
    target 252
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 222
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 223
    target 369
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 224
    target 323
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 410
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 275
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 232
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 263
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 226
    target 282
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 238
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 226
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 294
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 227
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 232
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 312
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 227
    target 368
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 408
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 227
    target 276
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 227
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 228
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 229
    target 253
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 229
    target 366
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 229
    target 325
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 230
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 232
    target 395
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 232
    target 401
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 232
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 232
    target 255
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 232
    target 384
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 232
    target 378
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 232
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 232
    target 237
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 232
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 232
    target 240
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 272
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 241
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 233
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 233
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 250
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 233
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 235
    target 309
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 235
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 263
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 294
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 306
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 240
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 390
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 297
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 408
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 236
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 236
    target 401
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 414
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 237
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 354
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 306
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 237
    target 411
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 237
    target 378
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 237
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 313
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 237
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 237
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 291
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 318
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 257
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 238
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 238
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 239
    target 334
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 239
    target 266
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 240
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 385
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 269
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 240
    target 350
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 241
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 241
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 243
    target 325
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 246
    target 363
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 335
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 247
    target 309
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 248
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 249
    target 269
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 250
    target 291
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 250
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 250
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 251
    target 265
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 252
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 254
    target 331
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 268
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 272
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 255
    target 414
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 255
    target 412
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 255
    target 401
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 255
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 255
    target 411
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 255
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 256
    target 379
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 257
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 257
    target 270
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 257
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 258
    target 308
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 259
    target 380
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 259
    target 341
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 259
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 260
    target 286
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 262
    target 340
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 262
    target 407
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 392
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 297
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 263
    target 312
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 263
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 412
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 304
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 401
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 263
    target 342
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 263
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 263
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 263
    target 272
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 264
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 264
    target 343
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 264
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 267
    target 325
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 298
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 268
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 268
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 317
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 306
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 268
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 268
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 268
    target 275
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 268
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 268
    target 288
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 414
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 269
    target 395
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 269
    target 330
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 269
    target 273
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 365
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 269
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 304
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 269
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 269
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 270
    target 339
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 271
    target 346
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 272
    target 373
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 272
    target 368
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 272
    target 367
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 272
    target 287
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 272
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 272
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 272
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 273
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 342
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 273
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 274
    target 333
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 327
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 275
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 275
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 288
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 275
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 276
    target 412
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 277
    target 298
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 278
    target 360
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 279
    target 370
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 294
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 282
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 282
    target 285
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 283
    target 356
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 283
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 283
    target 306
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 284
    target 287
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 285
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 285
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 285
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 287
    target 312
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 287
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 287
    target 297
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 287
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 287
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 287
    target 338
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 287
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 287
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 288
    target 312
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 288
    target 317
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 288
    target 298
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 288
    target 306
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 288
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 288
    target 299
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 288
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 288
    target 313
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 288
    target 371
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 288
    target 394
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 288
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 290
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 291
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 291
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 291
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 291
    target 318
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 291
    target 365
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 294
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 294
    target 304
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 294
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 294
    target 319
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 295
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 297
    target 318
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 297
    target 378
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 297
    target 413
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 297
    target 390
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 297
    target 348
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 297
    target 395
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 297
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 298
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 298
    target 385
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 298
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 299
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 299
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 299
    target 371
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 299
    target 373
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 299
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 299
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 299
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 299
    target 408
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 299
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 300
    target 302
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 300
    target 311
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 301
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 301
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 302
    target 366
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 303
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 303
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 304
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 304
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 304
    target 411
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 304
    target 330
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 304
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 304
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 305
    target 399
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 306
    target 389
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 306
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 306
    target 312
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 306
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 307
    target 361
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 308
    target 389
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 308
    target 354
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 308
    target 355
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 308
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 308
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 309
    target 415
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 309
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 309
    target 316
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 310
    target 351
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 311
    target 366
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 312
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 312
    target 411
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 312
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 312
    target 395
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 312
    target 327
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 312
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 312
    target 371
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 312
    target 377
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 313
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 313
    target 365
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 313
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 317
    target 375
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 317
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 318
    target 337
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 319
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 321
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 323
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 323
    target 381
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 325
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 326
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 326
    target 397
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 327
    target 330
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 327
    target 385
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 359
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 327
    target 333
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 327
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 327
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 327
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 330
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 330
    target 350
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 358
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 330
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 330
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 330
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 351
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 371
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 358
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 333
    target 368
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 333
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 334
    target 399
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 335
    target 348
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 337
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 347
    target 399
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 348
    target 379
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 348
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 348
    target 382
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 348
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 348
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 348
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 348
    target 355
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 348
    target 394
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 351
    target 367
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 351
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 354
    target 384
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 354
    target 389
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 354
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 354
    target 375
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 354
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 355
    target 383
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 357
    target 382
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 357
    target 362
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 358
    target 371
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 358
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 358
    target 414
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 358
    target 359
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 358
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 359
    target 378
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 359
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 359
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 361
    target 406
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 361
    target 369
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 363
    target 399
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 365
    target 373
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 371
    target 375
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 371
    target 377
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 371
    target 384
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 371
    target 411
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 371
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 371
    target 373
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 371
    target 372
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 373
    target 389
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 373
    target 395
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 373
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 373
    target 377
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 373
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 375
    target 389
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 375
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 377
    target 411
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 377
    target 408
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 377
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 377
    target 378
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 378
    target 382
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 378
    target 412
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 378
    target 408
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 378
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 378
    target 395
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 378
    target 384
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 384
    target 395
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 384
    target 391
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 385
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 389
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 389
    target 395
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 389
    target 411
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 389
    target 414
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 395
    target 411
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 395
    target 401
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 401
    target 414
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 401
    target 408
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 411
    target 412
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 411
    target 414
    weight 4
    lowerId 1
    higherId 3
  ]
]
