graph [
  node [
    id 0
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 15
    avgRank 5.595744680851064
    avgPoints 4739.04255319149
  ]
  node [
    id 1
    label "106045"
    name "Denis"
    surname "Kudla"
    country "USA"
    hand "R"
    dateOfBirth 19920817.0
    tourneyNum 14
    avgRank 101.91489361702128
    avgPoints 614.1914893617021
  ]
  node [
    id 2
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 18
    avgRank 25.97872340425532
    avgPoints 1604.9148936170213
  ]
  node [
    id 3
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 22
    avgRank 60.61702127659574
    avgPoints 881.2765957446809
  ]
  node [
    id 4
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 29
    avgRank 50.87234042553192
    avgPoints 990.0
  ]
  node [
    id 5
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 25
    avgRank 51.12765957446808
    avgPoints 974.936170212766
  ]
  node [
    id 6
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 22
    avgRank 20.19148936170213
    avgPoints 1868.6170212765958
  ]
  node [
    id 7
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 23
    avgRank 32.48936170212766
    avgPoints 1327.2340425531916
  ]
  node [
    id 8
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 25
    avgRank 57.5531914893617
    avgPoints 896.6170212765958
  ]
  node [
    id 9
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 25
    avgRank 48.59574468085106
    avgPoints 1016.5106382978723
  ]
  node [
    id 10
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 24
    avgRank 63.829787234042556
    avgPoints 828.7659574468086
  ]
  node [
    id 11
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 25
    avgRank 66.59574468085107
    avgPoints 830.0851063829788
  ]
  node [
    id 12
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 22
    avgRank 5.446808510638298
    avgPoints 4522.446808510638
  ]
  node [
    id 13
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 17
    avgRank 11.914893617021276
    avgPoints 2930.1063829787236
  ]
  node [
    id 14
    label "105657"
    name "Marius"
    surname "Copil"
    country "ROU"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 23
    avgRank 82.38297872340425
    avgPoints 689.0
  ]
  node [
    id 15
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 26
    avgRank 29.21276595744681
    avgPoints 1468.723404255319
  ]
  node [
    id 16
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 20
    avgRank 7.702127659574468
    avgPoints 3760.0
  ]
  node [
    id 17
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 25
    avgRank 7.276595744680851
    avgPoints 3811.3829787234044
  ]
  node [
    id 18
    label "105074"
    name "Ruben"
    surname "Bemelmans"
    country "BEL"
    hand "L"
    dateOfBirth 19880114.0
    tourneyNum 16
    avgRank 115.76595744680851
    avgPoints 496.1276595744681
  ]
  node [
    id 19
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 20
    avgRank 19.80851063829787
    avgPoints 2191.595744680851
  ]
  node [
    id 20
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 23
    avgRank 52.06382978723404
    avgPoints 967.7446808510638
  ]
  node [
    id 21
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 20
    avgRank 64.74468085106383
    avgPoints 844.1063829787234
  ]
  node [
    id 22
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 18
    avgRank 80.7872340425532
    avgPoints 696.5531914893617
  ]
  node [
    id 23
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 19
    avgRank 8.23404255319149
    avgPoints 3956.808510638298
  ]
  node [
    id 24
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 26
    avgRank 49.723404255319146
    avgPoints 1035.8510638297873
  ]
  node [
    id 25
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 17
    avgRank 43.46808510638298
    avgPoints 1250.8936170212767
  ]
  node [
    id 26
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 27
    avgRank 16.72340425531915
    avgPoints 2073.191489361702
  ]
  node [
    id 27
    label "104898"
    name "Robin"
    surname "Haase"
    country "NED"
    hand "R"
    dateOfBirth 19870406.0
    tourneyNum 30
    avgRank 44.97872340425532
    avgPoints 1076.595744680851
  ]
  node [
    id 28
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 15
    avgRank 25.361702127659573
    avgPoints 1646.063829787234
  ]
  node [
    id 29
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 25
    avgRank 53.638297872340424
    avgPoints 943.7234042553191
  ]
  node [
    id 30
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 17
    avgRank 77.72340425531915
    avgPoints 1223.8297872340424
  ]
  node [
    id 31
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 19
    avgRank 63.40425531914894
    avgPoints 904.468085106383
  ]
  node [
    id 32
    label "104607"
    name "Tomas"
    surname "Berdych"
    country "CZE"
    hand "R"
    dateOfBirth 19850917.0
    tourneyNum 12
    avgRank 44.91489361702128
    avgPoints 1391.7021276595744
  ]
  node [
    id 33
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 23
    avgRank 4.042553191489362
    avgPoints 5277.234042553191
  ]
  node [
    id 34
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 13
    avgRank 2.127659574468085
    avgPoints 7868.936170212766
  ]
  node [
    id 35
    label "106423"
    name "Thanasi"
    surname "Kokkinakis"
    country "AUS"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 6
    avgRank 176.7659574468085
    avgPoints 322.48936170212767
  ]
  node [
    id 36
    label "105041"
    name "Lukas"
    surname "Lacko"
    country "SVK"
    hand "R"
    dateOfBirth 19871103.0
    tourneyNum 16
    avgRank 93.36170212765957
    avgPoints 623.1276595744681
  ]
  node [
    id 37
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 23
    avgRank 61.361702127659576
    avgPoints 881.8085106382979
  ]
  node [
    id 38
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 19
    avgRank 24.659574468085108
    avgPoints 1639.5744680851064
  ]
  node [
    id 39
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 16
    avgRank 9.404255319148936
    avgPoints 4464.68085106383
  ]
  node [
    id 40
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 27
    avgRank 43.06382978723404
    avgPoints 1241.4893617021276
  ]
  node [
    id 41
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 24
    avgRank 27.25531914893617
    avgPoints 1685.7659574468084
  ]
  node [
    id 42
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 30
    avgRank 31.361702127659573
    avgPoints 1416.8085106382978
  ]
  node [
    id 43
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 30
    avgRank 57.08510638297872
    avgPoints 918.1063829787234
  ]
  node [
    id 44
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 13
    avgRank 194.61702127659575
    avgPoints 367.9574468085106
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 13
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 2
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 1
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 36
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 2
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 7
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 5
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 4
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 43
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 4
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 15
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 30
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 5
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 26
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 6
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 41
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 6
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 13
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 7
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 12
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 34
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 8
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 43
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 17
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 17
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 39
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 12
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 14
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 12
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 23
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 27
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 13
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 21
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 37
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 15
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 19
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 16
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 17
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 16
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 16
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 37
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 21
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 24
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 19
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 17
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 39
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 19
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 34
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 19
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 21
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 24
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 28
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 33
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 41
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 34
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 34
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 37
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 37
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 39
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 40
    target 41
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 40
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
]
