graph [
  node [
    id 0
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 28
    avgRank 54.829787234042556
    avgPoints 925.5106382978723
  ]
  node [
    id 1
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 29
    avgRank 52.42553191489362
    avgPoints 1126.4255319148936
  ]
  node [
    id 2
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 15
    avgRank 5.595744680851064
    avgPoints 4739.04255319149
  ]
  node [
    id 3
    label "105806"
    name "Mirza"
    surname "Basic"
    country "BIH"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 20
    avgRank 87.85106382978724
    avgPoints 659.3191489361702
  ]
  node [
    id 4
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 18
    avgRank 25.97872340425532
    avgPoints 1604.9148936170213
  ]
  node [
    id 5
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 29
    avgRank 50.87234042553192
    avgPoints 990.0
  ]
  node [
    id 6
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 25
    avgRank 51.12765957446808
    avgPoints 974.936170212766
  ]
  node [
    id 7
    label "105166"
    name "Peter"
    surname "Polansky"
    country "CAN"
    hand "R"
    dateOfBirth 19880615.0
    tourneyNum 11
    avgRank 125.29787234042553
    avgPoints 452.63829787234044
  ]
  node [
    id 8
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 22
    avgRank 20.19148936170213
    avgPoints 1868.6170212765958
  ]
  node [
    id 9
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 21
    avgRank 37.57446808510638
    avgPoints 1218.936170212766
  ]
  node [
    id 10
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 30
    avgRank 43.02127659574468
    avgPoints 1220.7446808510638
  ]
  node [
    id 11
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 25
    avgRank 66.59574468085107
    avgPoints 830.0851063829788
  ]
  node [
    id 12
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 22
    avgRank 5.446808510638298
    avgPoints 4522.446808510638
  ]
  node [
    id 13
    label "106121"
    name "Taro"
    surname "Daniel"
    country "JPN"
    hand "R"
    dateOfBirth 19930127.0
    tourneyNum 20
    avgRank 87.08510638297872
    avgPoints 656.8085106382979
  ]
  node [
    id 14
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 25
    avgRank 50.61702127659574
    avgPoints 1287.5106382978724
  ]
  node [
    id 15
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 26
    avgRank 41.06382978723404
    avgPoints 1124.3617021276596
  ]
  node [
    id 16
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 28
    avgRank 32.808510638297875
    avgPoints 1337.659574468085
  ]
  node [
    id 17
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 26
    avgRank 29.21276595744681
    avgPoints 1468.723404255319
  ]
  node [
    id 18
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 20
    avgRank 7.702127659574468
    avgPoints 3760.0
  ]
  node [
    id 19
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 25
    avgRank 7.276595744680851
    avgPoints 3811.3829787234044
  ]
  node [
    id 20
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 20
    avgRank 19.80851063829787
    avgPoints 2191.595744680851
  ]
  node [
    id 21
    label "144719"
    name "Jaume"
    surname "Munar"
    country "ESP"
    hand "R"
    dateOfBirth 19970505.0
    tourneyNum 11
    avgRank 122.2127659574468
    avgPoints 523.936170212766
  ]
  node [
    id 22
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 19
    avgRank 8.23404255319149
    avgPoints 3956.808510638298
  ]
  node [
    id 23
    label "105238"
    name "Alexandr"
    surname "Dolgopolov"
    country "UKR"
    hand "R"
    dateOfBirth 19881107.0
    tourneyNum 5
    avgRank 126.48936170212765
    avgPoints 706.6382978723404
  ]
  node [
    id 24
    label "104547"
    name "Horacio"
    surname "Zeballos"
    country "ARG"
    hand "L"
    dateOfBirth 19850427.0
    tourneyNum 15
    avgRank 124.76595744680851
    avgPoints 511.9148936170213
  ]
  node [
    id 25
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 32
    avgRank 34.04255319148936
    avgPoints 1336.4893617021276
  ]
  node [
    id 26
    label "105385"
    name "Donald"
    surname "Young"
    country "USA"
    hand "L"
    dateOfBirth 19890723.0
    tourneyNum 12
    avgRank 185.93617021276594
    avgPoints 372.0425531914894
  ]
  node [
    id 27
    label "104297"
    name "Rogerio"
    surname "Dutra Silva"
    country "BRA"
    hand "R"
    dateOfBirth 19840203.0
    tourneyNum 11
    avgRank 135.19148936170214
    avgPoints 426.59574468085106
  ]
  node [
    id 28
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 24
    avgRank 11.319148936170214
    avgPoints 3030.744680851064
  ]
  node [
    id 29
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 19
    avgRank 63.40425531914894
    avgPoints 904.468085106383
  ]
  node [
    id 30
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 29
    avgRank 39.255319148936174
    avgPoints 1440.4893617021276
  ]
  node [
    id 31
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 23
    avgRank 4.042553191489362
    avgPoints 5277.234042553191
  ]
  node [
    id 32
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 25
    avgRank 31.148936170212767
    avgPoints 1597.9787234042553
  ]
  node [
    id 33
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 13
    avgRank 2.127659574468085
    avgPoints 7868.936170212766
  ]
  node [
    id 34
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 23
    avgRank 61.361702127659576
    avgPoints 881.8085106382979
  ]
  node [
    id 35
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 22
    avgRank 20.29787234042553
    avgPoints 1863.0212765957447
  ]
  node [
    id 36
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 16
    avgRank 9.404255319148936
    avgPoints 4464.68085106383
  ]
  node [
    id 37
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 10
    avgRank 1.3191489361702127
    avgPoints 8804.36170212766
  ]
  node [
    id 38
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 19
    avgRank 24.659574468085108
    avgPoints 1639.5744680851064
  ]
  node [
    id 39
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 24
    avgRank 27.25531914893617
    avgPoints 1685.7659574468084
  ]
  node [
    id 40
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 30
    avgRank 31.361702127659573
    avgPoints 1416.8085106382978
  ]
  node [
    id 41
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 20
    avgRank 60.255319148936174
    avgPoints 870.8510638297872
  ]
  node [
    id 42
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 11
    avgRank 96.14893617021276
    avgPoints 669.1489361702128
  ]
  edge [
    source 0
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 4
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 3
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 1
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 18
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 11
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 2
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 32
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 2
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 28
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 2
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 10
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 37
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 2
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 6
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 17
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 20
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 14
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 8
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 19
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 24
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 20
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 36
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 12
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 29
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 40
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 15
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 22
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 16
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 34
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 17
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 20
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 18
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 30
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 19
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 36
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 18
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 30
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 19
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 37
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 19
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 20
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 19
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 36
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 20
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 33
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 20
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 22
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 37
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 31
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 28
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 28
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 34
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 37
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 30
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 39
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 37
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 31
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 36
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 31
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 37
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 32
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 39
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 33
    target 38
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 36
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 34
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 36
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 36
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 40
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 36
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 36
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
]
