graph [
  node [
    id 0
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 28
    avgRank 54.829787234042556
    avgPoints 925.5106382978723
  ]
  node [
    id 1
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 15
    avgRank 5.595744680851064
    avgPoints 4739.04255319149
  ]
  node [
    id 2
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 29
    avgRank 52.42553191489362
    avgPoints 1126.4255319148936
  ]
  node [
    id 3
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 22
    avgRank 15.872340425531915
    avgPoints 2140.744680851064
  ]
  node [
    id 4
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 20
    avgRank 81.29787234042553
    avgPoints 739.2127659574468
  ]
  node [
    id 5
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 21
    avgRank 71.57446808510639
    avgPoints 769.5957446808511
  ]
  node [
    id 6
    label "105806"
    name "Mirza"
    surname "Basic"
    country "BIH"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 20
    avgRank 87.85106382978724
    avgPoints 659.3191489361702
  ]
  node [
    id 7
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 18
    avgRank 25.97872340425532
    avgPoints 1604.9148936170213
  ]
  node [
    id 8
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 22
    avgRank 60.61702127659574
    avgPoints 881.2765957446809
  ]
  node [
    id 9
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 29
    avgRank 50.87234042553192
    avgPoints 990.0
  ]
  node [
    id 10
    label "111815"
    name "Cameron"
    surname "Norrie"
    country "GBR"
    hand "L"
    dateOfBirth 19950823.0
    tourneyNum 18
    avgRank 91.1063829787234
    avgPoints 644.5744680851063
  ]
  node [
    id 11
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 25
    avgRank 51.12765957446808
    avgPoints 974.936170212766
  ]
  node [
    id 12
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 22
    avgRank 20.19148936170213
    avgPoints 1868.6170212765958
  ]
  node [
    id 13
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 23
    avgRank 32.48936170212766
    avgPoints 1327.2340425531916
  ]
  node [
    id 14
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 21
    avgRank 37.57446808510638
    avgPoints 1218.936170212766
  ]
  node [
    id 15
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 25
    avgRank 48.59574468085106
    avgPoints 1016.5106382978723
  ]
  node [
    id 16
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 30
    avgRank 43.02127659574468
    avgPoints 1220.7446808510638
  ]
  node [
    id 17
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 25
    avgRank 57.5531914893617
    avgPoints 896.6170212765958
  ]
  node [
    id 18
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 24
    avgRank 63.829787234042556
    avgPoints 828.7659574468086
  ]
  node [
    id 19
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 25
    avgRank 66.59574468085107
    avgPoints 830.0851063829788
  ]
  node [
    id 20
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 22
    avgRank 5.446808510638298
    avgPoints 4522.446808510638
  ]
  node [
    id 21
    label "109739"
    name "Maximilian"
    surname "Marterer"
    country "GER"
    hand "L"
    dateOfBirth 19950615.0
    tourneyNum 23
    avgRank 66.63829787234043
    avgPoints 808.1063829787234
  ]
  node [
    id 22
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 21
    avgRank 52.276595744680854
    avgPoints 1041.1702127659576
  ]
  node [
    id 23
    label "106065"
    name "Marco"
    surname "Cecchinato"
    country "ITA"
    hand "R"
    dateOfBirth 19920930.0
    tourneyNum 25
    avgRank 50.61702127659574
    avgPoints 1287.5106382978724
  ]
  node [
    id 24
    label "106121"
    name "Taro"
    surname "Daniel"
    country "JPN"
    hand "R"
    dateOfBirth 19930127.0
    tourneyNum 20
    avgRank 87.08510638297872
    avgPoints 656.8085106382979
  ]
  node [
    id 25
    label "105449"
    name "Steve"
    surname "Johnson"
    country "USA"
    hand "R"
    dateOfBirth 19891224.0
    tourneyNum 26
    avgRank 41.06382978723404
    avgPoints 1124.3617021276596
  ]
  node [
    id 26
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 28
    avgRank 32.808510638297875
    avgPoints 1337.659574468085
  ]
  node [
    id 27
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 17
    avgRank 11.914893617021276
    avgPoints 2930.1063829787236
  ]
  node [
    id 28
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 19
    avgRank 66.17021276595744
    avgPoints 825.7872340425532
  ]
  node [
    id 29
    label "105657"
    name "Marius"
    surname "Copil"
    country "ROU"
    hand "R"
    dateOfBirth 19901017.0
    tourneyNum 23
    avgRank 82.38297872340425
    avgPoints 689.0
  ]
  node [
    id 30
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 26
    avgRank 29.21276595744681
    avgPoints 1468.723404255319
  ]
  node [
    id 31
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 20
    avgRank 7.702127659574468
    avgPoints 3760.0
  ]
  node [
    id 32
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 25
    avgRank 7.276595744680851
    avgPoints 3811.3829787234044
  ]
  node [
    id 33
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 20
    avgRank 19.80851063829787
    avgPoints 2191.595744680851
  ]
  node [
    id 34
    label "105992"
    name "Ryan"
    surname "Harrison"
    country "USA"
    hand "R"
    dateOfBirth 19920507.0
    tourneyNum 22
    avgRank 55.59574468085106
    avgPoints 926.9148936170212
  ]
  node [
    id 35
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 20
    avgRank 64.74468085106383
    avgPoints 844.1063829787234
  ]
  node [
    id 36
    label "104312"
    name "Andreas"
    surname "Seppi"
    country "ITA"
    hand "R"
    dateOfBirth 19840221.0
    tourneyNum 23
    avgRank 52.06382978723404
    avgPoints 967.7446808510638
  ]
  node [
    id 37
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 24
    avgRank 83.53191489361703
    avgPoints 815.6595744680851
  ]
  node [
    id 38
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 24
    avgRank 20.148936170212767
    avgPoints 1880.6382978723404
  ]
  node [
    id 39
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 19
    avgRank 8.23404255319149
    avgPoints 3956.808510638298
  ]
  node [
    id 40
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 54.255319148936174
    avgPoints 944.6595744680851
  ]
  node [
    id 41
    label "104468"
    name "Gilles"
    surname "Simon"
    country "FRA"
    hand "R"
    dateOfBirth 19841227.0
    tourneyNum 26
    avgRank 49.723404255319146
    avgPoints 1035.8510638297873
  ]
  node [
    id 42
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 32
    avgRank 34.04255319148936
    avgPoints 1336.4893617021276
  ]
  node [
    id 43
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 27
    avgRank 16.72340425531915
    avgPoints 2073.191489361702
  ]
  node [
    id 44
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 23
    avgRank 77.63829787234043
    avgPoints 733.7446808510638
  ]
  node [
    id 45
    label "104898"
    name "Robin"
    surname "Haase"
    country "NED"
    hand "R"
    dateOfBirth 19870406.0
    tourneyNum 30
    avgRank 44.97872340425532
    avgPoints 1076.595744680851
  ]
  node [
    id 46
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 15
    avgRank 25.361702127659573
    avgPoints 1646.063829787234
  ]
  node [
    id 47
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 25
    avgRank 53.638297872340424
    avgPoints 943.7234042553191
  ]
  node [
    id 48
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 17
    avgRank 77.72340425531915
    avgPoints 1223.8297872340424
  ]
  node [
    id 49
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 24
    avgRank 11.319148936170214
    avgPoints 3030.744680851064
  ]
  node [
    id 50
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 25
    avgRank 30.595744680851062
    avgPoints 1666.3829787234042
  ]
  node [
    id 51
    label "111797"
    name "Nicolas"
    surname "Jarry"
    country "CHI"
    hand "R"
    dateOfBirth 19951011.0
    tourneyNum 23
    avgRank 60.234042553191486
    avgPoints 892.5106382978723
  ]
  node [
    id 52
    label "104198"
    name "Guillermo"
    surname "Garcia Lopez"
    country "ESP"
    hand "R"
    dateOfBirth 19830604.0
    tourneyNum 23
    avgRank 78.74468085106383
    avgPoints 724.8723404255319
  ]
  node [
    id 53
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 19
    avgRank 63.40425531914894
    avgPoints 904.468085106383
  ]
  node [
    id 54
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 23
    avgRank 4.042553191489362
    avgPoints 5277.234042553191
  ]
  node [
    id 55
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 29
    avgRank 39.255319148936174
    avgPoints 1440.4893617021276
  ]
  node [
    id 56
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 25
    avgRank 31.148936170212767
    avgPoints 1597.9787234042553
  ]
  node [
    id 57
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 13
    avgRank 2.127659574468085
    avgPoints 7868.936170212766
  ]
  node [
    id 58
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 22
    avgRank 79.72340425531915
    avgPoints 834.9148936170212
  ]
  node [
    id 59
    label "104291"
    name "Malek"
    surname "Jaziri"
    country "TUN"
    hand "R"
    dateOfBirth 19840120.0
    tourneyNum 23
    avgRank 69.72340425531915
    avgPoints 816.0425531914893
  ]
  node [
    id 60
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 23
    avgRank 61.361702127659576
    avgPoints 881.8085106382979
  ]
  node [
    id 61
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 22
    avgRank 20.29787234042553
    avgPoints 1863.0212765957447
  ]
  node [
    id 62
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 10
    avgRank 1.3191489361702127
    avgPoints 8804.36170212766
  ]
  node [
    id 63
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 16
    avgRank 9.404255319148936
    avgPoints 4464.68085106383
  ]
  node [
    id 64
    label "105683"
    name "Milos"
    surname "Raonic"
    country "CAN"
    hand "R"
    dateOfBirth 19901227.0
    tourneyNum 19
    avgRank 24.659574468085108
    avgPoints 1639.5744680851064
  ]
  node [
    id 65
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 16.27659574468085
    avgPoints 2088.0851063829787
  ]
  node [
    id 66
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 24
    avgRank 27.25531914893617
    avgPoints 1685.7659574468084
  ]
  node [
    id 67
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 29
    avgRank 33.97872340425532
    avgPoints 1327.2340425531916
  ]
  node [
    id 68
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 30
    avgRank 31.361702127659573
    avgPoints 1416.8085106382978
  ]
  node [
    id 69
    label "105815"
    name "Tennys"
    surname "Sandgren"
    country "USA"
    hand "R"
    dateOfBirth 19910722.0
    tourneyNum 20
    avgRank 60.255319148936174
    avgPoints 870.8510638297872
  ]
  node [
    id 70
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 30
    avgRank 57.08510638297872
    avgPoints 918.1063829787234
  ]
  node [
    id 71
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 27
    avgRank 43.06382978723404
    avgPoints 1241.4893617021276
  ]
  node [
    id 72
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 22
    avgRank 47.297872340425535
    avgPoints 1113.8936170212767
  ]
  edge [
    source 0
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 36
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 5
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 6
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 2
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 0
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 3
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 56
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 40
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 64
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 49
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 1
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 62
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 1
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 2
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 66
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 31
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 2
    target 4
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 67
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 19
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 66
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 3
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 3
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 23
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 35
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 3
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 25
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 3
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 9
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 3
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 15
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 49
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 58
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 69
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 7
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 57
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 40
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 8
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 13
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 11
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 9
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 70
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 9
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 51
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 57
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 21
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 48
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 11
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 47
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 43
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 11
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 45
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 66
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 12
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 38
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 13
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 56
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 55
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 20
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 31
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 15
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 49
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 70
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 51
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 43
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 17
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 51
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 66
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 44
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 56
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 57
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 37
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 23
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 53
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 17
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 18
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 50
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 18
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 49
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 62
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 49
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 20
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 20
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 50
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 20
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 63
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 20
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 29
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 20
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 65
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 22
    target 58
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 50
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 22
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 22
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 36
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 53
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 23
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 43
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 24
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 71
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 25
    target 68
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 43
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 65
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 39
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 26
    target 28
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 42
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 56
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 27
    target 55
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 27
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 45
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 27
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 50
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 28
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 72
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 28
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 45
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 60
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 30
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 41
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 44
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 31
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 40
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 31
    target 33
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 31
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 55
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 31
    target 32
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 31
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 57
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 31
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 63
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 31
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 60
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 55
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 32
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 62
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 32
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 32
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 32
    target 33
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 32
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 63
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 33
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 33
    target 46
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 57
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 33
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 33
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 49
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 34
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 40
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 57
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 35
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 61
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 36
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 54
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 36
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 38
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 36
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 36
    target 68
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 37
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 37
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 56
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 38
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 38
    target 41
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 61
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 46
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 39
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 59
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 39
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 39
    target 48
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 39
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 40
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 40
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 40
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 58
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 42
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 62
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 42
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 44
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 69
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 43
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 47
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 70
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 44
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 44
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 44
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 72
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 45
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 67
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 45
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 45
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 45
    target 54
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 46
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 46
    target 57
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 46
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 54
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 47
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 49
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 51
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 47
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 47
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 47
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 54
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 49
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 64
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 49
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 56
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 49
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 49
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 49
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 70
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 50
    target 56
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 51
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 65
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 51
    target 70
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 51
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 52
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 52
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 63
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 53
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 54
    target 58
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 54
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 54
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 54
    target 62
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 54
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 54
    target 56
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 54
    target 59
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 54
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 54
    target 63
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 54
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 54
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 60
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 59
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 55
    target 71
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 55
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 62
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 55
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 58
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 55
    target 56
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 70
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 56
    target 62
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 56
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 56
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 56
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 66
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 57
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 64
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 57
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 57
    target 63
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 57
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 58
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 58
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 58
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 58
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 59
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 59
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 59
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 72
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 60
    target 64
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 63
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 67
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 67
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 61
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 61
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 62
    target 65
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 62
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 63
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 63
    target 68
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 63
    target 69
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 63
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 64
    target 67
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 64
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 64
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 65
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 65
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 67
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 66
    target 71
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 67
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 67
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 67
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 68
    target 71
    weight 1
    lowerId 1
    higherId 0
  ]
]
