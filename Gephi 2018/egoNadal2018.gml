graph [
  node [
    id 0
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 29
    avgRank 52.42553191489362
    avgPoints 1126.4255319148936
  ]
  node [
    id 1
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 15
    avgRank 5.595744680851064
    avgPoints 4739.04255319149
  ]
  node [
    id 2
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 29
    avgRank 50.87234042553192
    avgPoints 990.0
  ]
  node [
    id 3
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 23
    avgRank 32.48936170212766
    avgPoints 1327.2340425531916
  ]
  node [
    id 4
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 21
    avgRank 37.57446808510638
    avgPoints 1218.936170212766
  ]
  node [
    id 5
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 18
    avgRank 82.59574468085107
    avgPoints 680.2127659574468
  ]
  node [
    id 6
    label "103607"
    name "Victor"
    surname "Estrella"
    country "DOM"
    hand "R"
    dateOfBirth 19800802.0
    tourneyNum 9
    avgRank 217.89361702127658
    avgPoints 291.9574468085106
  ]
  node [
    id 7
    label "109739"
    name "Maximilian"
    surname "Marterer"
    country "GER"
    hand "L"
    dateOfBirth 19950615.0
    tourneyNum 23
    avgRank 66.63829787234043
    avgPoints 808.1063829787234
  ]
  node [
    id 8
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 24
    avgRank 63.829787234042556
    avgPoints 828.7659574468086
  ]
  node [
    id 9
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 25
    avgRank 48.59574468085106
    avgPoints 1016.5106382978723
  ]
  node [
    id 10
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 22
    avgRank 5.446808510638298
    avgPoints 4522.446808510638
  ]
  node [
    id 11
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 17
    avgRank 11.914893617021276
    avgPoints 2930.1063829787236
  ]
  node [
    id 12
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 26
    avgRank 29.21276595744681
    avgPoints 1468.723404255319
  ]
  node [
    id 13
    label "106210"
    name "Jiri"
    surname "Vesely"
    country "CZE"
    hand "L"
    dateOfBirth 19930710.0
    tourneyNum 19
    avgRank 81.70212765957447
    avgPoints 697.3191489361702
  ]
  node [
    id 14
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 25
    avgRank 7.276595744680851
    avgPoints 3811.3829787234044
  ]
  node [
    id 15
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 20
    avgRank 19.80851063829787
    avgPoints 2191.595744680851
  ]
  node [
    id 16
    label "105379"
    name "Aljaz"
    surname "Bedene"
    country "SLO"
    hand "R"
    dateOfBirth 19890718.0
    tourneyNum 20
    avgRank 64.74468085106383
    avgPoints 844.1063829787234
  ]
  node [
    id 17
    label "104620"
    name "Simone"
    surname "Bolelli"
    country "ITA"
    hand "R"
    dateOfBirth 19851008.0
    tourneyNum 4
    avgRank 151.5531914893617
    avgPoints 373.0851063829787
  ]
  node [
    id 18
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 19
    avgRank 8.23404255319149
    avgPoints 3956.808510638298
  ]
  node [
    id 19
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 32
    avgRank 34.04255319148936
    avgPoints 1336.4893617021276
  ]
  node [
    id 20
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 27
    avgRank 16.72340425531915
    avgPoints 2073.191489361702
  ]
  node [
    id 21
    label "105062"
    name "Mikhail"
    surname "Kukushkin"
    country "KAZ"
    hand "R"
    dateOfBirth 19871226.0
    tourneyNum 23
    avgRank 77.63829787234043
    avgPoints 733.7446808510638
  ]
  node [
    id 22
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 17
    avgRank 77.72340425531915
    avgPoints 1223.8297872340424
  ]
  node [
    id 23
    label "103970"
    name "David"
    surname "Ferrer"
    country "ESP"
    hand "R"
    dateOfBirth 19820402.0
    tourneyNum 18
    avgRank 77.82978723404256
    avgPoints 901.9148936170212
  ]
  node [
    id 24
    label "104198"
    name "Guillermo"
    surname "Garcia Lopez"
    country "ESP"
    hand "R"
    dateOfBirth 19830604.0
    tourneyNum 23
    avgRank 78.74468085106383
    avgPoints 724.8723404255319
  ]
  node [
    id 25
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 23
    avgRank 4.042553191489362
    avgPoints 5277.234042553191
  ]
  node [
    id 26
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 29
    avgRank 39.255319148936174
    avgPoints 1440.4893617021276
  ]
  node [
    id 27
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 25
    avgRank 31.148936170212767
    avgPoints 1597.9787234042553
  ]
  node [
    id 28
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 22
    avgRank 79.72340425531915
    avgPoints 834.9148936170212
  ]
  node [
    id 29
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 10
    avgRank 1.3191489361702127
    avgPoints 8804.36170212766
  ]
  node [
    id 30
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 16
    avgRank 9.404255319148936
    avgPoints 4464.68085106383
  ]
  node [
    id 31
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 16.27659574468085
    avgPoints 2088.0851063829787
  ]
  node [
    id 32
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 29
    avgRank 33.97872340425532
    avgPoints 1327.2340425531916
  ]
  node [
    id 33
    label "105577"
    name "Vasek"
    surname "Pospisil"
    country "CAN"
    hand "R"
    dateOfBirth 19900623.0
    tourneyNum 15
    avgRank 83.40425531914893
    avgPoints 670.2978723404256
  ]
  node [
    id 34
    label "104534"
    name "Dudi"
    surname "Sela"
    country "ISR"
    hand "R"
    dateOfBirth 19850404.0
    tourneyNum 10
    avgRank 158.27659574468086
    avgPoints 422.02127659574467
  ]
  node [
    id 35
    label "105373"
    name "Martin"
    surname "Klizan"
    country "SVK"
    hand "L"
    dateOfBirth 19890711.0
    tourneyNum 11
    avgRank 96.14893617021276
    avgPoints 669.1489361702128
  ]
  edge [
    source 0
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 4
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 1
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 27
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 23
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 1
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 11
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 29
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 1
    target 2
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 20
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 12
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 5
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 15
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 27
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 3
    target 10
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 3
    target 15
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 3
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 14
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 9
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 31
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 7
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 14
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 17
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 14
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 19
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 15
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 30
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 10
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 27
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 18
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 26
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 11
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 26
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 14
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 29
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 14
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 15
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 14
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 30
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 15
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 20
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 18
    target 22
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 18
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 29
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 19
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 35
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 22
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 25
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 23
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 28
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 25
    target 29
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 25
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 25
    target 30
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 25
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 32
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 26
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 29
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 26
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 26
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 29
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 27
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 28
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 33
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 31
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 29
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 30
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
]
